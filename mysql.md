#mysql常用操作
## 一、创建用户
### 命令：
```sql
CREATE USER 'username'@'host' IDENTIFIED BY 'password';
```
### 说明：
- username：你将创建的用户名
- host：指定该用户在哪个主机上可以登陆，如果是本地用户可用localhost，如果想让该用户可以从任意远程主机登陆，可以使用通配符%
- password：该用户的登陆密码，密码可以为空，如果为空则该用户可以不需要密码登陆服务器

### 例子：
```sql
CREATE USER 'dog'@'localhost' IDENTIFIED BY '123456';
CREATE USER 'pig'@'192.168.1.101_' IDENDIFIED BY '123456';
CREATE USER 'pig'@'%' IDENTIFIED BY '123456';
CREATE USER 'pig'@'%' IDENTIFIED BY '';
CREATE USER 'pig'@'%';
```

## 二、授权:
### 命令：
```sql
GRANT privileges ON databasename.tablename TO 'username'@'host'
```
### 说明：
- privileges：用户的操作权限，如SELECT，INSERT，UPDATE等，如果要授予所的权限则使用ALL
- databasename：数据库名
- tablename：表名，如果要授予该用户对所有数据库和表的相应操作权限则可用*表示，如*.*


### 例子：
```sql
GRANT SELECT, INSERT ON test.user TO 'pig'@'%';
GRANT ALL ON *.* TO 'pig'@'%';
```
### 注意：
用以上命令授权的用户不能给其它用户授权，如果想让该用户可以授权，用以下命令:
```sql
GRANT privileges ON databasename.tablename TO 'username'@'host' WITH GRANT OPTION;
```
## 三、设置与更改用户密码
### 命令：
```sql
SET PASSWORD FOR 'username'@'host' = PASSWORD('newpassword');
```
如果是当前登陆用户用:
```sql
SET PASSWORD = PASSWORD("newpassword");
```
### 说明：
- username：你将创建的用户名
- host：指定该用户在哪个主机上可以登陆，如果是本地用户可用localhost，如果想让该用户可以从任意远程主机登陆，可以使用通配符%
- password：该用户的登陆密码，密码可以为空，如果为空则该用户可以不需要密码登陆服务器

### 例子：
```sql
CREATE USER 'dog'@'localhost' IDENTIFIED BY '123456';
CREATE USER 'pig'@'192.168.1.101_' IDENDIFIED BY '123456';
CREATE USER 'pig'@'%' IDENTIFIED BY '123456';
CREATE USER 'pig'@'%' IDENTIFIED BY '';
CREATE USER 'pig'@'%';
```
## 四、 撤销用户权限
### 命令：
```sql
REVOKE privilege ON databasename.tablename FROM 'username'@'host';
```
### 说明：
privilege, databasename, tablename：同授权部分

### 例子：
```sql
REVOKE SELECT ON *.* FROM 'pig'@'%';
```
### 注意:
假如你在给用户`'pig'@'%'`授权的时候是这样的（或类似的）：`GRANT SELECT ON test.user TO 'pig'@'%'`，则在使用`REVOKE SELECT ON *.* FROM 'pig'@'%';`命令并不能撤销该用户对test数据库中user表的`SELECT` 操作。相反，如果授权使用的是`GRANT SELECT ON *.* TO 'pig'@'%';`则`REVOKE SELECT ON test.user FROM 'pig'@'%';`命令也不能撤销该用户对test数据库中user表的`Select`权限。

具体信息可以用命令`SHOW GRANTS FOR 'pig'@'%'; `查看。

##五、数据库导入导出

###一、Mysql数据库备份指令格式：

`mysqldump -h主机名  -P端口 -u用户名 -p密码 (–database) 数据库名 > 文件名.sql`

 注：**直接cmd执行该指令即可，不需要先mysql -u root -p链接数据库**
#####1、备份MySQL数据库的命令
```sql
mysqldump -hhostname -uusername -ppassword databasename > backupfile.sql
```

(例：`mysqldump -h 122.51.176.153 -uwang -p1992S@ sw_account --default-character-set=gbk --opt -Q -R --skip-lock-tables>testbackup.sql`)

#####2、备份MySQL数据库为带删除表的格式，能够让该备份覆盖已有数据库而不需要手动删除原有数据库。
```sql
mysqldump -–add-drop-table -uusername -ppassword databasename > backupfile.sql
```
#####3、直接将MySQL数据库压缩备份
```sql
mysqldump -hhostname -uusername -ppassword databasename | gzip > backupfile.sql.gz
```
#####4、备份MySQL数据库某个(些)表
```sql
mysqldump -hhostname -uusername -ppassword databasename specific_table1 specific_table2 > backupfile.sql
```
#####5、同时备份多个MySQL数据库
```sql
mysqldump -hhostname -uusername -ppassword –databases databasename1 databasename2 databasename3 > multibackupfile.sql
```
#####6、仅备份数据库结构
```sql
mysqldump –no-data –databases databasename1 databasename2 databasename3 > structurebackupfile.sql
```
#####7、备份服务器上所有数据库
```sql
mysqldump –all-databases > allbackupfile.sql
```
#####二、数据库还原有三种方式：source命令、mysql、gunzip命令

##### 1、source 命令
进入mysql数据库控制台，
`mysql -u root -p`
`mysql>use 数据库`
然后使用source命令，后面参数为脚本文件（如这里用到的.sql）
`mysql>source /home/work/db/bkdb.sql`

##### 2、mysql
```sql
mysql -hhostname -uusername -ppassword databasename < backupfile.sql
```

##### 3、gunzip
```sql
gunzip < backupfile.sql.gz | mysql -uusername -ppassword databasename
```
















