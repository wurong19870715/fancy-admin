# FancyAdmin #
## [前端源码](https://gitee.com/wurong19870715/fancy-admin-web "前端源码")

## [后端源码](https://gitee.com/wurong19870715/fancy-admin "后端源码")

## 项目简介
Fancy后台管理系统，基于springboot+shiro+mybatis-plus开发的后台管理系统，目前支持功能有：
-  组织架构管理
-  角色管理
-  定时任务管理
-  配置管理
-  枚举管理
-  日志管理
-  自动建表（类似于hibernate）
-  代码生成器（新版计划中）
## 技术选型
   SpringBoot、mybatis-plus、shiro、jwt、redis
## 功能列表
-  组织架构管理：机构、部门、岗位、人员管理，提供统一选择控件
-  角色管理：动态配置权限和组织架构的关联
-  枚举管理：对一些需要转换的数据进行统一管理，如：男、女等
-  配置管理：对于一些动态配置的数据，提供统一管理
-  登录日志：用于记录用户登录操作
-  定时任务：定时执行业务逻辑
-  自动建表：仿照hibernate的方式，根据数据字典来自动建表（目前支持mysql）
-  统一分类：统一分类，只需要配置即可以实现通用的增删改查

## 安装教程
 ### 环境及插件要求
 - JDK8+
 - Mysql5.7+
 - Maven
 - Lombok
 ### 开发工具
 推荐 Idea Community Edition + VSCode
 ### 运行项目
 通过Java应用方式运行`com.fancy.application.FancyDevApplication`文件
 
 `application.properties`文件中修改数据库配置、redis配置，以及建表配置，附件路径
## 项目截图
### 登录界面
![登录界面](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/login.png "登录界面")
### 首页
![首页](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/dashboard.png "首页")
### 组织架构
![组织架构](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/org.png "组织架构")
### 权限分配
![权限分配](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/role.png "权限分配")
### 代码生成器
![代码生成器](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/codeGenerate1.png "代码生成器1")
![代码生成器](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/codeGenerate2.png "代码生成器2")
![代码生成器](https://gitee.com/wurong19870715/fancy-admin-web/raw/master/public/img/codeGenerate3.png "代码生成器3")
## 交流
QQ群:816204482

#后续开发计划
1、组织架构的导入导出

2、表单设计器的集成
## LICENSE
**MIT**