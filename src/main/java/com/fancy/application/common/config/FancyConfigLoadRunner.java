package com.fancy.application.common.config;

import com.fancy.application.sys.permission.service.IDataLoadService;
import com.fancy.application.framework.dic.method.Dict;
import com.fancy.application.sys.rest.service.ISysRestMainService;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Created by wison on 17/3/23.
 */
@Log4j2
@Component
public class FancyConfigLoadRunner {
    @PostConstruct
    public void load() {
        //加载数据字典
        Dict.getInstance().load();
        //加载模块信息
        dataLoadService.load();
        //加载接口的配置信息
    }

    @Resource
    private IDataLoadService dataLoadService;

}
