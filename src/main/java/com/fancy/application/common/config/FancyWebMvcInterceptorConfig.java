package com.fancy.application.common.config;

import com.fancy.application.common.constant.Constants;
import com.fancy.application.sys.rest.service.ISysRestConstants;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.annotation.Resource;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Configuration
public class FancyWebMvcInterceptorConfig extends WebMvcConfigurationSupport {

    @Resource
    private HandlerInterceptor sysRestMainInterceptor;

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        //注册接口管理的拦截器
        String restApiPath = ISysRestConstants.fdBasePath+"/**";
        registry.addInterceptor(sysRestMainInterceptor).addPathPatterns(restApiPath);
        //拦截器
        super.addInterceptors(registry);
    }

/*    @Override
    protected void configurePathMatch(PathMatchConfigurer configurer) {
        super.configurePathMatch(configurer);
        // 常用的两种
        // 匹配结尾 / :会识别 url 的最后一个字符是否为 /
        // localhost:8080/test 与 localhost:8080/test/ 等价
        configurer.setUseTrailingSlashMatch(true);
        // 匹配后缀名：会识别 xx.* 后缀的内容
        // localhost:8080/test 与 localhost:8080/test.jsp 等价
        configurer.setUseSuffixPatternMatch(true);

        // TODO PathMatchConfigurer 还提供其他的一些 api 以供使用
    }*/

//    @Resource
//    private HandlerInterceptor sysAuthUrlInterceptor;



    //定义时间格式转换器
//    @Bean
//    public MappingJackson2HttpMessageConverter jackson2HttpMessageConverter() {
//        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
//        ObjectMapper mapper = new ObjectMapper();
//        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
//        mapper.setDateFormat(new SimpleDateFormat(Constants.FORMAT_DATETIME));
//        converter.setObjectMapper(mapper);
//        return converter;
//    }
//
//    //添加转换器
//    @Override
//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//        //将我们定义的时间格式转换器添加到转换器列表中,
//        //这样jackson格式化时候但凡遇到Date类型就会转换成我们定义的格式
//        converters.add(jackson2HttpMessageConverter());
//        super.addDefaultHttpMessageConverters(converters);
//    }
}
