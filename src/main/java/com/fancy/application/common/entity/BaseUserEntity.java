package com.fancy.application.common.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public abstract class BaseUserEntity extends BaseEntity {
    /**
     * 创建人id
     */
    @SimpleField(messageKey = "创建人id",name = "fdCreatorId",column = "fd_creator_id",relationModel = SysOrgPerson.class)
    @TableField(value="fd_creator_id",fill = FieldFill.INSERT)
    private String fdCreatorId;
    /**
     * 创建人姓名
     */
    @SimpleField(messageKey = "创建人姓名",name = "fdCreatorName",column = "fd_creator_name",relationModel = SysOrgPerson.class,relationField = "fdName")
    @TableField(value="fd_creator_name",fill = FieldFill.INSERT)
    private String fdCreatorName;
}
