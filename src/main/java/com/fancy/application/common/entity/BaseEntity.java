package com.fancy.application.common.entity;

/**
 * Created by wison on 2019/7/31.
 */
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fancy.application.common.constant.Constants;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.framework.dic.enums.TypeEnums;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@Dictionary(modelName = "com.fancy.application.common.entity.BaseEntity")
public abstract class BaseEntity {
    @TableId(value="fd_id")
    @SimpleField(messageKey = "主键id",name = "fdId",column = "fd_id",nullable = false,pk = true)
    private String fdId;
    
    @TableField(value="fd_create_time",fill = FieldFill.INSERT)
    @SimpleField(messageKey = "创建时间",name = "fdCreateTime",column = "fd_create_time",type = TypeEnums.DATE)
    @JsonFormat(pattern= Constants.FORMAT_DATETIME,timezone = "GMT+8")
    private Date fdCreateTime;

}
