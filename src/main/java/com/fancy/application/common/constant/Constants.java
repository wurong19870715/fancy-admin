package com.fancy.application.common.constant;

/**
 * 全局常量
 * @author wison
 */
public class Constants {

    /**
     * 初始化状态
     */
    public static final String INIT_STATUS="init_status";
    /**
     * 全局分页默认15条数据
     */
    public static final int DEFAULT_PAGE_SIZE = 15;

    public static final String DEFAULT_CHARSET = "UTF-8";
    public static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";
    public static final String FORMAT_DATE = "yyyy-MM-dd";

    /**
     * redis 中模块列表key值
     *
     */
    public static final String MODULE_KEY = "module";
    /**
     * redis 中权限分类key值
     */
    public static final String RES_CATE_KEY = "resCateList";
}
