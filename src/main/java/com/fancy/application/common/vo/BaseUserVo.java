package com.fancy.application.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 包涵创建者信息的对象
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BaseUserVo extends BaseVo {
    /**
     * 创建者id
     */
    private String fdCreatorId;

    /**
     * 创建者名称
     */
    private String fdCreatorName;
}
