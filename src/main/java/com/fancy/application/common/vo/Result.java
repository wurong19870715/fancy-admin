package com.fancy.application.common.vo;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.Collection;

/**
 * @author wison
 */
@Data
public class Result {

    /**
     * 1    success
     * 0    error
     */
    private Integer status;
    private String message;

    private Object data;

    private Result(int statusCode, String message) {
        this.status = statusCode;
        this.message = message;
    }

    public static Result ok(Object obj){
        if(obj instanceof  String){
            return Result.custom(1, StrUtil.toString(obj));
        }else{
            Result result = new Result(1,"success");
            result.setData(obj);
            return result;
        }

    }
    public static Result ok(){
        return Result.custom(1,"success");
    }

    public static Result error() {
        return Result.custom(0,"error");
    }
    public static Result error(String message) {
        return Result.custom(0,message);
    }

    public static Result error(Object obj){
        Result result = Result.error();
        result.setData(obj);
        return result;
    }

    public static Result custom(Integer statusCode ,String message){
        Result result = new Result(statusCode,message);
        result.setData("");
        return result;
    }


    public Result addData(Object data) {

        this.data = data;
        return this;
    }


}
