package com.fancy.application.common.vo;

import lombok.Data;

@Data
public class BaseParam {

    /**
     * 关键字
     */
    private String keyword;
}
