package com.fancy.application.common.vo;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.fancy.application.common.constant.Constants;
import com.fancy.application.common.service.impl.SqlQueryWrapper;
import lombok.Data;
import lombok.EqualsAndHashCode;


/**
 * 基础查询list用的VO
 * @author wison
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class BasePage<T> extends BaseParam {

    /**
     * 页数
     */
    private int size;
    /**
     * 当前页数
     */
    private int current;


    /**
     * 默认查询的字段
     */
    private final String defaultKeywordField = "fd_name";



    public int getSize() {
        return size > 0 ? size : Constants.DEFAULT_PAGE_SIZE;
    }

    public int getCurrent() {
        return current > 0 ? current : 1;
    }

    public IPage<T> getPage(){
        return new Page<>(getCurrent(),getSize());
    }

    public SqlQueryWrapper<T> getWrapper(){
        SqlQueryWrapper<T> wrapper = new SqlQueryWrapper<>();
        if(StrUtil.isNotEmpty(getKeyword())){
            wrapper.like(this.defaultKeywordField,getKeyword());
        }
        wrapper.orderByDesc("fd_create_time");
        return wrapper;
    }

    public SqlQueryWrapper<T> getWrapper(String defaultKeywordField){
        SqlQueryWrapper<T> wrapper = null;
        if(StrUtil.isNotEmpty(defaultKeywordField)){
            wrapper = new SqlQueryWrapper<>();
            if(StrUtil.isNotEmpty(getKeyword())){
                wrapper.like(defaultKeywordField,getKeyword());
            }
        }else{
            wrapper = getWrapper();
        }
        wrapper.orderByDesc("fd_create_time");
        return wrapper;
    }

}
