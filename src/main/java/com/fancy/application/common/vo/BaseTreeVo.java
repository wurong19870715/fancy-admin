package com.fancy.application.common.vo;

import lombok.Data;

import java.util.List;

/**
 * Created by wison on 2019/10/5.
 */
@Data
public class BaseTreeVo {
    private String id;

    private String name;

    private Boolean leaf;

    private List<BaseTreeVo> children;

    public BaseTreeVo(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public BaseTreeVo() {
    }
}
