package com.fancy.application.common.vo;

import lombok.Getter;

/*
    下拉列表展示的数据()
 */
@Getter
public class SelectVo {

    private final String name;
    private final String label;

    private final String value;

    public SelectVo(String name, String value) {
        this.label = name;
        this.name = name;
        this.value = value;
    }
}
