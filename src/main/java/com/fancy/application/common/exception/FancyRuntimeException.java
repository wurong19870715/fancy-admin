package com.fancy.application.common.exception;

/**
 * @author wison
 */
public class FancyRuntimeException extends RuntimeException {

    private int code;

    public FancyRuntimeException(String s) {
        super(s);
    }

    public FancyRuntimeException(int code, String message) {
        super(message);
        this.setCode(code);
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
