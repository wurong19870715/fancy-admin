package com.fancy.application.common.exception;

import com.fancy.application.common.utils.SpringBeanUtil;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.rest.service.ISysRestConstants;
import com.fancy.application.sys.rest.service.ISysRestLogService;
import com.fancy.application.sys.rest.service.ISysRestMainService;
import com.fancy.application.sys.rest.service.impl.CheckURIResult;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author wison
 */
@RestControllerAdvice
@Component
public class GlobalExceptionHandler {
    /**
     * 定义要捕获的异常 可以多个 @ExceptionHandler({})
     *
     * @param request  request
     * @param e        exception
     * @param response response
     * @return 响应结果
     */
    @ExceptionHandler(FancyRuntimeException.class)
    public Result customExceptionHandler(HttpServletRequest request, final Exception e, HttpServletResponse response) {
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        saveRestApiLog(request,e);
        FancyRuntimeException exception = (FancyRuntimeException) e;
        return Result.custom(exception.getCode(), exception.getMessage());
    }

    /**
     * 定义要捕获的异常 可以多个 @ExceptionHandler({})
     *
     * @param request  request
     * @param e        exception
     * @param response response
     * @return 响应结果
     */
    @ExceptionHandler(RuntimeException.class)
    public Result runtimeExceptionHandler(HttpServletRequest request, final Exception e, HttpServletResponse response) {
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        saveRestApiLog(request,e);
        return Result.error(e.getMessage());
    }

    /**
     * 捕获  Exception 异常
     * TODO  如果你觉得在一个 exceptionHandler 通过  if (e instanceof xxxException) 太麻烦
     * TODO  那么你还可以自己写多个不同的 exceptionHandler 处理不同异常
     *
     * @param request  request
     * @param e        exception
     * @param response response
     * @return 响应结果
     */
    @ExceptionHandler({Exception.class})
    public Result exceptionHandler(HttpServletRequest request, final Exception e, HttpServletResponse response) {
        response.setStatus(HttpStatus.BAD_REQUEST.value());
        saveRestApiLog(request,e);
        return Result.error(e.getMessage());
    }

    private void saveRestApiLog(HttpServletRequest request,Exception e){
        ISysRestLogService sysRestLogService = SpringBeanUtil.getBean(ISysRestLogService.class);
        ISysRestMainService sysRestMainService = SpringBeanUtil.getBean(ISysRestMainService.class);
        CheckURIResult checkURIResult = sysRestMainService.isAllowed(request);
        if(checkURIResult.getPass()) {//请求拦截通过。
            sysRestLogService.saveErrorLog(request, e);
        }
    }
}
