package com.fancy.application.common.service;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.common.entity.BaseUserEntity;
import com.fancy.application.common.utils.UserUtil;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;
@Log4j2
@Component
public class FillEntityHandler implements MetaObjectHandler {
    @Override
    public void insertFill(MetaObject metaObject) {
        Date date = new Date();
       log.debug("开始自动填充...");
        this.setFieldValByName("fdCreateTime",date,metaObject);
        if(metaObject.getOriginalObject() instanceof BaseUserEntity){
            SysOrgPerson person = UserUtil.getCurrentUser();
            this.setFieldValByName("fdCreatorId",person.getFdId(),metaObject);
            this.setFieldValByName("fdCreatorName",person.getFdName(),metaObject);
        }
        log.debug("填充结束...");
    }

    @Override
    public void updateFill(MetaObject metaObject) {

    }
}
