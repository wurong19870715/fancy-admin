package com.fancy.application.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

//queryWrapper的扩展，增加数据权限查询
public class SqlQueryWrapper<T> extends QueryWrapper<T> {
    //是否是数据权限的查询
    private Boolean authQuery = Boolean.FALSE;


    public Boolean getAuthQuery() {
        return authQuery;
    }

    public void authQuery(){
        this.authQuery = Boolean.TRUE;
    }

    public SqlQueryWrapper(){
        super();
        this.authQuery = Boolean.TRUE;
    }




}
