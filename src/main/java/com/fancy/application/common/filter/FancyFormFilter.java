package com.fancy.application.common.filter;


import com.fancy.application.common.Interceptor.wrapper.FancyHttpServletRequestWrapper;
import com.fancy.application.common.constant.Constants;
import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Log4j2
//获取设置编码格式
@Component
@WebFilter(filterName = "fancyFormFilter", urlPatterns = {"/*"})
public class FancyFormFilter implements IFancyHandlerFilter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        request.setCharacterEncoding(Constants.DEFAULT_CHARSET);
        response.setCharacterEncoding(Constants.DEFAULT_CHARSET);
        if(request instanceof HttpServletRequest){
            ServletRequest requestWrapper = new FancyHttpServletRequestWrapper((HttpServletRequest) request);
            chain.doFilter(requestWrapper, response);
        }else{
            chain.doFilter(request, response);
        }


    }
}
