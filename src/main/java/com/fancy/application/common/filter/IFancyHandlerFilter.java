package com.fancy.application.common.filter;

import org.springframework.http.MediaType;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;

//继承接口，扩展默认函数
public interface IFancyHandlerFilter extends Filter {
    /**
     * 判断本次请求的数据类型是否为json
     *
     * @param request request
     * @return boolean
     */
    default boolean isJson(HttpServletRequest request) {
        if (request.getContentType() != null) {
            return request.getContentType().equals(MediaType.APPLICATION_JSON_VALUE) ||
                    request.getContentType().equals(MediaType.APPLICATION_JSON_UTF8_VALUE);
        }

        return false;
    }
}
