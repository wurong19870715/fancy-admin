package com.fancy.application.common.convert;

import cn.hutool.core.convert.Converter;
import cn.hutool.core.convert.ConverterRegistry;
import cn.hutool.core.date.DateUtil;
import com.fancy.application.FancyDevApplication;

import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * @author wison
 * 简单的日期返回json的时候,做格式化
 */
@Log4j2
@Component
public class SimpleDateTimeConverter implements CommandLineRunner,Converter<String> {


    @Override
    public String convert(Object value, String defaultValue) throws IllegalArgumentException {
        if(value instanceof Date){
            defaultValue = DateUtil.format((Date) value,"yyyy-MM-dd HH:mm:ss");
        }
        return defaultValue;
    }

    @Override
    public void run(String... args) throws Exception {
        log.debug("注册日期转换器");
        log.info("注册日期转换器");
        log.warn("注册日期转换器");
        log.error("注册日期转换器");
        log.trace("注册日期转换器");
        ConverterRegistry converterRegistry = ConverterRegistry.getInstance();
        converterRegistry.putCustom(String.class, this);
    }
}
