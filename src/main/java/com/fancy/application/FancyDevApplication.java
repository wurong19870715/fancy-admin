package com.fancy.application;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * 如果mybatis中service实现类中加入事务注解，需要此处添加该注解
 * @author wison
 */
@SpringBootApplication
@EnableTransactionManagement
@ServletComponentScan(basePackages="com.fancy.application.common.filter")
public class FancyDevApplication {
    public static void main(String[] args) {
        SpringApplication.run(FancyDevApplication.class, args);

        System.out.println("系统启动成功");
    }

}
