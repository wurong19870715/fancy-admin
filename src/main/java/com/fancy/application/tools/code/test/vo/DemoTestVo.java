package com.fancy.application.tools.code.test.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2021-08-12
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class DemoTestVo extends BaseVo {
    /**
    * 测试
    */
    private String fdTest;

}
