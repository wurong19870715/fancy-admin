package com.fancy.application.tools.code.generate.controller;


import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.tools.code.generate.service.ICodeGenerateModuleInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author wison
 */
@RestController
@RequestMapping("/code/generate")
public class CodeGenerateController extends BaseController {
    @GetMapping
    public Result generate(@RequestParam String id){
        codeGenerateModuleInfoService.generateCodeByModuleId(id);
        return Result.ok();
    }

    @Resource
    private ICodeGenerateModuleInfoService codeGenerateModuleInfoService;
}
