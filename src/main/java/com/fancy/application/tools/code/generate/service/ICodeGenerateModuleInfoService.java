package com.fancy.application.tools.code.generate.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.tools.code.generate.entity.CodeGenerateModuleInfo;

public interface ICodeGenerateModuleInfoService extends IBaseService<CodeGenerateModuleInfo> {
    public String getDirPath(String fdModuleId);

    public void generateCodeByModuleId(String id);

    void deleteModuleById(String id);
}
