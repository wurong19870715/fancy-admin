package com.fancy.application.tools.code.generate.entity;


import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wison
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="code_generate_column_info")
@Dictionary(modelName = "com.fancy.application.tools.code.generate.entity.CodeGenerateColumnInfo",
        tableName = "code_generate_column_info",
        serviceBean = "codeGenerateColumnInfoService",
        messageKey = "代码生成字段信息"
)
public class CodeGenerateColumnInfo extends BaseEntity {
    /**
     * 名称
     */
    @TableField("fd_name")
    @SimpleField(messageKey = "名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;


    /**
     * sql字段名
     */
    @TableField("fd_column_name")
    @SimpleField(messageKey = "sql字段名称",name = "fdColumnName",column = "fd_column_name",length = 50)
    private String fdColumnName;

    /**
     * 表id
     */
    @TableField("fd_table_id")
    @SimpleField(messageKey = "关联表id",name = "fdTableId",column = "fd_table_id",relationModel =CodeGenerateTableInfo.class )
    private String fdTableId;

    /**
     * 长度
     */
    @TableField("fd_length")
    @SimpleField(messageKey = "长度",name = "fdLength",column = "fd_length",length = 50)
    private Integer fdLength;
    /**
     * 是否为主键
     */
    @TableField("fd_is_primary_key")
    @SimpleField(messageKey = "是否为主键",name = "fdIsPrimaryKey",column = "fd_is_primary_key",type = TypeEnums.BOOLEAN)
    private Boolean fdIsPrimaryKey;

    /**
     * 是否必填
     */
    @TableField("fd_is_required")
    @SimpleField(messageKey = "是否为必填",name = "fdIsRequired",column = "fd_is_required",type = TypeEnums.BOOLEAN)
    private Boolean fdIsRequired;

    /**
     * 字段类型
     */
    @TableField("fd_column_type")
    @SimpleField(messageKey = "字段类型",name = "fdColumnType",column = "fd_column_type")
    private String fdColumnType;

    /**
     * 模型名称
     */
    @TableField("fd_model_name")
    @SimpleField(messageKey = "模型名称",name = "fdModelName",column = "fd_model_name")
    private String fdModelName;

    /**
     * 排序
     */
    @TableField("fd_order")
    @SimpleField(messageKey = "排序",name = "fdOrder",column = "fd_order",type = TypeEnums.INTEGER)
    private int fdOrder;

}




