package com.fancy.application.tools.code.generate.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.tools.code.generate.entity.CodeGenerateColumnInfo;
import com.fancy.application.tools.code.generate.entity.CodeGenerateModuleInfo;
import com.fancy.application.tools.code.generate.entity.CodeGenerateTableInfo;
import com.fancy.application.tools.code.generate.service.ICodeGenerateColumnInfoService;
import com.fancy.application.tools.code.generate.service.ICodeGenerateModuleInfoService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fancy.application.tools.code.generate.util.GenUtil.*;

/**
 * @author wison
 */
@Service("codeGenerateCoreService")
public class CodeGenerateCoreServiceImpl {

    @Value("${code.save.path}")
    private String codeSavePath;
    @Value("${fancy.codeGenerate.basePackage}")
    private String packageName;

    private String getCodeDirPath(){
        return codeSavePath+ File.separator+"fancyCode";
    }

    void createJavaFiles(CodeGenerateTableInfo tableInfo, CodeGenerateModuleInfo moduleInfo, String modulePath) {
        String dirPath = getCodeDirPath()+ File.separator+JAVA+File.separator+modulePath;
        javaGenerate(dirPath,getPackageMap(tableInfo,moduleInfo));
    }

    private Map<String, Object> getPackageMap(CodeGenerateTableInfo tableInfo, CodeGenerateModuleInfo moduleInfo) {
        Map<String,Object> map = new HashMap<>();
        map.put("packageName",packageName);
        map.put("tableNameStr",tableInfo.getFdName());
        map.put("modelName",tableInfo.getFdModelName());
        map.put("tableName",tableInfo.getFdTableName());
        map.put("modulePath",moduleInfo.getFdModulePath());
        map.put("moduleName",moduleInfo.getFdName());
        map.put("version",VERSION);
        map.put("authorName",AUTHOR);
        map.put("email",EMAIL);
        map.put("date", DateUtil.today());
        List<CodeGenerateColumnInfo> columnInfos =  codeGenerateColumnInfoService.list(new QueryWrapper<CodeGenerateColumnInfo>().eq("fd_table_id",tableInfo.getFdId()).ne("fd_column_name","fd_id").orderByAsc("fd_order"));
        map.put("list",columnInfos);
        return map;
    }


    void createHtmlFiles(CodeGenerateTableInfo tableInfo, CodeGenerateModuleInfo moduleInfo, String modulePath) {
        String fdTableName = tableInfo.getFdTableName();
        String dirPath = getCodeDirPath()+ File.separator+HTML+File.separator+modulePath+File.separator+fdTableName;
        htmlGenerate(dirPath,getPackageMap(tableInfo,moduleInfo));
    }

    void createXmlFiles(CodeGenerateTableInfo tableInfo, CodeGenerateModuleInfo moduleInfo, String modulePath) {
        String dirPath = getCodeDirPath()+ File.separator+XML+File.separator+modulePath;
        xmlGenerate(dirPath,getPackageMap(tableInfo,moduleInfo));
    }

    void createJsFiles(CodeGenerateModuleInfo moduleInfo,List<CodeGenerateTableInfo> tableInfoList) {
        String modulePath = codeGenerateModuleInfoService.getDirPath(moduleInfo.getFdId());
        String dirPath = getCodeDirPath()+ File.separator+JS+File.separator+modulePath;
        jsGenerate(dirPath,getJsPackageMap(tableInfoList,moduleInfo));
    }

    private Map<String, Object> getJsPackageMap(List<CodeGenerateTableInfo> tableInfo, CodeGenerateModuleInfo moduleInfo) {
        Map<String,Object> map = new HashMap<>();
        map.put("modulePath",moduleInfo.getFdModulePath());
        map.put("moduleName",moduleInfo.getFdName());
        map.put("version",VERSION);
        map.put("authorName",AUTHOR);
        map.put("date", DateUtil.today());
        List<Map<String,Object>> list = new ArrayList<>();
        for(CodeGenerateTableInfo codeGenerateTableInfo:tableInfo){
            Map<String,Object> tableMap = new HashMap<>();
            tableMap.put("tableNameStr",codeGenerateTableInfo.getFdName());
            tableMap.put("modelName",codeGenerateTableInfo.getFdModelName());
            tableMap.put("tableName",codeGenerateTableInfo.getFdTableName());
            list.add(tableMap);
        }
        map.put("tableList",list);
        return map;
    }

    @Resource
    private ICodeGenerateColumnInfoService codeGenerateColumnInfoService;
    @Resource
    private ICodeGenerateModuleInfoService codeGenerateModuleInfoService;
}
