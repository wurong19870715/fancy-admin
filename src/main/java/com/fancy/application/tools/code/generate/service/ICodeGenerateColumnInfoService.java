package com.fancy.application.tools.code.generate.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.tools.code.generate.entity.CodeGenerateColumnInfo;
import com.fancy.application.tools.code.generate.vo.CodeGenerateColumnInfoVo;

import java.util.List;

public interface ICodeGenerateColumnInfoService extends IBaseService<CodeGenerateColumnInfo> {
    List<CodeGenerateColumnInfoVo> getColumnsVoByTableId(String id);
}
