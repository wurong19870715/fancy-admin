package com.fancy.application.tools.code.generate.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.tools.code.generate.entity.CodeGenerateColumnInfo;
import com.fancy.application.tools.code.generate.entity.CodeGenerateModuleInfo;
import com.fancy.application.tools.code.generate.entity.CodeGenerateTableInfo;
import com.fancy.application.tools.code.generate.mapper.CodeGenerateTableInfoMapper;
import com.fancy.application.tools.code.generate.service.ICodeGenerateColumnInfoService;
import com.fancy.application.tools.code.generate.service.ICodeGenerateModuleInfoService;
import com.fancy.application.tools.code.generate.service.ICodeGenerateTableInfoService;
import com.fancy.application.tools.code.generate.vo.CodeGenerateColumnInfoVo;
import com.fancy.application.tools.code.generate.vo.CodeGenerateTableInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author wison
 */
@Service("codeGenerateTableInfoService")
public class CodeGenerateTableInfoServiceImpl extends BaseService<CodeGenerateTableInfoMapper, CodeGenerateTableInfo> implements ICodeGenerateTableInfoService {


    @Override
    public List<CodeGenerateTableInfo> getTableInfoByModuleId(String id) {
        return list(new QueryWrapper<CodeGenerateTableInfo>().eq("fd_module_id",id));
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteTableById(String id) {
        codeGenerateColumnInfoService.remove(new QueryWrapper<CodeGenerateColumnInfo>().eq("fd_table_id",id));
        this.removeById(id);
    }



    @Override
    public void generateCodeByTableId(String id) {
        CodeGenerateTableInfo tableInfo = this.getById(id);
        String fdModuleId = tableInfo.getFdModuleId();
        CodeGenerateModuleInfo moduleInfo = codeGenerateModuleInfoService.getById(fdModuleId);
        /**
         * 创建模块基础的目录
         */
        String modulePath = codeGenerateModuleInfoService.getDirPath(fdModuleId);

        codeGenerateCoreService.createJavaFiles(tableInfo,moduleInfo,modulePath);
        codeGenerateCoreService.createHtmlFiles(tableInfo,moduleInfo,modulePath);
        codeGenerateCoreService.createXmlFiles(tableInfo,moduleInfo,modulePath);
    }

    @Override
    @Transactional(rollbackFor=Exception.class)
    public String saveTableInfo(CodeGenerateTableInfoVo codeGenerateTableInfoVo) throws Exception{
        CodeGenerateTableInfo codeGenerateTableInfo = new CodeGenerateTableInfo();
        BeanUtils.copyProperties(codeGenerateTableInfoVo,codeGenerateTableInfo);
        this.saveOrUpdate(codeGenerateTableInfo);
        List<CodeGenerateColumnInfoVo> columnInfoVos = codeGenerateTableInfoVo.getFields();
        codeGenerateColumnInfoService.remove(new QueryWrapper<CodeGenerateColumnInfo>().eq("fd_table_id",codeGenerateTableInfo.getFdId()));
        AtomicInteger i = new AtomicInteger();
        columnInfoVos.forEach(codeGenerateColumnInfoVo -> {
            CodeGenerateColumnInfo codeGenerateColumnInfo = new CodeGenerateColumnInfo();
            BeanUtils.copyProperties(codeGenerateColumnInfoVo,codeGenerateColumnInfo);
            codeGenerateColumnInfo.setFdTableId(codeGenerateTableInfo.getFdId());
            codeGenerateColumnInfo.setFdOrder(i.getAndIncrement());
            codeGenerateColumnInfoService.saveOrUpdate(codeGenerateColumnInfo);
        });
        return codeGenerateTableInfo.getFdId();
    }

    @Override
    public List<CodeGenerateColumnInfoVo> getColumnsByTableId(String id) {
        return codeGenerateColumnInfoService.getColumnsVoByTableId(id);
    }

    @Override
    public void generateJsCodeByTables(CodeGenerateModuleInfo moduleInfo,List<CodeGenerateTableInfo> tableInfoList) {
        codeGenerateCoreService.createJsFiles(moduleInfo,tableInfoList);
    }

    @Resource
    private ICodeGenerateColumnInfoService codeGenerateColumnInfoService;
    @Resource
    private ICodeGenerateModuleInfoService codeGenerateModuleInfoService;
    @Resource
    private CodeGenerateCoreServiceImpl codeGenerateCoreService;
}
