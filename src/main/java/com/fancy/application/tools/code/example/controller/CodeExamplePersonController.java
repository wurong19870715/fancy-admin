package com.fancy.application.tools.code.example.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.service.impl.SqlQueryWrapper;

import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.tools.code.example.entity.CodeExamplePerson;
import com.fancy.application.tools.code.example.service.ICodeExamplePersonService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author wison
 */
@RestController
@RequestMapping("/code/example/code_example_person")
@Log4j2
public class CodeExamplePersonController extends BaseController {
    @Resource
    private ICodeExamplePersonService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<CodeExamplePerson> basePage){
        SqlQueryWrapper<CodeExamplePerson> queryWrapper = basePage.getWrapper();
        queryWrapper.authQuery();
        IPage<CodeExamplePerson> personPage = baseService.page(basePage.getPage(),queryWrapper);
        return Result.ok(personPage);
    }

    @PostMapping(value = "/test")
    public Result test(){
        SqlQueryWrapper<CodeExamplePerson> personSqlQueryWrapper = new SqlQueryWrapper<>();
        //personSqlQueryWrapper.authQuery();
        List<CodeExamplePerson> persons = baseService.list(personSqlQueryWrapper);
        return Result.ok(persons);
    }

    @PutMapping
    public Result save(@RequestBody CodeExamplePerson codeExamplePerson){
        baseService.saveOrUpdate(codeExamplePerson);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        CodeExamplePerson person = baseService.getById(id);
        return Result.ok().addData(person);
    }

}
