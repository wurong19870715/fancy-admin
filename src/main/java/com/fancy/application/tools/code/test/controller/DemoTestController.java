package com.fancy.application.tools.code.test.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.tools.code.test.entity.DemoTest;
import com.fancy.application.tools.code.test.service.IDemoTestService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import lombok.extern.log4j.Log4j2;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2021-08-12
* email：wurong715@163.com
*/

@RestController
@RequestMapping("/code/test/demo_test")
@Log4j2
public class DemoTestController extends BaseController {

    @Resource
    private IDemoTestService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<DemoTest> basePage){
        IPage<DemoTest> demoTestPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(demoTestPage);
    }

    @PutMapping
    public Result save(@RequestBody DemoTest demoTest){
        baseService.saveOrUpdate(demoTest);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam(required = false) String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam(required = false) String id ){
        DemoTest demoTest = baseService.getById(id);
        return Result.ok().addData(demoTest);
    }
}
