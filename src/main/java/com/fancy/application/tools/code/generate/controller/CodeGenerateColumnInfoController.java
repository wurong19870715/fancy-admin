package com.fancy.application.tools.code.generate.controller;


import com.fancy.application.common.controller.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author wison
 */
@RestController
@RequestMapping("/code/generate/code_generate_column_info")
public class CodeGenerateColumnInfoController extends BaseController {

}
