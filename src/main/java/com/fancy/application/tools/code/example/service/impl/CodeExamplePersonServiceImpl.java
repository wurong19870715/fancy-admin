package com.fancy.application.tools.code.example.service.impl;

import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.tools.code.example.entity.CodeExamplePerson;
import com.fancy.application.tools.code.example.mapper.CodeExamplePersonMapper;
import com.fancy.application.tools.code.example.service.ICodeExamplePersonService;
import org.springframework.stereotype.Service;

/**
 * @author wison
 */
@Service("codeExamplePersonService")
public class CodeExamplePersonServiceImpl extends BaseService<CodeExamplePersonMapper,CodeExamplePerson> implements ICodeExamplePersonService {
}
