package com.fancy.application.tools.code.example.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.common.entity.BaseUserEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wison
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="code_example_person")
@Dictionary(modelName = "com.fancy.application.tools.code.example.entity.CodeExamplePerson",
        tableName = "code_example_person",
        serviceBean = "codeExamplePersonService",
        messageKey = "示例"
)
public class CodeExamplePerson extends BaseUserEntity {

    @TableField(value="fd_name")
    @SimpleField(messageKey = "名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;
    /**
     * 密码
     */
    @TableField(value="fd_password")
    @SimpleField(messageKey = "密码",name = "fdPassword",column = "fd_password",length = 50)
    private String fdPassword;
    /**
     * 爱好
     */
    @TableField(value="fd_hobby")
    @SimpleField(messageKey = "爱好",name = "fdHobby",column = "fd_hobby",length = 50)
    private String fdHobby;
    /**
     * 性别
     */
    @TableField(value="fd_sex")
    @SimpleField(messageKey = "爱好",name = "fdSex",column = "fd_sex",length = 50)
    private String fdSex;
    /**
     * 城市
     */
    @TableField(value="fd_city")
    @SimpleField(messageKey = "城市",name = "fdCity",column = "fd_city",length = 50)
    private String fdCity;
    /**
     * 是否有效
     */
    @TableField(value="fd_available")
    @SimpleField(messageKey = "是否有效",name = "fdAvailable",column = "fd_available",length = 10,type = TypeEnums.BOOLEAN)
    private Boolean fdAvailable = Boolean.FALSE;
    /**
     * 备注
     */
    @TableField(value="fd_remark")
    @SimpleField(messageKey = "备注",name = "fdRemark",column = "fd_remark",length = 500)
    private String fdRemark;

    //附件id
    @TableField(value = "fd_attchment_id")
    /*@AttFileModel*/
    @SimpleField(messageKey = "附件id",name = "attchmentId",column = "fd_attchment_id")
    private String attchmentId;



}
