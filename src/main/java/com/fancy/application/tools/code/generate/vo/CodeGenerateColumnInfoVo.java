package com.fancy.application.tools.code.generate.vo;

import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wison
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CodeGenerateColumnInfoVo extends BaseVo {

    private String fdName;


    /**
     * sql字段名
     */
    private String fdColumnName;

    /**
     * 表id
     */
    private String fdTableId;

    /**
     * 长度
     */
    private Integer fdLength;
    /**
     * 是否为主键
     */
    private Boolean fdIsPrimaryKey = Boolean.FALSE;

    /**
     * 字段类型
     */
    private String fdColumnType;

    /**
     * 模型名称
     */
    private String fdModelName;

    /**
     * 排序号
     */
    private Integer fdOrder;


    /**
     * 是否为必填
     */
    private Boolean fdIsRequired = Boolean.FALSE;
}
