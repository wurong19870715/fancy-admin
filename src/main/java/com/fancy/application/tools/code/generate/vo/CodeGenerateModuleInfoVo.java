package com.fancy.application.tools.code.generate.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wison
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CodeGenerateModuleInfoVo extends BaseVo {
    /**
     * 模块名称
     */
    private String fdName;

    /**
     * 备注
     */
    private String fdRemark;

    /**
     * 模块路径
     */
    private String fdModulePath;



}




