package com.fancy.application.tools.code.generate.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wison
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CodeGenerateTableInfoVo extends BaseVo {

    private String fdName;

    private String fdTableName;

    private String fdModelName;

    private String fdModuleId;

    private List<CodeGenerateColumnInfoVo> fields = new ArrayList<>();


}




