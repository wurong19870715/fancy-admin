package com.fancy.application.tools.code.generate.controller;


import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.tools.code.generate.entity.CodeGenerateTableInfo;
import com.fancy.application.tools.code.generate.service.ICodeGenerateTableInfoService;
import com.fancy.application.tools.code.generate.vo.CodeGenerateTableInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author wison
 */
@RestController
@RequestMapping("/code/generate/code_generate_table_info")
public class CodeGenerateTableInfoController extends BaseController {

    @Resource
    private ICodeGenerateTableInfoService baseService;

    @GetMapping(value = "/tables")
    public Result list(@RequestParam(value = "fdModuleId",required = false) String id ){
        return Result.ok(baseService.getTableInfoByModuleId(id));
    }

    @PostMapping
    public Result save(@RequestBody CodeGenerateTableInfoVo codeGenerateTableInfoVo) throws Exception {
        String fdId = baseService.saveTableInfo(codeGenerateTableInfoVo);
        return view(fdId);
    }

    @DeleteMapping
    public Result del(@RequestParam String id ){
        baseService.deleteTableById(id);
        return Result.ok();
    }

    @GetMapping
    public Result view(@RequestParam String id ){
        CodeGenerateTableInfoVo codeGenerateTableInfoVo = new CodeGenerateTableInfoVo();
        CodeGenerateTableInfo codeGenerateTableInfo = baseService.getById(id);
        BeanUtils.copyProperties(codeGenerateTableInfo,codeGenerateTableInfoVo);
        codeGenerateTableInfoVo.setFields(baseService.getColumnsByTableId(id));
        return Result.ok().addData(codeGenerateTableInfoVo);
    }
}
