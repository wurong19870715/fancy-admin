package com.fancy.application.tools.code.example.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wison
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class CodeExamplePersonVo extends BaseVo  {

    private String fdName;
    /**
     * 密码
     */
    private String fdPassword;
    /**
     * 爱好
     */
    private String[] fdHobby;
    /**
     * 性别
     */
    private String fdSex;
    /**
     * 城市
     */
    private String fdCity;
    /**
     * 是否有效
     */
    private Boolean fdAvailable = Boolean.FALSE;
    /**
     * 备注
     */
    private String fdRemark;

    //附件id
    private String attchmentId;


}
