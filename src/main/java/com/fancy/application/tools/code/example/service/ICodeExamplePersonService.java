package com.fancy.application.tools.code.example.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.tools.code.example.entity.CodeExamplePerson;

public interface ICodeExamplePersonService extends IBaseService<CodeExamplePerson> {

}
