package com.fancy.application.tools.code.generate.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.tools.code.generate.entity.CodeGenerateModuleInfo;
import com.fancy.application.tools.code.generate.entity.CodeGenerateTableInfo;
import com.fancy.application.tools.code.generate.vo.CodeGenerateColumnInfoVo;
import com.fancy.application.tools.code.generate.vo.CodeGenerateTableInfoVo;

import java.util.List;

public interface ICodeGenerateTableInfoService extends IBaseService<CodeGenerateTableInfo> {
    List<CodeGenerateTableInfo> getTableInfoByModuleId(String id);

    public void deleteTableById(String id);

    void generateCodeByTableId(String id);

    String saveTableInfo(CodeGenerateTableInfoVo codeGenerateTableInfoVo) throws Exception;

    List<CodeGenerateColumnInfoVo> getColumnsByTableId(String id);

    void generateJsCodeByTables(CodeGenerateModuleInfo moduleInfo,List<CodeGenerateTableInfo> tableInfoList);
}
