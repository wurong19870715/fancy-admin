package com.fancy.application.tools.code.generate.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;

import com.fancy.application.tools.code.generate.entity.CodeGenerateModuleInfo;
import com.fancy.application.tools.code.generate.service.ICodeGenerateModuleInfoService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author wison
 */
@RestController
@RequestMapping("/code/generate/code_generate_module_info")
public class CodeGenerateModuleInfoController extends BaseController {

    @Resource
    private ICodeGenerateModuleInfoService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<CodeGenerateModuleInfo> basePage){
        IPage<CodeGenerateModuleInfo> generateModuleInfoIPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(generateModuleInfoIPage);
    }

    @PutMapping
    public Result save(@RequestBody CodeGenerateModuleInfo codeGenerateModuleInfo){
        baseService.saveOrUpdate(codeGenerateModuleInfo);
        return Result.ok();
    }

    @DeleteMapping
    public Result del(@RequestParam String id ){
        baseService.deleteModuleById(id);
        return Result.ok();
    }



}
