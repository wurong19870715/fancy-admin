package com.fancy.application.tools.code.generate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.tools.code.generate.entity.CodeGenerateColumnInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CodeGenerateColumnInfoMapper extends BaseMapper<CodeGenerateColumnInfo> {
}
