package com.fancy.application.tools.code.generate.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wison
 */
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="code_generate_module_info")
@Dictionary(modelName = "com.fancy.application.tools.code.generate.entity.CodeGenerateModuleInfo",
        tableName = "code_generate_module_info",
        serviceBean = "codeGenerateModuleInfoService",
        messageKey = "代码生成模块信息"
)
public class CodeGenerateModuleInfo extends BaseEntity {
    /**
     * 模块名称
     */
    @TableField("fd_name")
    @SimpleField(messageKey = "模块名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;

    /**
     * 备注
     */
    @TableField("fd_remark")
    @SimpleField(messageKey = "备注",name = "fdRemark",column = "fd_remark",length = 1000)
    private String fdRemark;

    /**
     * 模块路径
     */
    @TableField("fd_module_path")
    @SimpleField(messageKey = "模块路径",name = "fdModulePath",column = "fd_module_path",length = 200)
    private String fdModulePath;



}




