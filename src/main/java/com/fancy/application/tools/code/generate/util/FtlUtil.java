package com.fancy.application.tools.code.generate.util;

import freemarker.template.*;

import java.io.*;
import java.util.Map;

public class FtlUtil {
    /**
     *
     * @param propMap			放入模板中的值，对应
     * @param templatePath		模板目录的路径
     * @param templateName		模板名称
     * @return
     */
    public static String generateTemplate(Map<String,Object> propMap, String templatePath, String templateName){
        String content = null;
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
        Template temp = null;
        StringWriter out = new StringWriter();
        try {
            cfg.setDefaultEncoding("UTF-8");
            //cfg.setTemplateLoader();
            cfg.setDirectoryForTemplateLoading(new File(templatePath));  	//设置模板目录
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
            //cfg.setObjectWrapper(new DefaultObjectWrapper(DefaultObjectWrapper.));
            temp = cfg.getTemplate(templateName);  			//得到模板名字
            temp.process(propMap, out);  					//生成模板内容
            content = out.toString();						//生成字符串
        }catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }catch (TemplateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }finally{
            if(out!=null){
                try {
                    out.close();
                    out = null;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return content;
    }
}
