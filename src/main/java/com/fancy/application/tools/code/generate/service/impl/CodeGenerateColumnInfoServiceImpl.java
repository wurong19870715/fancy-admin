package com.fancy.application.tools.code.generate.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.tools.code.generate.entity.CodeGenerateColumnInfo;
import com.fancy.application.tools.code.generate.mapper.CodeGenerateColumnInfoMapper;
import com.fancy.application.tools.code.generate.service.ICodeGenerateColumnInfoService;
import com.fancy.application.tools.code.generate.vo.CodeGenerateColumnInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author wison
 */
@Service("codeGenerateColumnInfoService")
public class CodeGenerateColumnInfoServiceImpl extends BaseService<CodeGenerateColumnInfoMapper, CodeGenerateColumnInfo> implements ICodeGenerateColumnInfoService {

    @Override
    public List<CodeGenerateColumnInfoVo> getColumnsVoByTableId(String id) {
        List<CodeGenerateColumnInfo> list = this.list(new QueryWrapper<CodeGenerateColumnInfo>().eq("fd_table_id",id).orderByAsc("fd_order"));
        List<CodeGenerateColumnInfoVo> vos = new ArrayList<>();
        list.forEach(codeGenerateColumnInfo -> {
            CodeGenerateColumnInfoVo vo = new CodeGenerateColumnInfoVo();
            BeanUtils.copyProperties(codeGenerateColumnInfo,vo);
            vos.add(vo);
        });
        return vos;
    }
}
