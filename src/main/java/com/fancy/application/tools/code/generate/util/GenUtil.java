package com.fancy.application.tools.code.generate.util;

import cn.hutool.core.io.FileUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ClassUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wison
 */
@Log4j2
public class GenUtil {
    private static final String TEMPLATE_PATH="templates/code/generate/ftl";
    public static final String JAVA="java";
    public static final String HTML="html";
    public static final String JS="js";
    public static final String XML="xml";
    public static final String SQL="sql";




    public static final String AUTHOR="wison";
    public static final String EMAIL="wurong715@163.com";
    public static final String VERSION="1.5";


    /** 类型转换 */
    public static Map<String, String> javaTypeMap = new HashMap<>();

    static
    {
        javaTypeMap.put("Integer","int");
        javaTypeMap.put("int","int");
//        javaTypeMap.put("Long","bigint");
        //javaTypeMap.put("Float", "float");            //暂不启用
        javaTypeMap.put("Double", "double");
        javaTypeMap.put("Boolean", "bit");
        //javaTypeMap.put("decimal", "BigDecimal");     //暂不启用
        javaTypeMap.put("String", "varchar");
        javaTypeMap.put("Date", "datetime");
    }

    private static List<String[]> getTemplates(){
        List<String[]> templates = new ArrayList<>(10);
        String fdPrefixName = "fancy_template";
        String templateJavaPath = getPath()+GenUtil.TEMPLATE_PATH+File.separator+JAVA+File.separator;
        String templateHtmlPath = getPath()+GenUtil.TEMPLATE_PATH+File.separator+HTML+File.separator;
        String templateJsPath = getPath()+GenUtil.TEMPLATE_PATH+File.separator+JS+File.separator;
        String templateXmlPath =  getPath()+GenUtil.TEMPLATE_PATH+File.separator+XML+File.separator;
        templates.add(new String[]{templateJavaPath,fdPrefixName+"_controller.java.ftl"});
        templates.add(new String[]{templateJavaPath,fdPrefixName+"_entity.java.ftl"});
        templates.add(new String[]{templateJavaPath,fdPrefixName+"_interface.java.ftl"});
        templates.add(new String[]{templateJavaPath,fdPrefixName+"_service.java.ftl"});
        templates.add(new String[]{templateJavaPath,fdPrefixName+"_mapper.java.ftl"});
        templates.add(new String[]{templateJavaPath,fdPrefixName+"_vo.java.ftl"});
        templates.add(new String[]{templateHtmlPath,fdPrefixName+"_list.vue.ftl"});
        templates.add(new String[]{templateHtmlPath,fdPrefixName+"_edit.vue.ftl"});
        templates.add(new String[]{templateHtmlPath,fdPrefixName+"_view.vue.ftl"});
        templates.add(new String[]{templateJsPath,fdPrefixName+"_index.js.ftl"});
        templates.add(new String[]{templateXmlPath,fdPrefixName+"_mapper.xml.ftl"});
        return templates;
    }

    public static String getPath(){
        return ClassUtils.getDefaultClassLoader().getResource("").getPath();
    }

    public static void javaGenerate(String dirPath,Map<String,Object> map) {
        List<String[]> templates = GenUtil.getTemplates();
        templates.forEach(template->{
            if(template[1].contains("controller.java")){
                String file = dirPath+File.separator+"controller"+File.separator+map.get("modelName")+"Controller.java";       //生成文件路径
                generate(file,template,map);
            }else if(template[1].contains("entity.java")){
                String file = dirPath+File.separator+"entity"+File.separator+map.get("modelName")+".java";
                generate(file,template,map);
            }else if(template[1].contains("interface.java")){
                String file = dirPath+File.separator+"service"+File.separator+"I"+map.get("modelName")+"Service.java";
                generate(file,template,map);
            }else if(template[1].contains("service.java")){
                String file = dirPath+File.separator+"service"+File.separator+"impl"+File.separator+map.get("modelName")+"ServiceImpl.java";
                generate(file,template,map);
            } else if(template[1].contains("mapper.java")){
                String file = dirPath+File.separator+"mapper"+File.separator+map.get("modelName")+"Mapper.java";
                generate(file,template,map);
            } else if(template[1].contains("vo.java")){
                String file = dirPath+File.separator+"vo"+File.separator+map.get("modelName")+"Vo.java";
                generate(file,template,map);
            }
        });
    }

    public static void htmlGenerate(String dirPath,Map<String,Object> map) {
        List<String[]> templates = GenUtil.getTemplates();
        templates.forEach(template->{
            if(template[1].contains("list.vue")){
                String file = dirPath+File.separator+map.get("modelName")+"_list.vue";
                generate(file,template,map);
            }else if(template[1].contains("view.vue")){
                String file = dirPath+File.separator+map.get("modelName")+"_view.vue";
                generate(file,template,map);
            }else if(template[1].contains("edit.vue")){
                String file = dirPath+File.separator+map.get("modelName")+"_edit.vue";
                generate(file,template,map);
            }
        });
    }

    /**
     * js需要特殊处理,合并成一个文件
     * @param dirPath
     * @param map
     */
    public static void jsGenerate(String dirPath,Map<String,Object> map) {
        List<String[]> templates = GenUtil.getTemplates();
        templates.forEach(template->{
            if(template[1].contains("index.js")){
                String file = dirPath+File.separator+"index.js";
                generate(file,template,map);
            }
        });
    }
    public static void xmlGenerate(String dirPath,Map<String,Object> map) {
        List<String[]> templates = GenUtil.getTemplates();
        templates.forEach(template->{
            if(template[1].contains("mapper.xml")){
                String file = dirPath+File.separator+map.get("modelName")+"Mapper.xml";
                generate(file,template,map);
            }
        });
    }

    private static void generate(String file,String[] template,Map<String,Object> map){
        String templateStr = FtlUtil.generateTemplate(map,template[0],template[1]);
        FileUtil.del(file);
        log.debug("file:{}",file);
        FileUtil.appendUtf8String(templateStr,file);
    }

}
