package com.fancy.application.tools.code.test.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.tools.code.test.entity.DemoTest;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2021-08-12
* email：wurong715@163.com
*/

public interface IDemoTestService extends IBaseService<DemoTest> {

}
