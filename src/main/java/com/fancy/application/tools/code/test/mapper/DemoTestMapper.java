package com.fancy.application.tools.code.test.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.tools.code.test.entity.DemoTest;
import org.apache.ibatis.annotations.Mapper;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2021-08-12
* email：wurong715@163.com
*/

@Mapper
public interface DemoTestMapper extends BaseMapper<DemoTest> {
}
