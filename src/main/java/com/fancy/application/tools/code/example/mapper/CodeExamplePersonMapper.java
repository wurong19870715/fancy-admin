package com.fancy.application.tools.code.example.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.tools.code.example.entity.CodeExamplePerson;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CodeExamplePersonMapper extends BaseMapper<CodeExamplePerson> {
}
