package com.fancy.application.tools.code.generate.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.tools.code.generate.entity.CodeGenerateTableInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CodeGenerateTableInfoMapper extends BaseMapper<CodeGenerateTableInfo> {
}
