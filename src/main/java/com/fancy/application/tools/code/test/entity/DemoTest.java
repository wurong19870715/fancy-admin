package com.fancy.application.tools.code.test.entity;

import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* @Date 2021-08-12
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="demo_test")
@Dictionary(modelName = "com.fancy.application.code.test.entity.DemoTest",
tableName = "demo_test",
serviceBean = "demoTestService",
messageKey = "测试"
)
public class DemoTest extends BaseEntity {

    /**
    * 测试
    */
    @TableField(value="fd_test")
    @SimpleField(messageKey = "测试",name = "fdTest",column = "fd_test",length = 36,type = TypeEnums.BOOLEAN)
    private String fdTest;

}
