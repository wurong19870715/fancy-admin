package com.fancy.application.tools.code.test.service.impl;

import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.tools.code.test.entity.DemoTest;
import com.fancy.application.tools.code.test.mapper.DemoTestMapper;
import com.fancy.application.tools.code.test.service.IDemoTestService;

import org.springframework.stereotype.Service;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2021-08-12
* email：wurong715@163.com
*/

@Service("demoTestService")
public class DemoTestServiceImpl extends BaseService<DemoTestMapper,DemoTest> implements IDemoTestService {
}
