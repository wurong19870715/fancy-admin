package com.fancy.application.tools.code.generate.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author wison
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName(value="code_generate_table_info")
@Dictionary(modelName = "com.fancy.application.tools.code.generate.entity.CodeGenerateTableInfo",
        tableName = "code_generate_table_info",
        serviceBean = "codeGenerateTableInfoService",
        messageKey = "代码生成表信息"
)
public class CodeGenerateTableInfo extends BaseEntity {


    /**
     * 表名称
     */
    @TableField("fd_name")
    @SimpleField(messageKey = "名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;


    /**
     * sql表名
     */
    @TableField("fd_table_name")
    @SimpleField(messageKey = "sql表名",name = "fdTableName",column = "fd_table_name",length = 50)
    private String fdTableName;

    /**
     * model名称
     */
    @TableField("fd_model_name")
    @SimpleField(messageKey = "model名称",name = "fdModelName",column = "fd_model_name",length = 200)
    private String fdModelName;


    /**
     * 模块路径
     */
    @TableField("fd_module_id")
    @SimpleField(messageKey = "模块路径",name = "fdModuleId",column = "fd_module_id",length = 200)
    private String fdModuleId;

}




