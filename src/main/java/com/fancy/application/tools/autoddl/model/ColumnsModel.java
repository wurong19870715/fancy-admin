package com.fancy.application.tools.autoddl.model;

public class ColumnsModel extends BaseModel {

    private Long length = 36L; //长度
    private boolean isPrimaryKey = false;   //是否为主键
    private String columnType = "varchar";  //字段类型
    private boolean isNull = true; //是否能为空
    private String defaultValue = "";//默认值
    //private boolean isIndex = false;    //是否为索引字段(暂不启用)


    public Long getLength() {
        return length;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        isPrimaryKey = primaryKey;
    }

    public String getColumnType() {
        return columnType;
    }

    public void setColumnType(String columnType) {
        this.columnType = columnType;
    }

    public boolean isNull() {
        return isNull;
    }

    public void setNull(boolean aNull) {
        isNull = aNull;
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }
}
