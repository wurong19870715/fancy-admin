package com.fancy.application.tools.autoddl.model;

public class BaseModel {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
