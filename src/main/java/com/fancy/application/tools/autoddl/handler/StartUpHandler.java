package com.fancy.application.tools.autoddl.handler;

import cn.hutool.core.util.StrUtil;
import com.fancy.application.common.utils.SpringBeanUtil;
import com.fancy.application.sys.config.service.ISysConfigMainService;
import com.fancy.application.tools.autoddl.constants.DataBaseTypeEnum;
import com.fancy.application.tools.autoddl.constants.DatabaseOptEnum;
import com.fancy.application.tools.autoddl.service.IDatabaseTableManageService;
import lombok.extern.log4j.Log4j2;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.init.ScriptUtils;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

import static com.fancy.application.tools.autoddl.constants.Constants.AUTO_KEY;
import static com.fancy.application.tools.autoddl.constants.Constants.DATABASE_TYPE;

@Log4j2
@Component
public class StartUpHandler implements IStartUpHandler {
    @Resource
    private Environment env;
    @Resource
    private IDatabaseTableManageService mysqlTableManageService;
    @Resource
    private DataSource datasource;
    @Resource
    private ISysConfigMainService sysConfigMainService;

    @Override
    @PostConstruct
    public void handler() {
        //获取
        String auto_key = env.getProperty(AUTO_KEY);
        log.debug("获取到配置文件的参数{}",auto_key);
        if(StrUtil.equalsIgnoreCase(DatabaseOptEnum.NONE.name(),auto_key)){
            log.debug("无需检查表结构...");
        }else{
            log.debug("需要检查表结构...");
            /**
             * 数据库类型
             */
            String databaseType = env.getProperty(DATABASE_TYPE);
            if(StrUtil.isBlank(databaseType)){
                databaseType = DataBaseTypeEnum.MYSQL.name();
            }
            if(StrUtil.equalsIgnoreCase(databaseType,DataBaseTypeEnum.MYSQL.name())){
                log.debug("数据库类型为mysql");
                mysqlTableManageService.createTables();
            }else if (StrUtil.equalsIgnoreCase(databaseType,DataBaseTypeEnum.SQLSERVER.name())){

                log.debug("数据库类型为sqlserver");
            }else if (StrUtil.equalsIgnoreCase(databaseType,DataBaseTypeEnum.ORACLE.name())){
                log.debug("数据库类型为oracle");

            }else{
                log.warn("未找到适配的数据库类型，请检查。。");
            }

        }
        //初始化脚本
        initSQLScripts();
    }

    private void initSQLScripts()  {
        String initStatus = sysConfigMainService.getInitStatus();
        if(StrUtil.isBlank(initStatus)||StrUtil.equals(initStatus,"false")){
            log.debug("开始初始化数据..");
            String classPath = "classpath:";
            final String QUARTZ_SQL = classPath +"config/scheme/quartz.mysql.sql";
            String databaseType = env.getProperty(DATABASE_TYPE);
            String INIT_SQL = classPath +"config/scheme/init."+databaseType+".sql";
            executeSqlScript(QUARTZ_SQL,INIT_SQL);
            //初始化数据
            //executeSQL(schemeFile);
            log.debug("数据初始化完毕...");
            //写入配置表中
            sysConfigMainService.updateInitStatus();
        }

    }

    private void executeSqlScript(String... SCHEME_SQL){
        for (String sql : SCHEME_SQL) {
            org.springframework.core.io.Resource resource = SpringBeanUtil.getApplicationContext().getResource(sql);
            ScriptUtils.executeSqlScript(getConnection(), resource);
        }
    }

    private Connection getConnection(){
        Connection connection = null;
        try {
            connection = datasource.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            log.warn("获取连接失败");
        }
        return connection;
    }


}
