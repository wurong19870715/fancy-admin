package com.fancy.application.tools.autoddl.constants;

public class Constants {
    //@Autowired private Environment env;
    /**
     * #是否开启建表模式，默认为none，可选update
     */
    public final static String AUTO_KEY = "database.auto.ddl";

    /**
     * 数据库类型
     */
    public static String DATABASE_TYPE = "database.type";

}
