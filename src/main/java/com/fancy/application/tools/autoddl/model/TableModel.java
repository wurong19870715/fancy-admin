package com.fancy.application.tools.autoddl.model;

import cn.hutool.core.util.StrUtil;

import java.util.ArrayList;
import java.util.List;

public class TableModel extends BaseModel {

    private List<ColumnsModel> columnsModelList;

    public TableModel() {
        this.columnsModelList = new ArrayList<>();
    }

    public List<ColumnsModel> getColumnsModelList() {
        return columnsModelList;
    }

    public TableModel(String tableName) {
        this.columnsModelList = new ArrayList<>();
        this.name = tableName;
    }



}
