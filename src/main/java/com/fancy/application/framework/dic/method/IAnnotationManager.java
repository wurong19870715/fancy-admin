package com.fancy.application.framework.dic.method;

import com.fancy.application.framework.dic.model.DictionaryObj;

/**
 * Created by wison on 17/3/22.
 */
public interface IAnnotationManager {

    public DictionaryObj getDicObj(Class<?> clazz);
}
