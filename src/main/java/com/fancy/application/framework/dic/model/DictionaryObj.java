package com.fancy.application.framework.dic.model;

import cn.hutool.core.util.StrUtil;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.ListField;
import com.fancy.application.framework.dic.annotation.ModelField;
import com.fancy.application.framework.dic.annotation.SimpleField;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by wison on 17/3/22.
 */
public final class DictionaryObj extends FancyAbStractDictionaryObj{

    private final String modelName;
    private final String messageKey;
    private final String tableName;
    private final String serviceBean;
    private final boolean exist;

    public DictionaryObj(Dictionary d, List<SimpleField> simpleFieldList, List<ModelField> modelFieldList, List<ListField> listFieldList) {
        this.modelName = d.modelName();
        this.disPlayName = d.disPlayName();
        this.messageKey = StrUtil.isNotBlank(d.messageKey())?getMessage(d.messageKey()):"";
        this.tableName = d.tableName();
        this.serviceBean = d.serviceBean();
        this.exist = d.exist();
        simpleFieldList.forEach(SimpleField->this.simpleFieldList.add(new DictionarySimpleFieldObj(SimpleField)));
//        if(modelFieldList!=null)
//            modelFieldList.forEach(ModelField->this.modelfieldList.add(new DictionaryModelFieldObj(ModelField)));
//        if(listFieldList!=null)
//            listFieldList.forEach(ListField->this.listFieldList.add(new DictionaryListFieldObj(ListField)));
    }



    public String getModelName() {
        return modelName;
    }


    public String getMessageKey() {

        return messageKey;
    }


    public String getTableName() {
        return tableName;
    }


    public String getServiceBean() {
        return serviceBean;
    }


    public String getDisPlayName() {
        return disPlayName;
    }

    private final String disPlayName;


    /**
     * 废弃model和list
     * @return
     */
//    private final List<DictionaryModelFieldObj> modelfieldList = new ArrayList<>();
//
//    private final List<DictionaryListFieldObj> listFieldList = new ArrayList<>();

    public List<DictionarySimpleFieldObj> getSimpleFieldList() {
        return simpleFieldList;
    }

//    public List<DictionaryListFieldObj> getListFieldList() {
//        return listFieldList;
//    }
//
//    public List<DictionaryModelFieldObj> getModelfieldList() {
//        return modelfieldList;
//    }

    private final List<DictionarySimpleFieldObj> simpleFieldList = new ArrayList<>();

    public boolean isExist() {
        return exist;
    }
}
