package com.fancy.application.framework.dic.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 *17年写的一部分数据字典的代码，终于在2020/09/09日从代码堆里面翻出来使用了，所以所有的积累，都会有回报，加油！
 * Created by wison on 17/3/20.
 */

/**
 * 数据字典
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Dictionary {

    public String modelName();
    public String messageKey() default "";
    public String tableName() default "";
    public String serviceBean() default "";
    public String disPlayName() default "";
    public String desc() default "";
    public String extendClass() default "";
    public boolean exist() default true;
}
