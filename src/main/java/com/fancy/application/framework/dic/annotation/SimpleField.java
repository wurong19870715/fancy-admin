package com.fancy.application.framework.dic.annotation;

import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.enums.TypeEnums;

import java.lang.annotation.*;


/**
 * Created by wison on 17/3/20.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface SimpleField {
    public String name() default"";
    public String messageKey() default "";
    public TypeEnums type() default TypeEnums.STRING;
    public String column() default "";
    public int length()default 36;
    public boolean pk() default false;  //是否为主键
    public boolean exist() default true;    //是否为数据库字段
    public Class<?> relationModel() default BaseEntity.class;   //关联的class
    public String relationField() default "fdId";   //关联的字段
    public Class<?> relationList() default Object.class;    //关联的
    public String desc() default "";
    public boolean nullable() default true;    //是否为空
}
