package com.fancy.application.framework.dic.model;

import cn.hutool.core.util.StrUtil;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.framework.dic.enums.TypeEnums;

/**
 * Created by wison on 17/3/22.
 */
public final class DictionarySimpleFieldObj extends FancyAbStractDictionaryObj {

    private final String name;
    private final String messageKey;
    private final TypeEnums type;
    private final String column;
    private final long length ;
    private final boolean nullable ;
    private final boolean exist;

    private final String relationModelName;

    public boolean isPk() {
        return pk;
    }

    public String getName() {
        return name;
    }

    public String getMessageKey() {
        return messageKey;
    }

    public TypeEnums getType() {
        return type;
    }

    public String getColumn() {
        return column;
    }

    public long getLength() {
        return length;
    }

    private final boolean pk;


    public DictionarySimpleFieldObj(SimpleField simpleField) {
        this.name = StrUtil.isNotBlank(simpleField.name())?simpleField.name():"";
        this.messageKey = StrUtil.isNotBlank(simpleField.messageKey())?getMessage(simpleField.messageKey()):"";
        this.type = simpleField.type();
        this.column = StrUtil.isNotBlank(simpleField.column())?simpleField.column():"";
        this.length = simpleField.length()>0?simpleField.length():36;
        this.pk = simpleField.pk();
        this.nullable = simpleField.nullable();
        this.exist = simpleField.exist();
        this.relationModelName = simpleField.relationModel().getName();
    }

    public boolean isNullable() {
        return nullable;
    }

    public boolean isExist() {
        return exist;
    }

    public String getRelationModelName() {
        return relationModelName;
    }
}
