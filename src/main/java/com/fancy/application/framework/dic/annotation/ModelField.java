package com.fancy.application.framework.dic.annotation;

import com.fancy.application.framework.dic.enums.TypeEnums;

import java.lang.annotation.*;

/**
 * 暂时不启用
 * Created by wison on 17/3/22.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface ModelField {
    public String name();
    public String messageKey() default "";
    public TypeEnums type() default TypeEnums.MODEL;
    public String table();
    public String column();
    public boolean nullable() default false;
}
