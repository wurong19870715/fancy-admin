package com.fancy.application.framework.dic.method;

import com.fancy.application.framework.dic.annotation.*;
import com.fancy.application.framework.dic.model.DictionaryObj;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by wison on 17/3/22.
 */
@Service
public final class AnnotationManager implements IAnnotationManager {


    @Override
    public DictionaryObj getDicObj(Class<?> clazz) {
        boolean hasAnnotation = clazz.isAnnotationPresent(Dictionary.class);
        if(hasAnnotation){
            Dictionary d = clazz.getAnnotation(Dictionary.class);

            List<SimpleField> simpleFields = new ArrayList<>();
            List<ModelField> modelFields = new ArrayList<>();
            List<ListField> listFields = new ArrayList<>();
            for (Class<?> superClass = clazz; superClass != Object.class; superClass = superClass.getSuperclass()) {
                for(Field field: superClass.getDeclaredFields()){
                    SimpleField simpleField = field.getAnnotation(SimpleField.class);
                    if(simpleField!=null)
                        simpleFields.add(simpleField);
                    ModelField modelField = field.getAnnotation(ModelField.class);
                    if(modelField!=null)
                        modelFields.add(modelField);
                    ListField listField = field.getAnnotation(ListField.class);
                    if(listField!=null)
                        listFields.add(listField);
                }
            }
            return new DictionaryObj(d,simpleFields,modelFields,listFields);
        }
        return null;
    }
}
