package com.fancy.application.framework.dic.model;

import cn.hutool.core.util.StrUtil;

/**
 * Created by wison on 17/3/22.
 */
public abstract class FancyAbStractDictionaryObj {

    String getMessage(String message){
        if(StrUtil.isNotBlank(message)){
            return message;
        }else{
            return "";
        }
    }
    String replaceNull(String text){
        return StrUtil.isNotBlank(text)?text:"";
    }
}
