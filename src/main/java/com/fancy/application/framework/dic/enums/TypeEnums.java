package com.fancy.application.framework.dic.enums;

/**
 * Created by wison on 17/3/20.
 */
public enum TypeEnums {
    STRING,INTEGER,LONG,DOUBLE,DATE,DATETIME,BOOLEAN,LIST,MODEL,JSON,LONGTEXT
}
