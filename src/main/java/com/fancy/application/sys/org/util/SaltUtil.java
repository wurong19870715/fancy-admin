package com.fancy.application.sys.org.util;

import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.SimpleHash;

public final class SaltUtil {

    private final static SecureRandomNumberGenerator secureRandomNumberGenerator = new SecureRandomNumberGenerator();

    public static String getSaltStr(){
        return  SaltUtil.secureRandomNumberGenerator.nextBytes().toHex();
    }

    public static String generatePassword(String password, String salt) {
        SimpleHash hash = new SimpleHash("MD5", password,salt,2);
        return hash.toString();
    }

/*    public static void main(String[] args) {
        System.out.println(IdUtil.fastSimpleUUID());
        String salt="adminc06c9868005e4b2bac8f315ff5c285b0";
        String password="1";
        System.out.println(generatePassword(password,salt));
    }*/
}
