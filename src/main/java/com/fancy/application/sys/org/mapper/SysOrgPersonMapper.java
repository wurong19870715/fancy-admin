package com.fancy.application.sys.org.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import org.apache.ibatis.annotations.Mapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/
@Mapper
public interface SysOrgPersonMapper extends BaseMapper<SysOrgPerson> {
}
