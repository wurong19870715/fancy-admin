package com.fancy.application.sys.org.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/@EqualsAndHashCode(callSuper = true)
@Data
public class SysOrgPostPersonRelationVo extends BaseVo {
        /**
        * 岗位id
        */
            private String fdPostId;
        /**
        * 人员id
        */
            private String fdPersonId;

}
