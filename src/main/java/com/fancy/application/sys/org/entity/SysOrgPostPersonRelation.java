package com.fancy.application.sys.org.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/

@Data
@TableName(value="sys_org_post_person_relation")
@Dictionary(modelName = "com.fancy.application.sys.org.entity.SysOrgPostPersonRelation",
        tableName = "sys_org_post_person_relation",
        serviceBean = "sysOrgPostPersonRelationService",
        messageKey = "岗位人员关联"
)
public class SysOrgPostPersonRelation  {

        /**
        * 岗位id
        */
        @SimpleField(messageKey = "岗位id",name = "fdPostId",column = "fd_post_id",relationModel =SysOrgPost.class,relationField = "fdId")
        @TableField(value="fd_post_id")
        private String fdPostId;
        /**
        * 人员id
        */
        @SimpleField(messageKey = "人员id",name = "fdPersonId",column = "fd_person_id",relationModel =SysOrgPerson.class,relationField = "fdId")
        @TableField(value="fd_person_id")
        private String fdPersonId;

        /**
         * 岗位名称
         */
        @SimpleField(messageKey = "岗位名称",name = "fdPostName",column = "fd_post_name",relationModel =SysOrgPost.class,relationField = "fdName")
        @TableField(value="fd_post_name")
        private String fdPostName;

}
