package com.fancy.application.sys.org.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysOrgElementVo extends SysOrgModelVo {
    /**
    * 领导名称
    */
    private String fdLeaderName;
    /**
    * 上级名称
    */
    private String fdParentName;

}
