package com.fancy.application.sys.org.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.org.entity.SysOrgDept;
import com.fancy.application.sys.org.entity.SysOrgModel;
import com.fancy.application.sys.org.entity.SysOrgPost;
import com.fancy.application.sys.org.mapper.SysOrgPostMapper;
import com.fancy.application.sys.org.service.ISysOrgDeptService;
import com.fancy.application.sys.org.service.ISysOrgPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/
@Service("sysOrgPostService")
public class SysOrgPostServiceImpl extends BaseService<SysOrgPostMapper,SysOrgPost> implements ISysOrgPostService {
    @Autowired
    private ISysOrgDeptService sysOrgDeptService;
    @Override
    public void savePost(SysOrgPost sysOrgPost) {
        String parentDeptId = sysOrgPost.getFdParentId();
        if(StrUtil.isEmpty(sysOrgPost.getFdId())){
            sysOrgPost.setFdId(IdUtil.simpleUUID());
        }
        if(StrUtil.isNotEmpty(parentDeptId)){
            SysOrgDept dept = sysOrgDeptService.getById(parentDeptId);
            sysOrgPost.setFdParentOrgId(dept.getFdParentOrgId());
            sysOrgPost.setFdParentOrgName(dept.getFdParentOrgName());
            String fdHierarchyId  = dept.getFdHierarchyId();

            if(StrUtil.isEmpty(fdHierarchyId)){
                fdHierarchyId += sysOrgPost.getFdId();
            }else{
                fdHierarchyId = fdHierarchyId+"x"+sysOrgPost.getFdId();
            }
            sysOrgPost.setFdHierarchyId(fdHierarchyId);
        }else{
            sysOrgPost.setFdHierarchyId(sysOrgPost.getFdId());
        }
        this.saveOrUpdate(sysOrgPost);
    }
}
