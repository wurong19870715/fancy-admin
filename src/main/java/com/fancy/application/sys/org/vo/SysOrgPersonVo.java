package com.fancy.application.sys.org.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysOrgPersonVo extends SysOrgModelVo {

    /**
    * 盐
    */
    private String fdSalt;

    /**
    * 登录名
    */
    private String fdLoginName;
    /**
    * 钉钉号
    */
    private String fdDingId;
    /**
    * 密码
    */
    private String fdPassword;
    /**
    * 组织架构名称
    */
    private String fdOrgName;
    /**
    * 微信号
    */
    private String fdWechatNo;
    /**
    * 生日
    */
    private String fdBirthDay;
    /**
    * 组织架构id
    */
    private String fdOrgId;

    /**
     * 岗位id
     */
    private String fdPostIds;
    /**
     * 岗位名称
     */
    private String fdPostNames;

    /**
     * 手机号
     */
    private String fdMobileNo;
    /**
     * 固定电话
     */
    private String fdTelPhone;

}
