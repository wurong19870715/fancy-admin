package com.fancy.application.sys.org.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.org.entity.*;
import com.fancy.application.sys.org.mapper.SysOrgElementMapper;
import com.fancy.application.sys.org.service.*;

import java.util.ArrayList;
import java.util.List;

import static com.fancy.application.sys.org.util.OrgUtil.*;

import com.fancy.application.sys.org.util.OrgUtil;
import com.fancy.application.sys.org.vo.OrgTreeView;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
 * 岗位暂时不考虑
* @author wison
* 2019-09-07
*/
@Service("sysOrgElementService")
public class SysOrgElementServiceImpl extends BaseService<SysOrgElementMapper,SysOrgElement> implements ISysOrgElementService {

  @Resource
  private ISysOrgDeptService sysOrgDeptService;
  @Resource
  private ISysOrgPersonService sysOrgPersonService;
  @Resource
  private ISysOrgCoreService sysOrgCoreService;
  @Resource
  private ISysOrgPostService sysOrgPostService;



  @Override
  public Object getTreeData(String fdParentId,String fdParentType, String fdOrgType) {
    if(StrUtil.isEmpty(fdOrgType)){
      fdOrgType = "ORG"; //默认只有机构
    }
    List<OrgTreeView> treeViews = new ArrayList<>();
    if(StrUtil.isEmpty(fdParentId)) {
      if(StrUtil.containsAny(fdOrgType,ORG.toString())) { //机构
        List<SysOrgElement> elementList = getElements(fdParentId);
        elementList.forEach(sysOrgElement -> treeViews.add(new OrgTreeView(sysOrgElement.getFdId(), sysOrgElement.getFdName(), sysOrgElement.getFdOrgType())));
      }
      if(StrUtil.containsAny(fdOrgType,DEPT.toString())){ //部门
        List<SysOrgDept> deptList = getDepts(fdParentId,ORG);
        deptList.forEach(sysOrgDept -> treeViews.add(new OrgTreeView(sysOrgDept.getFdId(),sysOrgDept.getFdName(),sysOrgDept.getFdOrgType())));
      }
    }else{
      int _fdParentType = NumberUtil.parseInt(fdParentType);
      OrgUtil ou = getOrgTypeByValue(_fdParentType);
      if(ou!=null&&ou.toString().equals(ORG.toString())){//机构的话,需要同时查询机构和部门
        if(StrUtil.containsAny(fdOrgType,ORG.toString())) { //机构
          List<SysOrgElement> elementList = getElements(fdParentId);
          elementList.forEach(sysOrgElement -> treeViews.add(new OrgTreeView(sysOrgElement.getFdId(),sysOrgElement.getFdName(),sysOrgElement.getFdOrgType())));
        }
        if(StrUtil.containsAny(fdOrgType,DEPT.toString())){ //部门
          List<SysOrgDept> deptList = getDepts(fdParentId,ou);
          deptList.forEach(sysOrgDept -> treeViews.add(new OrgTreeView(sysOrgDept.getFdId(),sysOrgDept.getFdName(),sysOrgDept.getFdOrgType())));
        }
      }else{    //部门的话,只需要查询部门就可以
        if(StrUtil.containsAny(fdOrgType,DEPT.toString())){ //部门
          List<SysOrgDept> deptList = getDepts(fdParentId);
          deptList.forEach(sysOrgDept -> treeViews.add(new OrgTreeView(sysOrgDept.getFdId(),sysOrgDept.getFdName(),sysOrgDept.getFdOrgType())));
        }
      }
    }
    return treeViews;
    
    
  }

  /**
   * 查询列表的显示的内容,是机构\部门\人员,岗位暂时不考虑
   * @param fdParentId  左边click的id
   * @param fdParentType  左边click的类型
   * @param fdOrgType   列表展示的类型
   * @return
   */
  @Override
  public Object getListData(String fdParentId, String fdParentType, String fdOrgType) {

    List<OrgTreeView> treeViews = new ArrayList<>();
    if(StrUtil.isEmpty(fdParentId)) {
      if(StrUtil.containsAny(fdOrgType,ORG.toString())){ //机构
        List<SysOrgElement> elementList = getElements(fdParentId);
        elementList.forEach(sysOrgElement -> treeViews.add(new OrgTreeView(sysOrgElement.getFdId(),sysOrgElement.getFdName(),sysOrgElement.getFdOrgType())));
      }
      if(StrUtil.containsAny(fdOrgType,DEPT.toString())){ //部门
        List<SysOrgDept> deptList = getDepts(fdParentId);
        deptList.forEach(sysOrgDept -> treeViews.add(new OrgTreeView(sysOrgDept.getFdId(),sysOrgDept.getFdName(),sysOrgDept.getFdOrgType())));
      }
      if(StrUtil.containsAny(fdOrgType,POST.toString())){  //岗位
        List<SysOrgPost> postList = getPosts(fdParentId);
        postList.forEach(SysOrgPost -> treeViews.add(new OrgTreeView(SysOrgPost.getFdId(),SysOrgPost.getFdName(),SysOrgPost.getFdOrgType())));
      }
      if(StrUtil.containsAny(fdOrgType,PERSON.toString())){  //人员
        List<SysOrgPerson> personList = getPersons(fdParentId);
        personList.forEach(sysOrgPerson -> treeViews.add(new OrgTreeView(sysOrgPerson.getFdId(),sysOrgPerson.getFdName(),sysOrgPerson.getFdOrgType())));
      }
      return treeViews;
    }else{
      int _fdParentType = NumberUtil.parseInt(fdParentType);
      OrgUtil ou = getOrgTypeByValue(_fdParentType);
      if(ou!=null&&ou.toString().equals(ORG.toString())){//机构,需要判断列表是否显示机构\部门\人员
        if(StrUtil.containsAny(fdOrgType,ORG.toString())){ //机构
          List<SysOrgElement> elementList = getElements(fdParentId);
          elementList.forEach(sysOrgElement -> treeViews.add(new OrgTreeView(sysOrgElement.getFdId(),sysOrgElement.getFdName(),sysOrgElement.getFdOrgType())));
        }
        if(StrUtil.containsAny(fdOrgType,DEPT.toString())){ //部门
          List<SysOrgDept> deptList = getDepts(fdParentId,ou);
          deptList.forEach(sysOrgDept -> treeViews.add(new OrgTreeView(sysOrgDept.getFdId(),sysOrgDept.getFdName(),sysOrgDept.getFdOrgType())));
        }
        if(StrUtil.containsAny(fdOrgType,PERSON.toString())){  //人员
          List<SysOrgPerson> deptList = getPersons(fdParentId);
          deptList.forEach(sysOrgPerson -> treeViews.add(new OrgTreeView(sysOrgPerson.getFdId(),sysOrgPerson.getFdName(),sysOrgPerson.getFdOrgType())));
        }
      }else{    //部门的话,只需要查询部门就可以
        if(StrUtil.containsAny(fdOrgType,DEPT.toString())){ //部门
          List<SysOrgDept> deptList = getDepts(fdParentId);
          deptList.forEach(sysOrgDept -> treeViews.add(new OrgTreeView(sysOrgDept.getFdId(),sysOrgDept.getFdName(),sysOrgDept.getFdOrgType())));
        }
        if(StrUtil.containsAny(fdOrgType,POST.toString())){  //岗位
          List<SysOrgPost> postList = getPosts(fdParentId);
          postList.forEach(SysOrgPost -> treeViews.add(new OrgTreeView(SysOrgPost.getFdId(),SysOrgPost.getFdName(),SysOrgPost.getFdOrgType())));
        }
        if(StrUtil.containsAny(fdOrgType,PERSON.toString())){  //人员
          List<SysOrgPerson> deptList = getPersons(fdParentId);
          deptList.forEach(sysOrgPerson -> treeViews.add(new OrgTreeView(sysOrgPerson.getFdId(),sysOrgPerson.getFdName(),sysOrgPerson.getFdOrgType())));
        }
      }
    }

    return treeViews;
  }

  private List<SysOrgPost> getPosts(String fdParentId) {
    QueryWrapper<SysOrgPost> postWrapper = new QueryWrapper<>();
    postWrapper.eq("fd_parent_id", fdParentId);
    postWrapper.eq("fd_is_avaliable",true);
    return sysOrgPostService.list(postWrapper);
  }

  private List<SysOrgDept> getDepts(String fdParentId) {
    QueryWrapper<SysOrgDept> deptWrapper = new QueryWrapper<>();
    deptWrapper.eq("fd_is_avaliable",true);
    deptWrapper.eq("fd_parent_id", fdParentId);

    return sysOrgDeptService.list(deptWrapper);
  }

  /**
   *
   * @param fdParentId
   * @param fdParentType
   * @return
   */
  private List<SysOrgDept> getDepts(String fdParentId, OrgUtil fdParentType) {
    QueryWrapper<SysOrgDept> deptWrapper = new QueryWrapper<>();
    deptWrapper.eq("fd_is_avaliable",true);
    deptWrapper.eq("fd_parent_id", fdParentId);
    return sysOrgDeptService.list(deptWrapper);
  }

  private List<SysOrgElement> getElements(String fdParentId){
    QueryWrapper<SysOrgElement> elementWrapper = new QueryWrapper<>();
    elementWrapper.eq("fd_is_avaliable",true);
    elementWrapper.eq("fd_parent_id", fdParentId);
    return this.list(elementWrapper);
  }

  private List<SysOrgPerson> getPersons(String fdParentId){
    QueryWrapper<SysOrgPerson> personWrapper = new QueryWrapper<>();
    personWrapper.eq("fd_parent_id", fdParentId).ne("fd_login_name","admin");
    return sysOrgPersonService.list(personWrapper);
  }

  @Override
  public boolean save(SysOrgElement entity) {
    return super.save(entity);
  }


  @Override
  public boolean saveOrUpdate(SysOrgElement sysOrgElement){
    String fdParentId = sysOrgElement.getFdParentId();
    if(StrUtil.isEmpty(sysOrgElement.getFdId())){
      sysOrgElement.setFdId(IdUtil.simpleUUID());
    }
    String fdHierarchyId = "";
    if(StrUtil.isNotEmpty(fdParentId)) {
      SysOrgModel model = this.getById(fdParentId);
      fdHierarchyId = model.getFdHierarchyId();
    }
    if(StrUtil.isEmpty(fdHierarchyId)){
      fdHierarchyId += sysOrgElement.getFdId();
    }else{
      fdHierarchyId = fdHierarchyId+"x"+sysOrgElement.getFdId();
    }
    sysOrgElement.setFdHierarchyId(fdHierarchyId);

    return super.saveOrUpdate(sysOrgElement);
  }
}
