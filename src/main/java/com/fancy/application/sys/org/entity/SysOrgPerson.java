package com.fancy.application.sys.org.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.constant.Constants;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_org_person")
@Dictionary(modelName = "com.fancy.application.sys.org.entity.SysOrgPerson",
        tableName = "sys_org_person",
        extendClass = "com.fancy.application.sys.org.SysOrgModel",
        serviceBean = "sysOrgPersonService",
        messageKey = "人员"
)
public class SysOrgPerson extends SysOrgModel {


        /**
         * 组织架构类型
         * 1.机构
         * 2.部门
         * 3.岗位
         * 4.人员
         */
        public SysOrgPerson() {
            super(4);
        }

        /**
        * 盐
        */
        @TableField(value="fd_salt")
        @SimpleField(messageKey = "盐值",name = "fdSalt",column = "fd_salt",desc = "和密码相关，无实际作用")
        private String fdSalt;

        /**
        * 登录名
        */
        @TableField(value="fd_login_name")
        @SimpleField(messageKey = "登录名",name = "fdLoginName",column = "fd_login_name",length = 50)
        private String fdLoginName;

        /**
        * 钉钉id
        */
        @TableField(value="fd_ding_id")
        @SimpleField(messageKey = "钉钉id",name = "fdDingId",column = "fd_ding_id",length = 50)
        private String fdDingId;

        /**
        * 密码
        */
        @TableField(value="fd_password")
        @SimpleField(messageKey = "密码",name = "fdPassword",column = "fd_password",length = 50)
        private String fdPassword;

        /**
        * 组织架构名称
        */
        @TableField(value="fd_org_name")
        @SimpleField(messageKey = "组织架构名称",name = "fdOrgName",column = "fd_org_name",length = 50)
        private String fdOrgName;
        /**
        * 微信号
        */
        @TableField(value="fd_wechat_no")
        @SimpleField(messageKey = "微信号",name = "fdWechatNo",column = "fd_wechat_no",length = 50)
        private String fdWechatNo;
        /**
        * 生日
        */
        @TableField(value="fd_birth_day")
        @SimpleField(messageKey = "生日",name = "fdBirthDay",column = "fd_birth_day",length = 20)
        @JsonFormat(pattern= Constants.FORMAT_DATE)
        private Date fdBirthDay;
        /**
        * 组织架构id
        */
        @SimpleField(messageKey = "组织架构id",name = "fdOrgId",column = "fd_org_id",length = 20)
        @TableField(value="fd_org_id")
        private String fdOrgId;

        /**
         * 密码盐.
         * @return
         */
        public String getCredentialsSalt(){
            return this.fdLoginName+this.fdSalt;
        }

        /**
         * SAP工号
         */
        @SimpleField(messageKey = "SAP工号",name = "fdSapNo",column = "fd_sap_no",length = 50)
        @TableField(value="fd_sap_no")
        private String fdSapNo;

        /**
         * 手机号
         */
        @SimpleField(messageKey = "手机号",name = "fdMobileNo",column = "fd_mobile_no",length = 20)
        @TableField(value="fd_mobile_no")
        private String fdMobileNo;

        /**
         * 固定电话
         */
        @SimpleField(messageKey = "固定电话",name = "fdTelPhone",column = "fd_tel_phone",length = 20)
        @TableField(value="fd_tel_phone")
        private String fdTelPhone;
}
