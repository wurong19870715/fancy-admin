package com.fancy.application.sys.org.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.org.entity.SysOrgPost;
import com.fancy.application.sys.org.service.ISysOrgPostService;
import com.fancy.application.sys.org.vo.SysOrgModelPage;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/@RestController
@RequestMapping("/sys/org/sys_org_post")
public class SysOrgPostController extends BaseController {
@Resource
private ISysOrgPostService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody SysOrgModelPage<SysOrgPost> basePage){
        QueryWrapper<SysOrgPost> wrapper = basePage.getWrapper();
        wrapper.eq("fd_is_avaliable",true);
        wrapper.eq("fd_parent_id",basePage.getFdParentId());
        IPage<SysOrgPost> sysOrgPersonPage = baseService.page(basePage.getPage(),wrapper);
        return Result.ok(sysOrgPersonPage);
    }

    @PutMapping
    public Result save(@RequestBody SysOrgPost sysOrgPost){
        baseService.savePost(sysOrgPost);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysOrgPost sysOrgPost = baseService.getById(id);
        return Result.ok().addData(sysOrgPost);
    }
}
