package com.fancy.application.sys.org.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.org.entity.SysOrgPost;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/public interface ISysOrgPostService extends IBaseService<SysOrgPost> {

    void savePost(SysOrgPost sysOrgPost);
}
