package com.fancy.application.sys.org.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.org.entity.SysOrgDept;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/public interface ISysOrgDeptService extends IBaseService<SysOrgDept> {

    void saveOrUpdateDept(SysOrgDept sysOrgDept);
}
