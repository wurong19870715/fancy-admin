package com.fancy.application.sys.org.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_org_post")
@Dictionary(modelName = "com.fancy.application.sys.org.entity.SysOrgPost",
        tableName = "sys_org_post",
        extendClass = "com.fancy.application.sys.org.SysOrgModel",
        serviceBean = "sysOrgPostService",
        messageKey = "岗位"
)
public class SysOrgPost extends SysOrgModel {


    /**
     * 组织架构类型
     * 1.机构
     * 2.部门
     * 3.岗位
     * 4.人员
     */
    public SysOrgPost() {
        super(3);
    }

    /**
    * 上级机构名称
    */
    @TableField(value="fd_parent_org_name")
    @SimpleField(messageKey = "上级机构名称",name = "fdParentOrgName",column = "fd_parent_org_name",relationModel =SysOrgElement.class,relationField = "fdName")
    private String fdParentOrgName;

    /**
    * 上级机构id
    */
    @TableField(value="fd_parent_org_id")
    @SimpleField(messageKey = "上级机构id",name = "fdParentOrgId",column = "fd_parent_org_id",relationModel =SysOrgElement.class,relationField = "fdId")
    private String fdParentOrgId;

}
