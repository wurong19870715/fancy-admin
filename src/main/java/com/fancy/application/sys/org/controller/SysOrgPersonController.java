package com.fancy.application.sys.org.controller;


import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.utils.UserUtil;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import com.fancy.application.sys.org.service.ISysOrgPersonService;
import com.fancy.application.sys.org.vo.PasswordVo;
import com.fancy.application.sys.org.vo.SysOrgModelPage;
import com.fancy.application.sys.org.vo.SysOrgPersonVo;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;

/**
* fancyCode自动生成v1.0
* @author wison
* 2019-09-07
*/

@Log4j2
@RestController
@RequestMapping("/sys/org/sys_org_person")
public class SysOrgPersonController extends BaseController {

    /**
     *  附件存放基础路径
     */
    @Value("${fancy.att.resources}")
    private String fdAttPath;

    @Resource
    private ISysOrgPersonService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody SysOrgModelPage<SysOrgPerson> basePage){
        QueryWrapper<SysOrgPerson> wrapper = basePage.getWrapper();
        wrapper.eq("fd_is_avaliable",true);
        if(StrUtil.isNotBlank(basePage.getFdParentId())){
            wrapper.eq("fd_parent_id",basePage.getFdParentId());
        }
        wrapper.orderByAsc("fd_create_time");
        IPage<SysOrgPerson> sysOrgPersonPage = baseService.page(basePage.getPage(),wrapper);
        return Result.ok(sysOrgPersonPage);
    }

    @PutMapping
    public Result save(@RequestBody SysOrgPersonVo sysOrgPersonVo){
        baseService.savePerson(sysOrgPersonVo);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysOrgPersonVo sysOrgPersonVo = new SysOrgPersonVo();
        SysOrgPerson sysOrgPerson = baseService.getById(id);
        BeanUtils.copyProperties(sysOrgPerson,sysOrgPersonVo);
        baseService.addPostInfo(sysOrgPersonVo);  //增加岗位信息
        return Result.ok().addData(sysOrgPersonVo);
    }

    /**
     * 上传图片
     */
    @PostMapping("/uploadUserImg")
    public Result uploadUserImg(@RequestParam("avatarfile") MultipartFile file) throws IOException {
        baseService.saveUserImg(file);
        return Result.ok().addData("success");
    }

    /**
     * 获取用户头像
     */
    @GetMapping("/getUserImg")
    public ResponseEntity<InputStreamResource> getUserImg() {
        String fdLoginName = UserUtil.getCurrentUser().getFdLoginName();
        String imgPath = fdAttPath+File.separator+"userImg"+File.separator+ fdLoginName +".jpg";  //头像路径

        File file = FileUtil.file(imgPath);
        if(file.exists()){
            InputStreamResource resource = baseService.getUserImg(file,fdLoginName);
            if(resource != null){
                HttpHeaders headers = new HttpHeaders();
                headers.add("Content-Disposition", String.format("attachment;filename=\"%s", fdLoginName +".jpg"));
                headers.add("Cache-Control", "no-cache,no-store,must-revalidate");
                headers.add("Pragma", "no-cache");
                headers.add("Expires", "0");
                return ResponseEntity.ok()
                        .headers(headers)
                        .contentLength(file.length())
                        .contentType(MediaType.parseMediaType("application/octet-stream"))
                        .body(resource);
            }else{
                // 没有找到文件
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    //管理员修改密码
    @PutMapping("/superResetPwd")
    public Result superResetPwd(@RequestBody PasswordVo passwordVo){
        baseService.superResetPwd(passwordVo);
        return Result.ok();
    }

    @PutMapping("/resetPwd")
    public Result resetPwd(@RequestBody PasswordVo passwordVo){
        baseService.resetPwd(passwordVo);
        return Result.ok();
    }

    /**
     * 获取用户基本信息
     */
    @GetMapping("/getUserInfo")
    public Result getUserInfo() {
        return Result.ok().addData(UserUtil.getCurrentUser());
    }
}
