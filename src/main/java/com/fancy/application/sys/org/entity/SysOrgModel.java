package com.fancy.application.sys.org.entity;

import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 部门只能挂在机构下面,或者人员岗位为空
 * 人员和岗位只能挂在部门下面,或者人员岗位为空
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Dictionary(modelName = "com.fancy.application.sys.org.entity.SysOrgModel",
        tableName = "sys_org_model",
        serviceBean = "sysOrgCoreService",
        messageKey = "组织架构基表",
        desc="组织架构虚拟表，sys_org_model为视图",
        exist=false
)
@TableName("sys_org_model")
public class SysOrgModel extends BaseEntity {
    /**
     * 编号
     */
    @TableField(value="fd_no")
    @SimpleField(messageKey = "编号",name = "fdNo",column = "fd_no",length = 50)
    private String fdNo;

    /**
     * 名称
     */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;

    /**
     * 排序号
     */
    @TableField(value="fd_order")
    @SimpleField(messageKey = "排序号",name = "fdOrder",column = "fd_order",type = TypeEnums.INTEGER)
    private Integer fdOrder;

    /**
     * 是否有效
     */
    @TableField(value="fd_is_avaliable")
    @SimpleField(messageKey = "排序号",name = "fdOrder",column = "fd_is_avaliable",type = TypeEnums.BOOLEAN)
    private Boolean fdIsAvaliable = Boolean.FALSE;

    /**
     * 上级id
     */
    @TableField(value="fd_parent_id")
    @SimpleField(messageKey = "上级id",name = "fdParentId",column = "fd_parent_id",relationModel = SysOrgModel.class,relationField = "fdId")
    private String fdParentId;
    /**
     * 上级名称
     */
    @TableField(value="fd_parent_name")
    @SimpleField(messageKey = "上级名称",name = "fdParentName",column = "fd_parent_name",relationModel = SysOrgModel.class,relationField = "fdName")
    private String fdParentName;

    /**
     * 层级id
     */
    @TableField(value="fd_hierarchy_id")
    @SimpleField(messageKey = "上级名称",name = "fdHierarchyId",column = "fd_hierarchy_id",length =1000)
    private String fdHierarchyId;

    @TableField(exist = false)
    @SimpleField(messageKey = "机构类型",name = "fdOrgType",column = "fd_org_type",exist = false)
    private Integer fdOrgType;

    public SysOrgModel(Integer fdOrgType) {
        this.fdOrgType = fdOrgType;
        this.setFdId(IdUtil.simpleUUID());
    }

    public SysOrgModel() {}
}
