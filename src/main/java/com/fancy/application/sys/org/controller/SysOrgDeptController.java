package com.fancy.application.sys.org.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.org.entity.SysOrgDept;
import com.fancy.application.sys.org.entity.SysOrgElement;
import com.fancy.application.sys.org.service.ISysOrgDeptService;
import com.fancy.application.sys.org.service.ISysOrgElementService;
import com.fancy.application.sys.org.vo.SysOrgDeptVo;
import com.fancy.application.sys.org.vo.SysOrgModelPage;
import com.fancy.application.sys.org.vo.SysOrgModelVo;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/@RestController
@RequestMapping("/sys/org/sys_org_dept")
public class SysOrgDeptController extends BaseController {
    @Resource
    private ISysOrgDeptService baseService;
    @Resource
    private ISysOrgElementService sysOrgElementService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody SysOrgModelPage<SysOrgDept> basePage){
        QueryWrapper<SysOrgDept> wrapper = basePage.getWrapper();
        wrapper.eq("fd_is_avaliable",true);
        wrapper.eq("fd_parent_id",basePage.getFdParentId());
        IPage<SysOrgDept> sysOrgDeptPage = baseService.page(basePage.getPage(),wrapper);
        return Result.ok(sysOrgDeptPage);
    }

    @PutMapping
    public Result save(@RequestBody SysOrgDept sysOrgDept){
        baseService.saveOrUpdateDept(sysOrgDept);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        SysOrgDept element = baseService.getById(id);
        element.setFdIsAvaliable(Boolean.FALSE);
        baseService.saveOrUpdate(element);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id){
        SysOrgModelVo sysOrgModelVo = new SysOrgDeptVo();
        SysOrgDept sysOrgDept = baseService.getById(id);
        if(sysOrgDept == null){
            SysOrgElement element = sysOrgElementService.getById(id);
            BeanUtils.copyProperties(element,sysOrgModelVo);
        }else{
            BeanUtils.copyProperties(sysOrgDept,sysOrgModelVo);
        }
        return Result.ok().addData(sysOrgModelVo);
    }
}
