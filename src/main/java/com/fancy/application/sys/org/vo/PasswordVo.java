package com.fancy.application.sys.org.vo;

import lombok.Data;

@Data
public class PasswordVo {
    private String userId;
    private String oldPassword;
    private String newPassword;
}
