package com.fancy.application.sys.org.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/
@EqualsAndHashCode(callSuper = true)
@Data
public class SysOrgDeptVo extends SysOrgModelVo {
        /**
        * 领导名称
        */
        private String fdLeaderName;
        /**
        * 上级机构id
        */
        private String fdParentOrgId;
        /**
        * 上级机构名称
        */
        private String fdParentOrgName;
        /**
        * 领导id
        */
        private String fdLeaderId;

}
