package com.fancy.application.sys.org.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/
@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_org_element")
@Dictionary(modelName = "com.fancy.application.sys.org.entity.SysOrgElement",
        tableName = "sys_org_element",
        serviceBean = "sysOrgElementService",
        messageKey = "组织架构机构表",
        desc="组织架构机构表"
)
public class SysOrgElement extends SysOrgModel {


    /**
     * 组织架构类型
     * 1.机构
     * 2.部门
     * 3.岗位
     * 4.人员
     */
    public SysOrgElement() {
        super(1);
    }

    /**
    * 领导id
    */
    @TableField(value="fd_leader_id")
    @SimpleField(messageKey = "领导id",name = "fdLeaderId",column = "fd_leader_id",relationModel =SysOrgPerson.class,relationField = "fdId")
    private String fdLeaderId;
    /**
    * 领导名称
    */
    @TableField(value="fd_leader_name")
    @SimpleField(messageKey = "领导姓名",name = "fdLeaderName",column = "fd_leader_name",relationModel =SysOrgPerson.class,relationField = "fdName")
    private String fdLeaderName;




}
