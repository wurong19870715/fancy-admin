package com.fancy.application.sys.org.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.org.entity.SysOrgPostPersonRelation;
import com.fancy.application.sys.org.vo.SysOrgPersonVo;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/public interface ISysOrgPostPersonRelationService extends IBaseService<SysOrgPostPersonRelation> {

    void saveRelations(String personId,String fdPostIds, String fdPostNames);

    void getRelationInfoByUserId(String userId, SysOrgPersonVo sysOrgPersonVo);
}
