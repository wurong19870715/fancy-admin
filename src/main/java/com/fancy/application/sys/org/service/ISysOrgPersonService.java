package com.fancy.application.sys.org.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.auth.vo.LoginForm;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import com.fancy.application.sys.org.vo.PasswordVo;
import com.fancy.application.sys.org.vo.SysOrgPersonVo;
import org.springframework.core.io.InputStreamResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/
public interface ISysOrgPersonService extends IBaseService<SysOrgPerson> {



    void savePerson(SysOrgPersonVo sysOrgPersonVo);

    SysOrgPerson getPersonByLoginInfo(LoginForm loginForm);

    SysOrgPerson getPersonByToken(String token);

    void saveUserImg(MultipartFile file) throws IOException;

    InputStreamResource getUserImg(File fdLoginName, String loginName);

    void resetPwd(PasswordVo passwordVo);

    void addPostInfo(SysOrgPersonVo sysOrgPersonVo);

    List<String> getResByUserId(String fdId);

    void superResetPwd(PasswordVo passwordVo);
}
