package com.fancy.application.sys.org.controller;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.org.entity.SysOrgPostPersonRelation;
import com.fancy.application.sys.org.service.ISysOrgPostPersonRelationService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/@RestController
@RequestMapping("/sys/org/sys_org_post_person_relation")
public class SysOrgPostPersonRelationController extends BaseController {
@Resource
private ISysOrgPostPersonRelationService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysOrgPostPersonRelation> basePage){
        IPage<SysOrgPostPersonRelation> sysOrgPostPersonRelationPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(sysOrgPostPersonRelationPage);
    }

    @PutMapping
    public Result save(@RequestBody SysOrgPostPersonRelation sysOrgPostPersonRelation){
        baseService.saveOrUpdate(sysOrgPostPersonRelation);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysOrgPostPersonRelation sysOrgPostPersonRelation = baseService.getById(id);
        return Result.ok().addData(sysOrgPostPersonRelation);
    }
}
