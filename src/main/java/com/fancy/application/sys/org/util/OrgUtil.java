package com.fancy.application.sys.org.util;

public enum OrgUtil {
    ORG(1),DEPT(2),POST(3),PERSON(4);

    private int orgTypeVal;

    private OrgUtil(int orgType) {
        this.orgTypeVal = orgType;
    }

    public static OrgUtil getOrgTypeByValue(int type){
        Integer length = OrgUtil.values().length;
        for (OrgUtil c : OrgUtil.values()) {
            if (c.getOrgTypeVal() == type) {
                return c;
            }
        }
        return null;
    }
    public int getOrgTypeVal() {
        return orgTypeVal;
    }

    public void setOrgTypeVal(int orgTypeVal) {
        this.orgTypeVal = orgTypeVal;
    }
}
