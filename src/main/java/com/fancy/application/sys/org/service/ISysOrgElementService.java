package com.fancy.application.sys.org.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.org.entity.SysOrgElement;
import com.fancy.application.sys.org.mapper.SysOrgElementMapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/

public interface ISysOrgElementService extends IBaseService<SysOrgElement> {


  public Object getTreeData(String fdParentId,String fdParentType, String fdOrgType);

  Object getListData(String fdParentId, String fdParentType, String fdOrgType);
}
