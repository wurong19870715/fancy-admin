package com.fancy.application.sys.org.vo;

import cn.hutool.core.util.StrUtil;
import com.fancy.application.sys.auth.vo.TreeView;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class OrgTreeView extends TreeView {


    public OrgTreeView(String id, String name,Integer orgType) {
        super(id, name);
        this.orgType = orgType;
    }

    private Integer orgType;

    @Override
    public boolean equals(Object obj) {
        if(obj==null){
            return false;
        }
        if(this==obj){
            return true;
        }
        if(obj instanceof OrgTreeView){
            OrgTreeView otv=(OrgTreeView)obj;
            if(otv.getOrgType() == this.getOrgType() && StrUtil.equals(otv.getId(),this.getId())&&StrUtil.equals(otv.getName(),otv.getName())){
                return true;
            }else{
                return false;
            }
        }
        return false;
    }
}
