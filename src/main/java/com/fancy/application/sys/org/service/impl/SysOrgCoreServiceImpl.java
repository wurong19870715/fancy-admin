package com.fancy.application.sys.org.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.org.entity.SysOrgModel;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import com.fancy.application.sys.org.mapper.SysOrgModelMapper;
import com.fancy.application.sys.org.service.ISysOrgCoreService;
import com.fancy.application.sys.org.service.ISysOrgDeptService;
import com.fancy.application.sys.org.service.ISysOrgElementService;
import com.fancy.application.sys.org.service.ISysOrgPersonService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


@Service("sysOrgCoreService")
public class SysOrgCoreServiceImpl extends BaseService<SysOrgModelMapper,SysOrgModel> implements ISysOrgCoreService {
    @Resource
    private ISysOrgElementService sysOrgElementService;

    @Resource
    private ISysOrgDeptService sysOrgDeptService;

    @Resource
    private ISysOrgPersonService sysOrgPersonService;
    @Resource
    private SysOrgModelMapper sysOrgModelMapper;


    @Override
    public List<String> expandToPersonIds(List<String> orgList) {
        QueryWrapper<SysOrgPerson> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fd_is_avaliable",Boolean.TRUE);
        queryWrapper.and(wrapper -> {
            AtomicInteger index = new AtomicInteger();
            int size = orgList.size();
            orgList.forEach(s -> {
                wrapper.like("fd_hierarchy_id",s);
                if(index.get() != size-1){
                    wrapper.or();
                }
                index.getAndIncrement();
            });
        });

        List<SysOrgPerson> personList = sysOrgPersonService.list(queryWrapper);
        return personList.stream().map(SysOrgPerson::getFdId).distinct().collect(Collectors.toList());
    }

    /**
     * 根据id列表获取
     * @param orgIds
     * @return
     */
    @Override
    public List<SysOrgModel> getSysOrgModelList(List<String> orgIds) {
        Collection<SysOrgModel> sysOrgModels = this.listByIds(orgIds);
        return CollectionUtil.list(Boolean.FALSE,sysOrgModels);
    }

    @Override
    public SysOrgModel getSysOrgModelById(String sysOrgModelId) {
        SysOrgModel model = this.getById(sysOrgModelId);
        return model;
    }


}
