package com.fancy.application.sys.org.service;

import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.org.entity.SysOrgModel;

import java.util.List;

/**
 * Created by wison on 2019/10/14.
 */
public interface ISysOrgCoreService extends IBaseService<SysOrgModel> {
    List<String> expandToPersonIds(List<String> orgList);

    List<SysOrgModel> getSysOrgModelList(List<String> orgIds);
    SysOrgModel getSysOrgModelById(String sysOrgModelId);
}
