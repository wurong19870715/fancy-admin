package com.fancy.application.sys.org.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.org.entity.SysOrgModel;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysOrgModelMapper extends BaseMapper<SysOrgModel> {
    public SysOrgModel getSysOrgModelById(String fdId);
}
