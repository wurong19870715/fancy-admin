package com.fancy.application.sys.org.vo;

import com.fancy.application.common.vo.BasePage;
import com.fancy.application.sys.org.entity.SysOrgElement;
import lombok.Data;
import lombok.EqualsAndHashCode;

@EqualsAndHashCode(callSuper = true)
@Data
public class SysOrgModelPage<T> extends BasePage<T> {
    private String fdParentId;
    private String fdOrgType;
}
