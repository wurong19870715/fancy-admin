package com.fancy.application.sys.org.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.org.entity.SysOrgElement;
import com.fancy.application.sys.org.service.ISysOrgElementService;
import com.fancy.application.sys.org.vo.SysOrgModelPage;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/@RestController
@RequestMapping("/sys/org/sys_org_element")
public class SysOrgElementController extends BaseController {
@Resource
private ISysOrgElementService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody SysOrgModelPage<SysOrgElement> basePage){
        QueryWrapper<SysOrgElement> wrapper = basePage.getWrapper();
        wrapper.eq("fd_is_avaliable",true);
        wrapper.eq("fd_parent_id",basePage.getFdParentId());
        IPage<SysOrgElement> sysOrgElementPage = baseService.page(basePage.getPage(),wrapper);
        return Result.ok(sysOrgElementPage);
    }

    @PutMapping
    public Result save(@RequestBody SysOrgElement sysOrgElement){
        baseService.saveOrUpdate(sysOrgElement);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam String id ){
        SysOrgElement element = baseService.getById(id);
        element.setFdIsAvaliable(Boolean.FALSE);
        baseService.saveOrUpdate(element);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysOrgElement sysOrgElement = baseService.getById(id);
        return Result.ok().addData(sysOrgElement);
    }
    
    /**
     * 获取组织架构选择器左侧树的数据
     * @param fdParentId
     * @param fdOrgType
     * @return 
     */
    @GetMapping("/treeData")
    public Result treeData(@RequestParam(required = false) String fdParentId,
                           @RequestParam(required = false) String fdParentType,
                            @RequestParam(required = false) String fdOrgType) {
      return Result.ok().addData(baseService.getTreeData(fdParentId,fdParentType, fdOrgType));
    }

    /**
     * 获取组织架构选择器列表内的的数据
     * @param fdParentId
     * @param fdOrgType
     * @return
     */
    @GetMapping("/listData")
    public Result listData(@RequestParam String fdParentId,
                           @RequestParam String fdParentType,
                            @RequestParam String fdOrgType) {
      return Result.ok().addData(baseService.getListData(fdParentId,fdParentType, fdOrgType));
    }
}
