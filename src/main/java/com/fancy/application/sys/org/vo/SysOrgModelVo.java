package com.fancy.application.sys.org.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/
@EqualsAndHashCode(callSuper = true)
@Data
public class SysOrgModelVo extends BaseVo {

        /**
        * 编号
        */
        private String fdNo;
        /**
        * 名称
        */
        private String fdName;
        /**
        * 是否有效
        */
        private String fdIsAvaliable;
        /**
         * 排序号
         */
        private Integer fdOrder;

        /**
         * 上级id
         */
        private String fdParentId;

        /**
         * 上级名称
         */
        private String fdParentName;

        /**
         * 层级id
         */
        private String fdHierarchyId;

        /**
         * 类型
         */
        private Integer fdOrgType;


}
