package com.fancy.application.sys.org.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.org.entity.SysOrgPostPersonRelation;
import com.fancy.application.sys.org.mapper.SysOrgPostPersonRelationMapper;
import com.fancy.application.sys.org.service.ISysOrgPostPersonRelationService;
import com.fancy.application.sys.org.vo.SysOrgPersonVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-07
*/
@Service("sysOrgPostPersonRelationService")
public class SysOrgPostPersonRelationServiceImpl extends BaseService<SysOrgPostPersonRelationMapper,SysOrgPostPersonRelation> implements ISysOrgPostPersonRelationService {
    @Override
    public void saveRelations(String fdPersonId, String fdPostIds, String fdPostNames) {
        List<String> ids = StrUtil.split(fdPostIds,';');
        List<String> names = StrUtil.split(fdPostNames,';');
        QueryWrapper<SysOrgPostPersonRelation> relationQueryWrapper = new QueryWrapper<>();
        relationQueryWrapper.eq("fd_person_id",fdPersonId);
        //List<SysOrgPostPersonRelation> relations = this.list(relationQueryWrapper);
        this.remove(relationQueryWrapper);  //清空原来的数据
        for (int i=0;i<ids.size();i++){
            String postId = ids.get(i);
            String postName = names.get(i);
            SysOrgPostPersonRelation relation = new SysOrgPostPersonRelation();
            relation.setFdPostId(postId);
            relation.setFdPersonId(fdPersonId);
            relation.setFdPostName(postName);
            this.save(relation);
        }

    }

    @Override
    public void getRelationInfoByUserId(String userId, SysOrgPersonVo sysOrgPersonVo) {
        QueryWrapper<SysOrgPostPersonRelation> relationQueryWrapper = new QueryWrapper<>();
        relationQueryWrapper.eq("fd_person_id",userId);
        List<SysOrgPostPersonRelation> relations = this.list(relationQueryWrapper);
        StringBuilder fdPostIds = new StringBuilder();
        StringBuilder fdPostNames = new StringBuilder();
        for(SysOrgPostPersonRelation relation:relations){
            String fdPostId = relation.getFdPostId();
            String fdPostName = relation.getFdPostName();
            fdPostIds.append(';').append(fdPostId);
            fdPostNames.append(';').append(fdPostName);
        }
        if(StrUtil.isNotEmpty(fdPostIds.toString())) fdPostIds = new StringBuilder(fdPostIds.substring(1));
        if(StrUtil.isNotEmpty(fdPostNames.toString())) fdPostNames = new StringBuilder(fdPostNames.substring(1));
        sysOrgPersonVo.setFdPostIds(fdPostIds.toString());
        sysOrgPersonVo.setFdPostNames(fdPostNames.toString());
    }
}
