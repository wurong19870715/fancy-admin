package com.fancy.application.sys.category.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-06-10
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysCategoryMainVo extends BaseVo {
    /**
    * 分类名称
    */
    private String fdName;
    /**
    * 上级分类id
    */
    private String fdParentId;
    /**
    * 上级分类名称
    */
    private String fdParentName;
    /**
    * 模块分类
    */
    private String fdModelName;
    /**
    * 关键值
    */
    private String fdKey;
    /**
    * 排序号
    */
    private String fdOrder;
    /**
    * 创建人id
    */
    private String fdCreatorId;
    /**
    * 创建人
    */
    private String fdCreatorName;
    /**
    * 层级id
    */
    private String fdHierachyId;

}
