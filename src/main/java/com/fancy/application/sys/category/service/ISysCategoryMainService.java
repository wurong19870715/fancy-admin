package com.fancy.application.sys.category.service;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.service.IBaseService;
import com.fancy.application.common.vo.BaseTreeVo;
import com.fancy.application.sys.category.entity.SysCategoryMain;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-06-10
*/

public interface ISysCategoryMainService extends IBaseService<SysCategoryMain> {

    public String defaultModelKey = "category";

    List<BaseTreeVo> getCategoryTree(String fdParentId, String fdModelName, String fdModelKey);

    IPage<SysCategoryMain> page(IPage<SysCategoryMain> page, QueryWrapper<SysCategoryMain> wrapper, String name, String modelName, String parentId);
}
