package com.fancy.application.sys.category.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.category.entity.SysCategoryMain;
import org.apache.ibatis.annotations.Mapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-06-10
*/

@Mapper
public interface SysCategoryMainMapper extends BaseMapper<SysCategoryMain> {
}
