package com.fancy.application.sys.category.service.impl;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.common.utils.UserUtil;
import com.fancy.application.common.vo.BaseTreeVo;
import com.fancy.application.sys.category.entity.SysCategoryMain;
import com.fancy.application.sys.category.mapper.SysCategoryMainMapper;
import com.fancy.application.sys.category.service.ISysCategoryMainService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-06-10
*/

@Service("sysCategoryMainService")
public class SysCategoryMainServiceImpl extends BaseService<SysCategoryMainMapper,SysCategoryMain> implements ISysCategoryMainService {
    @Override
    public List<BaseTreeVo> getCategoryTree(String fdParentId, String fdModelName, String fdModelKey) {
        QueryWrapper<SysCategoryMain> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fd_parent_id",fdParentId)
                .eq("fd_model_name",fdModelName)
                .eq("fd_key",fdModelKey);
        List<SysCategoryMain> categoryMains = this.list(queryWrapper);
        List<BaseTreeVo> treeViewList = categoryMains.stream().map(sysCategoryMain ->
                new BaseTreeVo(sysCategoryMain.getFdId(),sysCategoryMain.getFdName())).collect(Collectors.toList());
        return treeViewList;
    }

    @Override
    public IPage<SysCategoryMain> page(IPage<SysCategoryMain> page, QueryWrapper<SysCategoryMain> wrapper, String modelName, String parentId,String modelKey) {
        if(StrUtil.isBlank(modelKey))modelKey = ISysCategoryMainService.defaultModelKey;
        wrapper.eq("fd_parent_id",parentId)
                .eq("fd_model_name",modelName)
                .eq("fd_key",modelKey);
        return super.page(page,wrapper);
    }

    public boolean saveOrUpdate(SysCategoryMain main) {
        String fdId = UserUtil.getCurrentUser().getFdId();
        String fdName = UserUtil.getCurrentUser().getFdName();
        main.setFdCreatorId(fdId);
        main.setFdCreatorName(fdName);
        String fdHierachyId = "";
        String fdParentId = main.getFdParentId();
        if(StrUtil.isNotBlank(fdParentId)) {
            SysCategoryMain model = this.getById(fdParentId);
            if(model != null){
                fdHierachyId = model.getFdHierachyId();
            }
        }
        String uuid = "";
        if(StrUtil.isBlank(main.getFdId())){
            uuid = IdUtil.simpleUUID();
            main.setFdId(uuid);
        }else{
            uuid = main.getFdId();
        }
        if(StrUtil.isBlank(fdHierachyId)){
            fdHierachyId += uuid;
        }else{
            fdHierachyId += "x"+uuid;
        }

        main.setFdHierachyId(fdHierachyId);
        return super.saveOrUpdate(main);
    }
}
