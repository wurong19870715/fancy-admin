package com.fancy.application.sys.category.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.category.entity.SysCategoryMain;
import com.fancy.application.sys.category.service.ISysCategoryMainService;
import com.fancy.application.sys.category.vo.SysCategoryMainVo;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-06-10
*/

@RestController
@RequestMapping("/sys/category/sys_category_main")
public class SysCategoryMainController extends BaseController {
@Resource
private ISysCategoryMainService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody Map<String,String> map){
        String current = map.get("current");
        String keyword = map.get("keyword");
        String modelName = map.get("modelName");
        String parentId = map.get("parentId");
        String modelKey = map.get("modelKey");
        BasePage<SysCategoryMain> basePage = new BasePage<>();
        basePage.setCurrent(Integer.parseInt(current));
        basePage.setKeyword(keyword);

        IPage<SysCategoryMain> sysCategoryMainPage = baseService.page(basePage.getPage(),basePage.getWrapper(),modelName,parentId,modelKey);
        return Result.ok(sysCategoryMainPage);
    }

    @PutMapping
    public Result save(@RequestBody SysCategoryMain sysCategoryMain){
        baseService.saveOrUpdate(sysCategoryMain);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysCategoryMainVo sysCategoryMainVo = new SysCategoryMainVo();
        SysCategoryMain sysCategoryMain = baseService.getById(id);
        BeanUtils.copyProperties(sysCategoryMain,sysCategoryMainVo);
        return Result.ok().addData(sysCategoryMainVo);
    }

    @GetMapping(value = "/treeData")
    public Result treeData(
            @RequestParam String fdParentId,
            @RequestParam String fdModelName,
            @RequestParam String fdModelKey)
    {
        return Result.ok().addData(baseService.getCategoryTree(fdParentId,fdModelName,fdModelKey));
    }
}
