package com.fancy.application.sys.category.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-06-10
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_category_main")
@Dictionary(modelName = "com.fancy.application.sys.category.entity.SysCategoryMain",
        tableName = "sys_category_main",
        serviceBean = "sysCategoryMainService",
        messageKey = "通用分类",
        desc="通用分类"
)
public class SysCategoryMain extends BaseEntity {

    /**
    * 分类名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "分类名称",name = "fdName",column = "fd_name",length=50)
    private String fdName;
    /**
    * 上级分类id
    */
    @TableField(value="fd_parent_id")
    @SimpleField(messageKey = "上级分类id",name = "fdParentId",column = "fd_parent_id",relationModel =SysCategoryMain.class )
    private String fdParentId;
    /**
    * 上级分类名称
    */
    @SimpleField(messageKey = "上级分类名称",name = "fdParentName",column = "fd_parent_name",relationModel =SysCategoryMain.class,relationField = "fdName")
    @TableField(value="fd_parent_name")
    private String fdParentName;
    /**
    * 模块分类
    */
    @TableField(value="fd_model_name")
    @SimpleField(messageKey = "模块分类",name = "fdModelName",column = "fd_model_name",length=200)
    private String fdModelName;
    /**
    * 关键值
    */
    @TableField(value="fd_key")
    @SimpleField(messageKey = "关键值",name = "fdKey",column = "fd_key",length=50)
    private String fdKey;
    /**
    * 排序号
    */
    @TableField(value="fd_order")
    @SimpleField(messageKey = "排序号",name = "fdOrder",column = "fd_order",length=10)
    private String fdOrder;
    /**
    * 创建人id
    */
    @TableField(value="fd_creator_id")
    @SimpleField(messageKey = "创建人id",name = "fdCreatorId",column = "fd_creator_id",relationModel = SysOrgPerson.class)
    private String fdCreatorId;
    /**
    * 创建人
    */
    @TableField(value="fd_creator_name")
    @SimpleField(messageKey = "创建人",name = "fdCreatorName",column = "fd_creator_name",relationModel = SysOrgPerson.class,relationField = "fdName")
    private String fdCreatorName;
    /**
    * 层级id
    */
    @TableField(value="fd_hierachy_id")
    @SimpleField(messageKey = "层级id",name = "fdHierachyId",column = "fd_hierachy_id",length = 500)
    private String fdHierachyId;

}
