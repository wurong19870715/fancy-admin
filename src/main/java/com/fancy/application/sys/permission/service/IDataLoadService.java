package com.fancy.application.sys.permission.service;

import com.fancy.application.sys.permission.entity.BaseModuleEntity;
import com.fancy.application.sys.permission.entity.RoleResource;

import java.util.List;

public interface IDataLoadService {

    public void load();

    public List<BaseModuleEntity> getModules();

    List<RoleResource> getRoleResourceByModulePath(String fdId);

    public List<RoleResource> getAllRoleResource();

}
