package com.fancy.application.sys.permission.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermissionRoleVo extends BaseVo {
    /**
    * 分类名称
    */
    private String fdCateName;
    /**
    * 角色名
    */
    private String fdName;
    /**
    * 是否有效
    */
    private Boolean fdIsAvaliable = Boolean.FALSE;
    /**
    * 分类id
    */
    private String fdCateId;

    /**
     * 权限列表
     */
    private String checkedRes;

    /**
     * 组织架构id
     */
    private String fdOrgId;
    /**
     * 组织架构名称
     */
    private String fdOrgName;

}
