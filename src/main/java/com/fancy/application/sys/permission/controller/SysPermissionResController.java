package com.fancy.application.sys.permission.controller;


import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONArray;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.permission.entity.SysPermissionRes;
import com.fancy.application.sys.permission.service.ISysPermissionResService;
import com.fancy.application.sys.permission.service.ISysPermissionUrlService;
import com.fancy.application.sys.permission.vo.SysPermissionPage;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@RestController
@RequestMapping("/sys/permission/sys_permission_res")
public class SysPermissionResController extends BaseController {
    @Resource
    private ISysPermissionResService baseService;
    @Resource
    private ISysPermissionUrlService sysPermissionUrlService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody SysPermissionPage<SysPermissionRes> basePage){
        QueryWrapper<SysPermissionRes> wrapper = basePage.getWrapper();
        wrapper.eq("fd_category_id",basePage.getFdCategoryId());
        IPage<SysPermissionRes> sysPermissionResPage = baseService.page(basePage.getPage(),wrapper);
        return Result.ok(sysPermissionResPage);
    }

    @GetMapping(value = "/listAll")
    public Result listAll(@RequestParam String fdModuleId,@RequestParam String fdId ){
        if (StrUtil.isEmpty(fdModuleId)) {
            fdModuleId = sysPermissionUrlService.getById(fdId).getFdModuleId();
        }
        JSONArray resArray = baseService.getAllResByCategoryId(fdModuleId);
        return Result.ok().addData(resArray);
    }

    @PutMapping
    public Result save(@RequestBody SysPermissionRes sysPermissionRes){
        baseService.saveOrUpdate(sysPermissionRes);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysPermissionRes sysPermissionRes = baseService.getById(id);
        return Result.ok().addData(sysPermissionRes);
    }

    @GetMapping("/all")
    public Result all(@RequestParam(required = false) String fdRoleId){
        JSONArray list = baseService.getAllRes(fdRoleId);
        return Result.ok().addData(list);
    }
}
