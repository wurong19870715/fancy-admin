package com.fancy.application.sys.permission.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.permission.entity.SysPermissionRoleOrg;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

public interface ISysPermissionRoleOrgService extends IBaseService<SysPermissionRoleOrg> {

    void saveOrgIds(String fdRoleId,String fdOrgId);

    void removeByRoleId(String id);


}
