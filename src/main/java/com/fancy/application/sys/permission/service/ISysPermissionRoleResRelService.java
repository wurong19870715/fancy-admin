package com.fancy.application.sys.permission.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.permission.entity.SysPermissionRoleResRel;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-06
*/

public interface ISysPermissionRoleResRelService extends IBaseService<SysPermissionRoleResRel> {

    void removeByRoleId(String fdRoleId);

    void saveRoleResRelation(String s, String fdRoleId);

    List<String> findResByPersonId(String personId);

}
