package com.fancy.application.sys.permission.service;

import com.fancy.application.sys.permission.entity.FilterInfo;

public interface ISysPermissionService {
    FilterInfo getFilterInfoByModelName(String modelName);
}
