package com.fancy.application.sys.permission.entity;

import lombok.Data;

@Data
public class FilterRoleInfo {
    private String key;
    private String resName;
}
