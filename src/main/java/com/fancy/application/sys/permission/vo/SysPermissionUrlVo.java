package com.fancy.application.sys.permission.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-02-25
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermissionUrlVo extends BaseVo {
    /**
    * 路径
    */
    private String fdPath;
    /**
    * 上级id
    */
    private String fdParentId;
    /**
    * 表达式
    */
    private String fdExpression;

    /**
     * 模块id
     */
    private String fdModuleId;

    /**
     * 请求方法
     */
    private String fdMethod;

}
