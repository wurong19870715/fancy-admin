package com.fancy.application.sys.permission.entity;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Data
public class FilterInfo {

    /**
     * 权限字段
     */
    private String authColumn;


    private List<FilterRoleInfo> authRole;

    public FilterInfo() {
        this.authRole = new ArrayList<>();
    }
}
