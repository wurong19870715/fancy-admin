package com.fancy.application.sys.permission.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.sys.org.entity.SysOrgModel;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_permission_role_org_person")
@Dictionary(
        modelName = "com.fancy.application.sys.permission.entity.SysPermissionRoleOrgPerson",
        tableName = "sys_permission_role_org_person",
        serviceBean = "sysPermissionRoleOrgPersonService",
        messageKey = "人员角色关联",
        desc="人员角色关联"
)
public class SysPermissionRoleOrgPerson extends BaseEntity {

    /**
    * 人员id
    */
    @TableField(value="fd_person_id")
    @SimpleField(messageKey = "人员id",name = "fdPersonId",column = "fd_person_id",relationModel = SysOrgPerson.class)
    private String fdPersonId;
    /**
    * 角色id
    */
    @TableField(value="fd_role_id")
    @SimpleField(messageKey = "角色id",name = "fdRoleId",column = "fd_role_id",relationModel = SysPermissionRole.class)
    private String fdRoleId;

}
