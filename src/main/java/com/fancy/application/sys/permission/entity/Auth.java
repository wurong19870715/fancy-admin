package com.fancy.application.sys.permission.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;

/**
 * 权限过滤器
 */
@Data
public class Auth {
    private String modelName;
    private List<String> aUthFilter;
    public Auth() {}
}
