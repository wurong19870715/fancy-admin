package com.fancy.application.sys.permission.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import com.fancy.application.sys.permission.entity.SysPermissionRole;
import com.fancy.application.sys.permission.vo.SysPermissionRoleVo;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

public interface ISysPermissionRoleService extends IBaseService<SysPermissionRole> {


    List<String> getResByUser(SysOrgPerson person);

    void saveOrUpdateRole(SysPermissionRoleVo sysPermissionRoleVo);

    void deleteSysPermissionRoleById(String id);

    void setOrgs(SysPermissionRoleVo sysPermissionRoleVo, String id);

}
