package com.fancy.application.sys.permission.service;


import cn.hutool.json.JSONArray;
import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.permission.entity.BaseModuleEntity;
import com.fancy.application.sys.permission.entity.SysPermissionRes;
import java.util.List;
import java.util.Map;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

public interface ISysPermissionResService extends IBaseService<SysPermissionRes> {

    JSONArray getAllRes(String fdRoleId);

    JSONArray getAllResByCategoryId(String fdModuleId);

}
