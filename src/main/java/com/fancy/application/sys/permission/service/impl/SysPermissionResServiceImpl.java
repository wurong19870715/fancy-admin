package com.fancy.application.sys.permission.service.impl;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.permission.entity.*;
import com.fancy.application.sys.permission.mapper.SysPermissionResMapper;
import com.fancy.application.sys.permission.service.IDataLoadService;
import com.fancy.application.sys.permission.service.ISysPermissionResCategoryService;
import com.fancy.application.sys.permission.service.ISysPermissionResService;
import com.fancy.application.sys.permission.service.ISysPermissionRoleResRelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@Service("sysPermissionResService")
public class SysPermissionResServiceImpl extends BaseService<SysPermissionResMapper,SysPermissionRes> implements ISysPermissionResService {

    @Resource
    private ISysPermissionResCategoryService sysPermissionResCategoryService;

    @Resource
    private ISysPermissionRoleResRelService sysPermissionRoleResRelService;

    @Resource
    private IDataLoadService dataLoadService;

    /**
     * 获取所有的权限,并且按照模块展示
     * @return
     * @param fdRoleId
     */
    @Override
    public JSONArray getAllRes(String fdRoleId) {
        List<BaseModuleEntity> baseModuleEntities = dataLoadService.getModules();
        JSONArray jsonArray = new JSONArray();
        List<SysPermissionResCategory> categories = sysPermissionResCategoryService.list();
        QueryWrapper<SysPermissionRoleResRel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fd_role_id",fdRoleId);
        List<SysPermissionRoleResRel> resRelations = sysPermissionRoleResRelService.list(queryWrapper);
        List<String> resList = resRelations.stream().map(SysPermissionRoleResRel::getFdResId).collect(Collectors.toList());


        baseModuleEntities.forEach(baseModuleEntity -> {
            String fdId = baseModuleEntity.getFdPath();
            String fdName = baseModuleEntity.getFdName();
            JSONObject resObject = getResByCateId(fdId,resList);
            //resObject.put("cateId",fdId);
            resObject.put("cateName",fdName);
            resObject.put("cateAllSelected",resObject.get("containsResFlag"));
            jsonArray.add(resObject);
        });
        return jsonArray;
    }


    @Override
    public JSONArray getAllResByCategoryId(String fdModuleId) {
        JSONArray jsonArray = new JSONArray();
        QueryWrapper<SysPermissionRes> wrapper = new QueryWrapper<>();
        wrapper.eq("fd_category_id",fdModuleId);
        List<SysPermissionRes> resObj = this.list(wrapper);
        resObj.forEach(sysPermissionRes -> {
            String value = sysPermissionRes.getFdResName();
            String  name = sysPermissionRes.getFdName();
            JSONObject object = new JSONObject();
            object.put("value",value);
            object.put("name",name);
            jsonArray.add(object);
        });
        return jsonArray;
    }


    private JSONObject getResByCateId(String fdId, List<String> autoResList) {
        QueryWrapper<SysPermissionRes> resQueryWrapper = new QueryWrapper<>();
        resQueryWrapper.eq("fd_category_id",fdId);
        List<SysPermissionRes> resList = this.list(resQueryWrapper);
        List<RoleResource> resources = dataLoadService.getRoleResourceByModulePath(fdId);
        JSONObject resObject = new JSONObject();
        JSONArray rtnResList = new JSONArray();
        List<Integer> allLength = new ArrayList<>();
        resources.forEach(roleResource -> {
            JSONObject object = new JSONObject();
            Boolean checked = autoResList.contains(roleResource.getValue())?Boolean.TRUE:Boolean.FALSE;
            object.put("id",roleResource.getValue());
            object.put("name",roleResource.getName());
            object.put("key",roleResource.getValue());
            object.put("checked",checked);
            rtnResList.add(object);
            if(checked){
                allLength.add(1);
            } else{
                allLength.add(0);
            }
        });
        resObject.put("res",rtnResList);
        int sum = allLength.stream().mapToInt(value -> value).sum();
        resObject.put("containsResFlag",allLength.size()==sum);
        return resObject;
    }
}
