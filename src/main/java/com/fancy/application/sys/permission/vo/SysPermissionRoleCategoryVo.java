package com.fancy.application.sys.permission.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermissionRoleCategoryVo extends BaseVo {
    /**
    * 所属分类id
    */
    private String fdParentId;
    /**
    * 排序号
    */
    private Integer fdOrder;
    /**
    * 所属分类名称
    */
    private String fdParentName;
    /**
    * 分类名称
    */
    private String fdName;

}
