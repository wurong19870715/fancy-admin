package com.fancy.application.sys.permission.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.permission.entity.SysPermissionRoleResRel;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-06
*/

@Mapper
public interface SysPermissionRoleResRelMapper extends BaseMapper<SysPermissionRoleResRel> {
    List<String> findResByPersonId(String personId);
}
