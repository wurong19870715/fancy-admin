package com.fancy.application.sys.permission.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermissionResVo extends BaseVo {
    /**
    * 名称
    */
    private String fdName;
    /**
    * 权限名称
    */
    private String fdResName;

    /**
     * 分类id
     */
    private String fdCategoryId;

    /**
     * 分类name
     */
    private String fdCategoryName;

}
