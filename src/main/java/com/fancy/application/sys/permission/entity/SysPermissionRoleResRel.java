package com.fancy.application.sys.permission.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-06
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_permission_role_res_rel")
@Dictionary(
        modelName = "com.fancy.application.sys.permission.entity.SysPermissionRoleResRel",
        tableName = "sys_permission_role_res_rel",
        serviceBean = "sysPermissionRoleResRelService",
        messageKey = "角色-权限关联",
        desc="角色-权限关联"
)
public class SysPermissionRoleResRel extends BaseEntity {

    /**
    * 角色id
    */
    @TableField(value="fd_role_id")
    @SimpleField(messageKey = "角色id",name = "fdRoleId",column = "fd_role_id",relationModel = SysPermissionRole.class)
    private String fdRoleId;
    /**
    * 权限id
    */
    @TableField(value="fd_res_id")
    @SimpleField(messageKey = "权限id",name = "fdResId",column = "fd_res_id",relationModel = SysPermissionRes.class)
    private String fdResId;

}
