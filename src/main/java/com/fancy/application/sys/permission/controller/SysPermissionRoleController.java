package com.fancy.application.sys.permission.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.permission.entity.SysPermissionRole;
import com.fancy.application.sys.permission.service.ISysPermissionRoleService;
import com.fancy.application.sys.permission.vo.SysPermissionPage;
import com.fancy.application.sys.permission.vo.SysPermissionRoleVo;
import org.springframework.beans.BeanUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@RestController
@RequestMapping("/sys/permission/sys_permission_role")
public class SysPermissionRoleController extends BaseController {
    @Resource
    private ISysPermissionRoleService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody SysPermissionPage<SysPermissionRole> basePage){
        QueryWrapper<SysPermissionRole> queryWrapper = basePage.getWrapper();
        queryWrapper.eq("fd_cate_id",basePage.getFdCategoryId());
        IPage<SysPermissionRole> sysPermissionRolePage = baseService.page(basePage.getPage(),queryWrapper);
        return Result.ok(sysPermissionRolePage);
    }

    @PutMapping
    public Result save(@RequestBody SysPermissionRoleVo sysPermissionRoleVo){
        baseService.saveOrUpdateRole(sysPermissionRoleVo);
        return Result.ok();
    }

    @DeleteMapping
    public Result del(@RequestParam String id ){
        baseService.deleteSysPermissionRoleById(id);
        return Result.ok();
    }

    @GetMapping
    public Result view(@RequestParam String id ){
        SysPermissionRoleVo sysPermissionRoleVo = new SysPermissionRoleVo();
        SysPermissionRole sysPermissionRole = baseService.getById(id);
        BeanUtils.copyProperties(sysPermissionRole,sysPermissionRoleVo);
        baseService.setOrgs(sysPermissionRoleVo,id);
        return Result.ok().addData(sysPermissionRoleVo);
    }
}
