package com.fancy.application.sys.permission.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.permission.entity.SysPermissionResCategory;
import com.fancy.application.sys.permission.service.ISysPermissionResCategoryService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@RestController
@RequestMapping("/sys/permission/sys_permission_res_category")
public class SysPermissionResCategoryController extends BaseController {
    @Resource
    private ISysPermissionResCategoryService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysPermissionResCategory> basePage){
        IPage<SysPermissionResCategory> sysPermissionResCategoryPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(sysPermissionResCategoryPage);
    }

    @GetMapping(value = "/all")
    public Result all(){
        return Result.ok(baseService.list());
    }

    @PutMapping
    public Result save(@RequestBody SysPermissionResCategory sysPermissionResCategory){
        baseService.saveOrUpdate(sysPermissionResCategory);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysPermissionResCategory sysPermissionResCategory = baseService.getById(id);
        return Result.ok().addData(sysPermissionResCategory);
    }
}
