package com.fancy.application.sys.permission.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermissionRoleOrgPersonVo extends BaseVo {
    /**
    * 人员id
    */
    private String fdPersonId;
    /**
    * 角色id
    */
    private String fdRoleId;

}
