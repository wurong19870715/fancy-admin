package com.fancy.application.sys.permission.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.permission.entity.SysPermissionRoleOrgPerson;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

public interface ISysPermissionRoleOrgPersonService extends IBaseService<SysPermissionRoleOrgPerson> {

    void savePersonIds(List<String> orgList, String fdRoleId);

    void removeByRoleId(String id);
}
