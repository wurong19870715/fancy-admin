package com.fancy.application.sys.permission.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_permission_role")
@Dictionary(
        modelName = "com.fancy.application.sys.permission.entity.SysPermissionRole",
        tableName = "sys_permission_role",
        serviceBean = "sysPermissionRoleService",
        messageKey = "权限角色",
        desc="权限角色"
)
public class SysPermissionRole extends BaseEntity {

    /**
    * 分类名称
    */
    @TableField(value="fd_cate_name")
    @SimpleField(messageKey = "分类名称",name = "fdCateName",column = "fd_cate_name",length = 50,relationModel = SysPermissionRoleCategory.class,relationField = "fdName")
    private String fdCateName;
    /**
    * 角色名
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "角色名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;
    /**
    * 是否有效
    */
    @TableField(value="fd_is_avaliable")
    @SimpleField(messageKey = "是否有效",name = "fdIsAvaliable",column = "fd_is_avaliable",type = TypeEnums.BOOLEAN)
    private Boolean fdIsAvaliable = Boolean.FALSE;
    /**
    * 分类id
    */
    @TableField(value="fd_cate_id")
    @SimpleField(messageKey = "分类id",name = "fdCateId",column = "fd_cate_id",length = 50,relationModel = SysPermissionRoleCategory.class)
    private String fdCateId;

}
