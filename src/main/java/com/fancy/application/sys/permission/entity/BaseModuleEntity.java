package com.fancy.application.sys.permission.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/*
模块列表
 */
@Data
public class BaseModuleEntity {

    private String fdName;

    private Integer fdOrder;

    private String fdPath;

    private List<RoleResource> roleResources;

    private List<Request> requests;

    private List<Auth> auths;

    public BaseModuleEntity() {
        this.roleResources = new ArrayList<>();
        this.requests = new ArrayList<>();
        this.auths = new ArrayList<>();
    }

}
