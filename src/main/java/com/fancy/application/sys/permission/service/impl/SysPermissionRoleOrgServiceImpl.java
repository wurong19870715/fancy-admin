package com.fancy.application.sys.permission.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.permission.entity.SysPermissionRoleOrg;
import com.fancy.application.sys.permission.mapper.SysPermissionRoleOrgMapper;
import com.fancy.application.sys.permission.service.ISysPermissionRoleOrgPersonService;
import com.fancy.application.sys.permission.service.ISysPermissionRoleOrgService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

@Service("sysPermissionRoleOrgService")
public class SysPermissionRoleOrgServiceImpl extends BaseService<SysPermissionRoleOrgMapper,SysPermissionRoleOrg> implements ISysPermissionRoleOrgService {

    @Resource
    private ISysPermissionRoleOrgPersonService sysPermissionRoleOrgPersonService;

    @Override
    @Transactional
    public void saveOrgIds(String fdRoleId, String fdOrgId) {
        List<String> orgList = new ArrayList<>();
        if(StrUtil.indexOf(fdOrgId, ';') > -1) {
            orgList.addAll(StrUtil.split(fdOrgId,';'));
        }else{
            orgList.add(fdOrgId);
        }
        this.removeByRoleId(fdRoleId);
        orgList.forEach(s -> this.save(new SysPermissionRoleOrg(s,fdRoleId)));
        sysPermissionRoleOrgPersonService.savePersonIds(orgList,fdRoleId);
    }

    @Override
    public void removeByRoleId(String id) {
        QueryWrapper<SysPermissionRoleOrg> orgQueryWrapper = new QueryWrapper<>();
        orgQueryWrapper.eq("fd_role_id",id);
        this.remove(orgQueryWrapper);

        sysPermissionRoleOrgPersonService.removeByRoleId(id);
    }


}
