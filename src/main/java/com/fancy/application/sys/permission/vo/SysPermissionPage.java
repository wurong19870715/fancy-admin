package com.fancy.application.sys.permission.vo;


import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermissionPage<T> extends BasePage<T> {

    /**
     * 分类id
     */
    private String fdCategoryId;

    private String fdParentId;

}
