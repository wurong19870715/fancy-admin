package com.fancy.application.sys.permission.controller;



import cn.hutool.core.util.StrUtil;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;

import com.fancy.application.common.vo.Result;
import com.fancy.application.common.vo.SelectVo;

import com.fancy.application.sys.permission.entity.SysPermissionUrl;
import com.fancy.application.sys.permission.service.ISysPermissionMatcher;
import com.fancy.application.sys.permission.service.ISysPermissionUrlService;
import com.fancy.application.sys.permission.vo.SysPermissionPage;
import com.fancy.application.sys.permission.vo.SysPermissionUrlVo;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
* url权限控制主要逻辑如下：
 * 1、每一个url都有一个父类基础的权限，用于快速验证该请求是否属于此权限下，父类一般为GET权限
 * 2、子权限可以自己扩展自己的权限，并且定义请求方法，但是一般get为页面url可以访问，其他为后台访问，get|post|put可以跳转到你没有权限页面，delete只能提示你没有权限操作此数据
 * 3、父类和子类为且的关系，即，如果父类验证不通过，子类就不必再验证，父类验证通过，子类验证不通过，也属于验证失败
 * 4、待定
* @author wison
* 2020-02-25
*/
@Log4j2
@RestController
@RequestMapping("/sys/permission/sys_permission_url")
public class SysPermissionUrlController extends BaseController {
    @Resource
    private ISysPermissionUrlService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody SysPermissionPage<SysPermissionUrl> basePage){
        QueryWrapper<SysPermissionUrl> wrapper = basePage.getWrapper();
        wrapper.eq("fd_module_id",basePage.getFdCategoryId());
        IPage<SysPermissionUrl> sysPermissionUrlPage = baseService.page(basePage.getPage(),wrapper);
        return Result.ok(sysPermissionUrlPage);
    }

    @GetMapping(value = "/listAll")
    public Result listAll(@RequestParam String fdModuleId,@RequestParam String fdId ){
        if (StrUtil.isEmpty(fdModuleId)) {
            fdModuleId = this.baseService.getById(fdId).getFdModuleId();
        }
        List<SysPermissionUrl> urls = baseService.getParentUrls(fdModuleId);
        List<SelectVo> selectVos = baseService.getSelectVoData(urls);
        return Result.ok().addData(selectVos);
    }

    @PutMapping
    public Result save(@RequestBody SysPermissionUrl sysPermissionUrl){
        baseService.saveOrUpdate(sysPermissionUrl);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysPermissionUrl sysPermissionUrl = baseService.getById(id);
        return Result.ok().addData(sysPermissionUrl);
    }

    /**
     * 校验url是否有这个权限
     * @param permissionPath 校验url
     * @return
     */
    @GetMapping("/checkPermission")
    public Result checkPermission(@RequestParam("permissionPath") String permissionPath){
        log.warn("permissionPath:{}",permissionPath);
        Boolean flag = sysPermissionMatcher.doMatch(permissionPath,"GET");
        return Result.ok().addData(flag);

    }

    @Resource
    private ISysPermissionMatcher sysPermissionMatcher;
}
