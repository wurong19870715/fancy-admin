package com.fancy.application.sys.permission.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.org.service.ISysOrgCoreService;
import com.fancy.application.sys.permission.entity.SysPermissionRoleOrg;
import com.fancy.application.sys.permission.entity.SysPermissionRoleOrgPerson;
import com.fancy.application.sys.permission.mapper.SysPermissionRoleOrgPersonMapper;
import com.fancy.application.sys.permission.service.ISysPermissionRoleOrgPersonService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

@Service("sysPermissionRoleOrgPersonService")
public class SysPermissionRoleOrgPersonServiceImpl extends BaseService<SysPermissionRoleOrgPersonMapper,SysPermissionRoleOrgPerson> implements ISysPermissionRoleOrgPersonService {

    @Resource
    private ISysOrgCoreService sysOrgCoreService;

    @Override
    public void savePersonIds(List<String> orgList, String fdRoleId) {
        List<String> ids = sysOrgCoreService.expandToPersonIds(orgList);

        List<SysPermissionRoleOrgPerson> personList = new ArrayList<>();
        ids.forEach(s -> {
            SysPermissionRoleOrgPerson permissionRoleOrgPerson = new SysPermissionRoleOrgPerson();
            permissionRoleOrgPerson.setFdPersonId(s);
            permissionRoleOrgPerson.setFdRoleId(fdRoleId);
            personList.add(permissionRoleOrgPerson);
        });
        this.saveBatch(personList);
    }

    @Override
    public void removeByRoleId(String id) {
        LambdaQueryWrapper<SysPermissionRoleOrgPerson> orgQueryWrapper = new LambdaQueryWrapper<>();
        orgQueryWrapper.eq(SysPermissionRoleOrgPerson::getFdRoleId,id);
        this.remove(orgQueryWrapper);
    }
}
