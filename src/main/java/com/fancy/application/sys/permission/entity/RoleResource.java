package com.fancy.application.sys.permission.entity;

import lombok.Data;

@Data
public class RoleResource {

    private String value;

    private String name;

    private String description;

    public RoleResource() {
    }
}
