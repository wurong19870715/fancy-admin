package com.fancy.application.sys.permission.vo;


import com.baomidou.mybatisplus.annotation.TableField;
import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermissionResCategoryVo extends BaseVo {
    /**
    * 分类名称
    */
    private String fdName;
    /**
    * 排序号
    */
    private Integer fdOrder;

    /**
     * 模块路径
     */
    private String fdModulePath;

}
