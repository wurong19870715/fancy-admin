package com.fancy.application.sys.permission.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.permission.entity.SysPermissionRoleOrgPerson;
import org.apache.ibatis.annotations.Mapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

@Mapper
public interface SysPermissionRoleOrgPersonMapper extends BaseMapper<SysPermissionRoleOrgPerson> {
}
