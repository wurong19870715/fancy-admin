package com.fancy.application.sys.permission.service.impl;

import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.common.utils.RedisUtil;
import com.fancy.application.sys.permission.entity.BaseModuleEntity;
import com.fancy.application.sys.permission.entity.SysPermissionResCategory;
import com.fancy.application.sys.permission.mapper.SysPermissionResCategoryMapper;
import com.fancy.application.sys.permission.service.ISysPermissionResCategoryService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@Service("sysPermissionResCategoryService")
public class SysPermissionResCategoryServiceImpl extends BaseService<SysPermissionResCategoryMapper,SysPermissionResCategory> implements ISysPermissionResCategoryService {

}
