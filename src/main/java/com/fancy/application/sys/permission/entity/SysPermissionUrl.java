package com.fancy.application.sys.permission.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-02-25
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_permission_url")
@Dictionary(
        modelName = "com.fancy.application.sys.permission.entity.SysPermissionUrl",
        tableName = "sys_permission_url",
        serviceBean = "sysPermissionUrlService",
        messageKey = "url权限",
        desc="url权限"
)
public class SysPermissionUrl extends BaseEntity {

    /**
    * 路径
    */
    @TableField(value="fd_path")
    @SimpleField(messageKey = "路径",name = "fdPath",column = "fd_path",length = 200)
    private String fdPath;
    /**
    * 上级id
    */
    @TableField(value="fd_parent_id")
    @SimpleField(messageKey = "上级id",name = "fdParentId",column = "fd_parent_id",relationModel = SysPermissionUrl.class)
    private String fdParentId;
    /**
    * 表达式
    */
    @TableField(value="fd_expression")
    @SimpleField(messageKey = "表达式",name = "fdExpression",column = "fd_expression",length = 200)
    private String fdExpression;

    /**
     * 模块id
     */
    @TableField(value="fd_module_id")
    @SimpleField(messageKey = "模块id",name = "fdModuleId",column = "fd_module_id")
    private String fdModuleId;

    /**
     * 请求方法
     */
    @TableField(value="fd_method")
    @SimpleField(messageKey = "请求方法",name = "fdMethod",column = "fd_method",length=50)
    private String fdMethod;

}
