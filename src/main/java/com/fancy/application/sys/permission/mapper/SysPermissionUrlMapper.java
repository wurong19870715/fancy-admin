package com.fancy.application.sys.permission.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.permission.entity.SysPermissionUrl;
import org.apache.ibatis.annotations.Mapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-02-25
*/

@Mapper
public interface SysPermissionUrlMapper extends BaseMapper<SysPermissionUrl> {
}
