package com.fancy.application.sys.permission.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_permission_res")
@Dictionary(modelName = "com.fancy.application.sys.permission.entity.SysPermissionRes",
        tableName = "sys_permission_res",
        serviceBean = "sysPermissionResService",
        messageKey = "权限名称",
        desc="权限名称列表"
)
public class SysPermissionRes extends BaseEntity {

    /**
    * 名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;
    /**
    * 权限名称
    */
    @TableField(value="fd_res_name")
    @SimpleField(messageKey = "权限名称",name = "fdResName",column = "fd_res_name",length = 50)
    private String fdResName;

    /**
     * 分类id
     */
    @TableField(value="fd_category_id")
    @SimpleField(messageKey = "权限分类id",name = "fdCategoryId",column = "fd_category_id",relationModel = SysPermissionResCategory.class)
    private String fdCategoryId;

    /**
     * 分类name
     */
    @TableField(value="fd_category_name")
    @SimpleField(messageKey = "权限分类名称",name = "fdCategoryName",column = "fd_category_name",relationModel = SysPermissionResCategory.class,relationField ="fdName")
    private String fdCategoryName;
}
