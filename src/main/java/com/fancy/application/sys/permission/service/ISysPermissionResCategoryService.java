package com.fancy.application.sys.permission.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.permission.entity.SysPermissionResCategory;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

public interface ISysPermissionResCategoryService extends IBaseService<SysPermissionResCategory> {

}
