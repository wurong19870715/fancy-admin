package com.fancy.application.sys.permission.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.common.vo.SelectVo;
import com.fancy.application.sys.permission.entity.SysPermissionUrl;
import com.fancy.application.sys.permission.mapper.SysPermissionUrlMapper;
import com.fancy.application.sys.permission.service.ISysPermissionUrlService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-02-25
*/

@Service("sysPermissionUrlService")
public class SysPermissionUrlServiceImpl extends BaseService<SysPermissionUrlMapper,SysPermissionUrl> implements ISysPermissionUrlService {

    /**
     * 获取父url权限
     * @return
     * @param fdModuleId
     */
    @Override
    public List<SysPermissionUrl> getParentUrls(String fdModuleId) {
        QueryWrapper<SysPermissionUrl> urlQueryWrapper = new QueryWrapper<>();
        urlQueryWrapper.eq("fd_parent_id","");
        urlQueryWrapper.eq("fd_module_id",fdModuleId);
        List<SysPermissionUrl> urls = this.list(urlQueryWrapper);
        return urls;
    }

    @Override
    public List<SelectVo> getSelectVoData(List<SysPermissionUrl> urls) {
        List<SelectVo> selectVos = new ArrayList<>();
        urls.forEach(sysPermissionUrl -> selectVos.add(new SelectVo(sysPermissionUrl.getFdPath(),sysPermissionUrl.getFdId())));
        return selectVos;
    }
}
