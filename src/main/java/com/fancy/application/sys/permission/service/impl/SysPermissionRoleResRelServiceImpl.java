package com.fancy.application.sys.permission.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.permission.entity.SysPermissionRoleResRel;
import com.fancy.application.sys.permission.mapper.SysPermissionRoleResRelMapper;
import com.fancy.application.sys.permission.service.ISysPermissionRoleResRelService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-06
*/

@Service("sysPermissionRoleResRelService")
public class SysPermissionRoleResRelServiceImpl extends BaseService<SysPermissionRoleResRelMapper,SysPermissionRoleResRel> implements ISysPermissionRoleResRelService {

    @Override
    public void removeByRoleId(String fdRoleId) {
        QueryWrapper<SysPermissionRoleResRel> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fd_role_id",fdRoleId);
        this.remove(queryWrapper);
    }

    @Override
    public void saveRoleResRelation(String s, String fdRoleId) {
        SysPermissionRoleResRel resRel = new SysPermissionRoleResRel();
        resRel.setFdResId(s);
        resRel.setFdRoleId(fdRoleId);
        this.save(resRel);
    }

    @Override
    public List<String> findResByPersonId(String personId) {
        return baseMapper.findResByPersonId(personId);
    }

}
