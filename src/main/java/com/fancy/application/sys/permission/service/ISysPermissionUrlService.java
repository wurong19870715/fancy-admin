package com.fancy.application.sys.permission.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.common.vo.SelectVo;
import com.fancy.application.sys.permission.entity.SysPermissionUrl;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-02-25
*/

public interface ISysPermissionUrlService extends IBaseService<SysPermissionUrl> {

    List<SysPermissionUrl> getParentUrls(String fdModuleId);

    List<SelectVo> getSelectVoData(List<SysPermissionUrl> urls);
}
