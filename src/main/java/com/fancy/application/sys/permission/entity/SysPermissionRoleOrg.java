package com.fancy.application.sys.permission.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.sys.org.entity.SysOrgModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_permission_role_org")
@Dictionary(
        modelName = "com.fancy.application.sys.permission.entity.SysPermissionRoleOrg",
        tableName = "sys_permission_role_org",
        serviceBean = "sysPermissionRoleOrgService",
        messageKey = "角色组织架构关联",
        desc="角色组织架构关联"
)
public class SysPermissionRoleOrg extends BaseEntity {

    public SysPermissionRoleOrg(String fdOrgId, String fdRoleId) {
        this.fdOrgId = fdOrgId;
        this.fdRoleId = fdRoleId;
    }

    /**
    * 组织架构id
    */
    @SimpleField(messageKey = "组织架构id",name = "fdOrgId",column = "fd_org_id",relationModel = SysOrgModel.class)
    @TableField(value="fd_org_id")
    private String fdOrgId;
    /**
    * 组织架构类型
    */
    @SimpleField(messageKey = "组织架构类型",name = "fdOrgType",column = "fd_org_type",relationModel = SysOrgModel.class,relationField = "fdOrgType")
    @TableField(value="fd_org_type")
    private String fdOrgType;
    /**
    * 角色id
    */
    @SimpleField(messageKey = "角色id",name = "fdRoleId",column = "fd_role_id",relationModel = SysPermissionRole.class)
    @TableField(value="fd_role_id")
    private String fdRoleId;

}
