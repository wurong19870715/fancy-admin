package com.fancy.application.sys.permission.service;

public interface ISysPermissionMatcher {
    public  Boolean  doMatch(String uri,String method);
}
