package com.fancy.application.sys.permission.service.impl;


import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.common.utils.UserUtil;
import com.fancy.application.sys.org.entity.SysOrgModel;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import com.fancy.application.sys.org.service.ISysOrgCoreService;
import com.fancy.application.sys.permission.entity.RoleResource;
import com.fancy.application.sys.permission.entity.SysPermissionRes;
import com.fancy.application.sys.permission.entity.SysPermissionRole;
import com.fancy.application.sys.permission.entity.SysPermissionRoleOrg;
import com.fancy.application.sys.permission.mapper.SysPermissionRoleMapper;
import com.fancy.application.sys.permission.service.*;
import com.fancy.application.sys.permission.vo.SysPermissionRoleVo;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@Service("sysPermissionRoleService")
public class SysPermissionRoleServiceImpl extends BaseService<SysPermissionRoleMapper,SysPermissionRole> implements ISysPermissionRoleService {

    @Resource
    private ISysPermissionRoleResRelService sysPermissionRoleResRelService;
    @Resource
    private ISysPermissionResService sysPermissionResService;
    @Resource
    private ISysPermissionRoleOrgService sysPermissionRoleOrgService;
    @Resource
    private ISysOrgCoreService sysOrgCoreService;
    @Resource
    private IDataLoadService dataLoadService;

    @Override
    public List<String> getResByUser(SysOrgPerson person) {
        if(UserUtil.isAdmin(person)){
            List<RoleResource> roleResources = dataLoadService.getAllRoleResource();
            return roleResources.stream().map(RoleResource::getValue).collect(Collectors.toList());
        }else{
            String personId = person.getFdId();
            return sysPermissionRoleResRelService.findResByPersonId(personId);
        }

    }

    @Override
    @Transactional
    public void saveOrUpdateRole(SysPermissionRoleVo sysPermissionRoleVo) {
        SysPermissionRole sysPermissionRole = new SysPermissionRole();
        BeanUtils.copyProperties(sysPermissionRoleVo,sysPermissionRole);
        this.saveOrUpdate(sysPermissionRole);
        //维护权限
        if (StrUtil.isNotEmpty(sysPermissionRoleVo.getCheckedRes())) {
            List<String> resList = StrUtil.splitTrim(sysPermissionRoleVo.getCheckedRes(),";");
            String fdRoleId = sysPermissionRole.getFdId();
            sysPermissionRoleResRelService.removeByRoleId(fdRoleId);
            resList.forEach(s -> sysPermissionRoleResRelService.saveRoleResRelation(s,fdRoleId));
        }
        //维护组织架构
        sysPermissionRoleOrgService.saveOrgIds(sysPermissionRole.getFdId(),sysPermissionRoleVo.getFdOrgId());
    }

    @Override
    @Transactional
    public void deleteSysPermissionRoleById(String id) {
        this.removeById(id);
        sysPermissionRoleOrgService.removeByRoleId(id);
        sysPermissionRoleResRelService.removeByRoleId(id);
    }

    /**
     * 加载指派的用户对象
     * @param sysPermissionRoleVo
     * @param fdRoleId
     */
    @Override
    public void setOrgs(SysPermissionRoleVo sysPermissionRoleVo, String fdRoleId) {
        QueryWrapper<SysPermissionRoleOrg> orgQueryWrapper = new QueryWrapper<>();
        orgQueryWrapper.eq("fd_role_id",fdRoleId);
        List<SysPermissionRoleOrg> orgList = sysPermissionRoleOrgService.list(orgQueryWrapper);
        List<String> orgIds = orgList.stream().map(SysPermissionRoleOrg::getFdOrgId).collect(Collectors.toList());
        List<SysOrgModel> orgModels = sysOrgCoreService.getSysOrgModelList(orgIds);
        List<String> orgNames = orgModels.stream().map(SysOrgModel::getFdName).collect(Collectors.toList());
        sysPermissionRoleVo.setFdOrgId(CollectionUtil.join(orgIds,";"));
        sysPermissionRoleVo.setFdOrgName(CollectionUtil.join(orgNames,";"));
    }
}
