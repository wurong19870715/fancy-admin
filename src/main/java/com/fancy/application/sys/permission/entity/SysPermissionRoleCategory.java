package com.fancy.application.sys.permission.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_permission_role_category")
@Dictionary(
        modelName = "com.fancy.application.sys.permission.entity.SysPermissionRoleCategory",
        tableName = "sys_permission_role_category",
        serviceBean = "sysPermissionRoleCategoryService",
        messageKey = "权限角色分类",
        desc="权限角色分类"
)
public class SysPermissionRoleCategory extends BaseEntity {

    /**
    * 所属分类id
    */
    @TableField(value="fd_parent_id")
    @SimpleField(messageKey = "上级分类id",name = "fdParentId",column = "fd_parent_id",relationModel = SysPermissionRoleCategory.class)
    private String fdParentId;
    /**
    * 排序号
    */
    @TableField(value="fd_order")
    @SimpleField(messageKey = "排序号",name = "fdOrder",column = "fd_order",type = TypeEnums.INTEGER)
    private Integer fdOrder;
    /**
    * 所属分类名称
    */
    @TableField(value="fd_parent_name")
    @SimpleField(messageKey = "上级分类名称",name = "fdParentName",column = "fd_parent_name",relationModel = SysPermissionRoleCategory.class,relationField = "fdName")
    private String fdParentName;
    /**
    * 分类名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "上级分类名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;

}
