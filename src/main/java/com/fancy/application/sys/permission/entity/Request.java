package com.fancy.application.sys.permission.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class Request {

    private String path;

    private String role;

    private String method;

    private List<Request> children;

    public List<Request> getChildren() {
        if(children == null) children = new ArrayList<>();
        return children;
    }

    public Request() {
    }
}
