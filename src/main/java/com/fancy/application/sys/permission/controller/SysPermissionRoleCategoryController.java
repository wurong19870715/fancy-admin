package com.fancy.application.sys.permission.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.permission.entity.SysPermissionRoleCategory;
import com.fancy.application.sys.permission.service.ISysPermissionRoleCategoryService;
import com.fancy.application.sys.permission.vo.SysPermissionPage;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@RestController
@RequestMapping("/sys/permission/sys_permission_role_category")
public class SysPermissionRoleCategoryController extends BaseController {
@Resource
private ISysPermissionRoleCategoryService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody SysPermissionPage<SysPermissionRoleCategory> basePage){
        QueryWrapper<SysPermissionRoleCategory> queryWrapper = basePage.getWrapper();
        queryWrapper.eq("fd_parent_id",basePage.getFdParentId());
        IPage<SysPermissionRoleCategory> sysPermissionRoleCategoryPage = baseService.page(basePage.getPage(),queryWrapper);
        return Result.ok(sysPermissionRoleCategoryPage);
    }

    @PutMapping
    public Result save(@RequestBody SysPermissionRoleCategory sysPermissionRoleCategory){
        baseService.saveOrUpdate(sysPermissionRoleCategory);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam(required = false) String id ){
        SysPermissionRoleCategory sysPermissionRoleCategory = baseService.getById(id);
        return Result.ok().addData(sysPermissionRoleCategory);
    }

    @GetMapping(value = "/treeData")
    public Result treeData(@RequestParam(required = false) String fdParentId){
        return Result.ok().addData(baseService.getCategoryTree(fdParentId));
    }
}
