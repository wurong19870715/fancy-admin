package com.fancy.application.sys.permission.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_permission_res_category")
@Dictionary(modelName = "com.fancy.application.sys.permission.entity.SysPermissionResCategory",
        tableName = "sys_permission_res_category",
        serviceBean = "sysPermissionResCategoryService",
        messageKey = "权限分类",
        desc="权限分类"
)
public class SysPermissionResCategory extends BaseEntity {

    /**
    * 分类名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "分类名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;
    /**
    * 排序号
    */
    @TableField(value="fd_order")
    @SimpleField(messageKey = "排序号",name = "fdOrder",column = "fd_order",type = TypeEnums.INTEGER)
    private Integer fdOrder;
    /**
     * 模块路径
     */
    @TableField(value="fd_module_path")
    @SimpleField(messageKey = "模块路径",name = "fdModulePath",column = "fd_module_path",length = 100)
    private String fdModulePath;

}
