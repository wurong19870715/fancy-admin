package com.fancy.application.sys.permission.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-10-11
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysPermissionRoleOrgVo extends BaseVo {
    /**
    * 组织架构id
    */
    private String fdOrgId;
    /**
    * 组织架构类型
    */
    private String fdOrgType;
    /**
    * 角色id
    */
    private String fdRoleId;

}
