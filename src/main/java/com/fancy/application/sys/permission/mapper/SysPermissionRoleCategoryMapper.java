package com.fancy.application.sys.permission.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.permission.entity.SysPermissionRoleCategory;
import org.apache.ibatis.annotations.Mapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@Mapper
public interface SysPermissionRoleCategoryMapper extends BaseMapper<SysPermissionRoleCategory> {
}
