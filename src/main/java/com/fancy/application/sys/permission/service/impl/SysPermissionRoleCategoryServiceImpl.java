package com.fancy.application.sys.permission.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.common.vo.BaseTreeVo;
import com.fancy.application.sys.permission.entity.SysPermissionRoleCategory;
import com.fancy.application.sys.permission.mapper.SysPermissionRoleCategoryMapper;
import com.fancy.application.sys.permission.service.ISysPermissionRoleCategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-09-30
*/

@Service("sysPermissionRoleCategoryService")
public class SysPermissionRoleCategoryServiceImpl extends BaseService<SysPermissionRoleCategoryMapper,SysPermissionRoleCategory> implements ISysPermissionRoleCategoryService {
    @Override
    public List<BaseTreeVo> getCategoryTree(String fdParentId) {
        return getBaseTreeByParentId(fdParentId);
    }


    private List<BaseTreeVo> getBaseTreeByParentId(String parentId){
        QueryWrapper<SysPermissionRoleCategory> sysPermissionRoleCategoryQueryWrapper = new QueryWrapper<>();
        if(StrUtil.isBlank(parentId)){
            parentId = "";
        }
        sysPermissionRoleCategoryQueryWrapper.eq("fd_parent_id",parentId);
        List<SysPermissionRoleCategory> categories = this.list(sysPermissionRoleCategoryQueryWrapper);
        List<BaseTreeVo> btvs = new ArrayList<>();

        categories.forEach(sysPermissionRoleCategory -> {
            BaseTreeVo btv = new BaseTreeVo();
            btv.setId(sysPermissionRoleCategory.getFdId());
            btv.setName(sysPermissionRoleCategory.getFdName());
            List<BaseTreeVo> baseTreeVos = getBaseTreeByParentId(sysPermissionRoleCategory.getFdId());
            btv.setChildren(baseTreeVos);
            btvs.add(btv);
        });
        return btvs;
    }
}
