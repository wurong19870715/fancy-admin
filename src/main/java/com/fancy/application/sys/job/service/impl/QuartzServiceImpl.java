package com.fancy.application.sys.job.service.impl;


import com.fancy.application.common.utils.SpringBeanUtil;
import com.fancy.application.sys.job.quartz.ISysQuartzModel;
import com.fancy.application.sys.job.service.QuartzService;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.lang.reflect.Method;

@Log4j2
@Service("quartzService")
public class QuartzServiceImpl implements QuartzService {


    private static final String METHODNAME = "execute";

    @Override
    public void executeTask(String beanName, String methodName,String fdParam) {
        ISysQuartzModel sysQuartzModel = SpringBeanUtil.getBean(beanName,ISysQuartzModel.class);
        try {
            Method method = sysQuartzModel.getClass().getMethod(methodName,String.class);
            method.invoke(sysQuartzModel,fdParam);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("[QuartzServiceImpl] method invoke error,beanName:{},methodName:{}",beanName,methodName,e.getMessage());
        }

    }

    @Override
    public void executeTask(String beanName,String fdParam) {
        executeTask(beanName, METHODNAME,fdParam);
    }
}
