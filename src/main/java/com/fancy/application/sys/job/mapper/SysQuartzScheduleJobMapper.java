package com.fancy.application.sys.job.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.job.entity.ScheduleJobExample;
import com.fancy.application.sys.job.entity.SysQuartzScheduleJob;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-12-29
*/

@Mapper
public interface SysQuartzScheduleJobMapper extends BaseMapper<SysQuartzScheduleJob> {
    int deleteByExample(ScheduleJobExample example);

    int deleteByPrimaryKey(Integer id);

    Integer insertSelective(SysQuartzScheduleJob record);

    List<SysQuartzScheduleJob> selectByExample(ScheduleJobExample example);

    SysQuartzScheduleJob selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record")SysQuartzScheduleJob record, @Param("example") ScheduleJobExample example);

    int updateByExample(@Param("record") SysQuartzScheduleJob record, @Param("example") ScheduleJobExample example);

    int updateByPrimaryKeySelective(SysQuartzScheduleJob record);

    int updateByPrimaryKey(SysQuartzScheduleJob record);
}
