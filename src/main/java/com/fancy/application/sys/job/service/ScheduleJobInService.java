package com.fancy.application.sys.job.service;

import com.fancy.application.sys.job.entity.SysQuartzScheduleJob;

public interface ScheduleJobInService {

    int insertSelective(SysQuartzScheduleJob scheduleJob);

    SysQuartzScheduleJob selectByJobNameAngJobGroup(String jobName, String groupName);

    SysQuartzScheduleJob selectByPrimaryKey(Integer id);

    int updateByPrimaryKey(SysQuartzScheduleJob scheduleJob);

    int updateByExample(SysQuartzScheduleJob scheduleJob);

    int deleteByPrimaryKey(Integer id);

    int deleteByJobNameAndJobGroup(String jobName, String jobGroup);



}
