package com.fancy.application.sys.job.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.job.entity.SysQuartzScheduleJob;
import com.fancy.application.sys.job.service.ISysQuartzScheduleJobService;
import lombok.extern.log4j.Log4j2;
import org.quartz.SchedulerException;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-12-29
*/
@Log4j2
@RestController
@RequestMapping("/sys/job/sys_quartz_schedule_job")
public class SysQuartzScheduleJobController extends BaseController {
@Resource
private ISysQuartzScheduleJobService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysQuartzScheduleJob> basePage){
        IPage<SysQuartzScheduleJob> sysQuartzScheduleJobPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(sysQuartzScheduleJobPage);
    }

    @PutMapping
    public Result save(@RequestBody SysQuartzScheduleJob sysQuartzScheduleJob){
        baseService.saveOrUpdate(sysQuartzScheduleJob);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam String id ){
        Boolean flag = schedulerJobService.deleteJob(id);
        if(flag){
            return Result.ok();
        }else{
            return Result.error("job删除失败,请联系管理员");
        }
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysQuartzScheduleJob sysQuartzScheduleJob = baseService.getById(id);
        return Result.ok().addData(sysQuartzScheduleJob);
    }


    @Resource
    private ISysQuartzScheduleJobService schedulerJobService;



    /**
     *获取所有的任务
     * @return
     */
    @GetMapping("/getAllJobs")
    public Result getAllJobs(){
        log.info("[JobController] the method:getAllJobs! the url path:------------/getAllJobs----------------");
        List<SysQuartzScheduleJob> jobList = schedulerJobService.getAllScheduleJob();
        log.info("[JobController] the method:getAllJobs is execution over ");
        return Result.ok().addData(jobList);
    }

    /**
     * 获取正在执行的任务列表
     * @return
     * @throws SchedulerException
     */
    @GetMapping("/getRunJob")
    public Result getAllRunningJob() throws SchedulerException {
        log.info("[JobController] the method:getAllRunningJob! the url path:------------/getRunJob----------------");
        List<SysQuartzScheduleJob> jobList = schedulerJobService.getAllRunningJob();
        log.info("[JobController] the method:getAllRunningJob is execution over ");
        return Result.ok().addData(jobList);
    }

    /**
     *运行一个任务
     * @param jobName
     * @param jobGroup
     */
    @GetMapping("/runOneJob")
    public Result runJob(@RequestParam(required = false) String jobName,
                         @RequestParam(required = false) String jobGroup){
        log.info("[JobController] the url path:------------/runOneJob----------------");
        try {
            schedulerJobService.runOneJob(jobName,jobGroup);
            return Result.ok();
        } catch (SchedulerException e) {
            log.error("[JobController] runOnejob is failure in method:runJob");
            return Result.error(e.getMessage());
        }
    }

    /**
     *停止一个定时任务
     * @param jobName
     * @param jobGroup
     */
    @GetMapping(value = "/pauseJob")
    public Result pauseJob(@RequestParam(required = false) String jobName,
                           @RequestParam(required = false) String jobGroup){
        try {
            schedulerJobService.pauseJob(jobName,jobGroup);
            return Result.ok();
        } catch (SchedulerException e) {
            log.error("[JobController] runOnejob is failure in method:runJob");
            return Result.error(e.getMessage());
        }
    }


    /**
     * 启动一个定时任务
     * @param jobName
     * @param jobGroup
     * @return
     */
    @GetMapping("/resumeJob")
    public Object resumeJob(@RequestParam(required = false) String jobName,
                            @RequestParam(required = false) String jobGroup){
        log.info("[JobController] the url path:------------/resumeJob----------------");
        try {
            schedulerJobService.resumeJob(jobName,jobGroup);
            return Result.ok();
        } catch (SchedulerException e) {
            log.error("[JobController] runOnejob is failure in method:runJob");
            return Result.error(e.getMessage());
        }
    }
}
