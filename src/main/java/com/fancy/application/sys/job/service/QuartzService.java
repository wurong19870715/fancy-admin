package com.fancy.application.sys.job.service;

public interface QuartzService {

    void executeTask(String beanName, String methodName, String fdParam);

    void executeTask(String beanName, String fdParam);
}
