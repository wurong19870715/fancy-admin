package com.fancy.application.sys.job.config;



import com.fancy.application.sys.job.entity.SysQuartzScheduleJob;
import com.fancy.application.sys.job.service.QuartzService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 *
 *
 * @create: 2018-05-31 14:38
 **/
@Service("quartzJobFactory")
public class QuartzJobFactory extends QuartzJobBean {

    @Resource
    private QuartzService quartzService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        SysQuartzScheduleJob object = (SysQuartzScheduleJob) jobExecutionContext.getMergedJobDataMap().get("scheduleJob");
        if(object.getFdMethodName()==null || object.getFdMethodName().equals("")){
            quartzService.executeTask(object.getFdBeanName(),object.getFdParam());
        }else {
            quartzService.executeTask(object.getFdBeanName(),object.getFdMethodName(),object.getFdParam());
        }
    }
}
