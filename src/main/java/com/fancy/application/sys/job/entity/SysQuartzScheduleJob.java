package com.fancy.application.sys.job.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-12-29
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_quartz_schedule_job")
@Dictionary(modelName = "com.fancy.application.sys.job.entity.SysQuartzScheduleJob",
        tableName = "sys_quartz_schedule_job",
        serviceBean = "sysQuartzScheduleJobService",
        messageKey = "定时任务表",
        desc="定时任务表"
)
public class SysQuartzScheduleJob extends BaseEntity implements Serializable {

    /**
    * 名称
    */
    @SimpleField(messageKey = "名称",name = "fdName",column = "fd_name",length=50)
    @TableField(value="fd_name")
    private String fdName;
    /**
    * 任务名称
    */
    @SimpleField(messageKey = "任务名称",name = "fdJobName",column = "fd_job_name",length=200)
    @TableField(value="fd_job_name")
    private String fdJobName;
    /**
    * 任务状态
    */
    @SimpleField(messageKey = "任务状态",name = "fdJobStatus",column = "fd_job_status",length=10)
    @TableField(value="fd_job_status")
    private String fdJobStatus = "NORMAL";
    /**
    * 任务组
    */
    @TableField(value="fd_job_group")
    @SimpleField(messageKey = "任务组",name = "fdJobGroup",column = "fd_job_group",length=10,desc = "分系统级任务和用户级任务")
    private String fdJobGroup = "SYSTEM";
    /**
    * 任务脚本
    */
    @SimpleField(messageKey = "任务脚本",name = "fdCronExpression",column = "fd_cron_expression")
    @TableField(value="fd_cron_expression")
    private String fdCronExpression;
    /**
    * 描述
    */
    @TableField(value="fd_description")
    @SimpleField(messageKey = "描述",name = "fdDescription",column = "fd_description",length=500)
    private String fdDescription;
    /**
    * Bean名称
    */
    @TableField(value="fd_bean_name")
    @SimpleField(messageKey = "Bean名称",name = "fdBeanName",column = "fd_bean_name",length=50)
    private String fdBeanName;
    /**
    * method名称
    */
    @TableField(value="fd_method_name")
    @SimpleField(messageKey = "method名称",name = "fdMethodName",column = "fd_method_name",length=50)
    private String fdMethodName;
    /**
    * 参数
    */
    @TableField(value="fd_param")
    @SimpleField(messageKey = "参数",name = "fdParam",column = "fd_param",length=200)
    private String fdParam;
    /**
    * 分类
    */
    @TableField(value="fd_type")
    @SimpleField(messageKey = "分类",name = "fdType",column = "fd_type")
    private String fdType;


    public String getFdJobName() {
        this.fdJobName = this.fdBeanName+this.getFdId();
        return fdJobName;
    }
}
