package com.fancy.application.sys.job.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-12-29
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysQuartzScheduleJobVo extends BaseVo {
    /**
    * 名称
    */
    private String fdName;
    /**
    * 任务名称
    */
    private String fdJobName;
    /**
    * 任务状态
    */
    private String fdJobStatus;
    /**
    * 任务组
    */
    private String fdJobGroup;
    /**
    * 任务脚本
    */
    private String fdCronExpression;
    /**
    * 描述
    */
    private String fdDescription;
    /**
    * Bean名称
    */
    private String fdBeanName;
    /**
    * method名称
    */
    private String fdMethodName;
    /**
    * 参数
    */
    private String fdParam;
    /**
    * 分类
    */
    private String fdType;

}
