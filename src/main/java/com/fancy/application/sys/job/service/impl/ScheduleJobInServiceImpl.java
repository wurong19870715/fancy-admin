package com.fancy.application.sys.job.service.impl;



import com.fancy.application.common.service.impl.BaseService;

import com.fancy.application.sys.job.entity.ScheduleJobExample;
import com.fancy.application.sys.job.entity.SysQuartzScheduleJob;
import com.fancy.application.sys.job.mapper.SysQuartzScheduleJobMapper;
import com.fancy.application.sys.job.service.ScheduleJobInService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * Created by 张城城 on 2018/6/1.
 */
@Service("scheduleJobInService")
public class ScheduleJobInServiceImpl extends BaseService<SysQuartzScheduleJobMapper, SysQuartzScheduleJob> implements ScheduleJobInService {





    @Override
    public int insertSelective(SysQuartzScheduleJob scheduleJob) {
        return baseMapper.insert(scheduleJob);
    }

    @Override
    public SysQuartzScheduleJob selectByJobNameAngJobGroup(String jobName, String groupName) {
        ScheduleJobExample scheduleJobExample = new ScheduleJobExample();
        scheduleJobExample.createCriteria().andJobGroupEqualTo(groupName).andJobNameEqualTo(jobName);
        List<SysQuartzScheduleJob> list = baseMapper.selectByExample(scheduleJobExample);
        if (list.size()>0){
            return list.get(0);
        }
        return null;
    }

    @Override
    public SysQuartzScheduleJob selectByPrimaryKey(Integer id) {
        return baseMapper.selectByPrimaryKey(id);
    }

    @Override
    public int updateByPrimaryKey(SysQuartzScheduleJob scheduleJob) {
        return baseMapper.updateByPrimaryKeySelective(scheduleJob);
    }

    @Override
    public int updateByExample(SysQuartzScheduleJob scheduleJob) {
        ScheduleJobExample scheduleJobExample = new ScheduleJobExample();
        scheduleJobExample.createCriteria().andJobNameEqualTo(scheduleJob.getFdJobName()).andJobGroupEqualTo(scheduleJob.getFdJobGroup());
        return baseMapper.updateByExample(scheduleJob,scheduleJobExample);
    }

    @Override
    public int deleteByPrimaryKey(Integer id) {
        return baseMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteByJobNameAndJobGroup(String jobName, String jobGroup) {
        ScheduleJobExample scheduleJobExample = new ScheduleJobExample();
        scheduleJobExample.createCriteria().andJobGroupEqualTo(jobGroup).andJobNameEqualTo(jobName);
        return baseMapper.deleteByExample(scheduleJobExample);
    }
}

