package com.fancy.application.sys.job.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.job.entity.SysQuartzScheduleJob;
import org.quartz.SchedulerException;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-12-29
*/

public interface ISysQuartzScheduleJobService extends IBaseService<SysQuartzScheduleJob> {
    List<SysQuartzScheduleJob> getAllScheduleJob();

    void checkNotNull(SysQuartzScheduleJob scheduleJob);

    List<SysQuartzScheduleJob> getAllRunningJob() throws SchedulerException;

    boolean saveOrUpdate(SysQuartzScheduleJob scheduleJob);

    public void pauseJob(String jobName, String jobGroup) throws SchedulerException;

    public void deleteJob(String jobName, String jobGroup) throws SchedulerException;

    public void runOneJob(String jobName, String jobGroup) throws SchedulerException;

    public void resumeJob(String jobName, String jobGroup) throws SchedulerException;


    Boolean deleteJob(String id);
}
