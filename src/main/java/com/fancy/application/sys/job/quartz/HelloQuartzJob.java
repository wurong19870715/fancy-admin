package com.fancy.application.sys.job.quartz;

import cn.hutool.core.date.DateUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

/**
 * Created by wison on 18/11/21.
 */
@Log4j2
@Service("helloQuartzJob")
public class HelloQuartzJob implements ISysQuartzModel {

    @Override
    public void excute(String param) {
        log.info("hello quartz  "+ DateUtil.now());
    }
}
