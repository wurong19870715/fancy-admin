package com.fancy.application.sys.config.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.fancy.application.common.constant.Constants;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.config.entity.SysConfigMain;
import com.fancy.application.sys.config.mapper.SysConfigMainMapper;
import com.fancy.application.sys.config.service.ISysConfigMainService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.fancy.application.sys.config.constants.ConfigConstants.CAPTCHA_ENABLED;

/**
 * fancyCode自动生成v1.0
 * @author wison
 * 2019-11-27
 */

@Service("sysConfigMainService")
public class SysConfigMainServiceImpl extends BaseService<SysConfigMainMapper,SysConfigMain> implements ISysConfigMainService {

    /**
     * 获取初始化状态
     */
    @Override
    public String getInitStatus() {
        return getValueByName(Constants.INIT_STATUS);
    }


    @Override
    public void updateInitStatus() {
        final String initStatus = "true";
        SysConfigMain sysConfigMain = getConfigByName(Constants.INIT_STATUS);
        getConfigByName(Constants.INIT_STATUS);
        if(sysConfigMain!=null){
            sysConfigMain.setFdValue(initStatus);
        }else{
            sysConfigMain = new SysConfigMain();
            sysConfigMain.setFdIsSystem(Boolean.TRUE);
            sysConfigMain.setFdName(Constants.INIT_STATUS);
            sysConfigMain.setFdValue(initStatus);
            sysConfigMain.setFdRemark("系统初始化键值");
        }
        this.saveOrUpdate(sysConfigMain);
    }

    private SysConfigMain getConfigByName(String name){
        LambdaQueryWrapper<SysConfigMain> configMainQueryWrapper = new LambdaQueryWrapper<>();
        configMainQueryWrapper.eq(SysConfigMain::getFdName,name);
        return this.getOne(configMainQueryWrapper);
    }

    @Override
    public void saveSystemConfig(Map<String, Object> map) {
        map.forEach((k,v)->{
            SysConfigMain sysConfigMain = getConfigByName(k);
            if (sysConfigMain == null) {
                sysConfigMain = new SysConfigMain();
                sysConfigMain.setFdIsSystem(Boolean.TRUE);
                sysConfigMain.setFdName(k);
            }
            sysConfigMain.setFdValue(String.valueOf(v));
            this.saveOrUpdate(sysConfigMain);
        });
    }

    /**
     * 获取系统配置
     */
    @Override
    public Map<String, Object> getSystemConfig() {
        List<String> keys = CollectionUtil.toList(CAPTCHA_ENABLED);
        LambdaQueryWrapper<SysConfigMain> configMainQueryWrapper = new LambdaQueryWrapper<>();
        configMainQueryWrapper.in(SysConfigMain::getFdName,keys);
        List<SysConfigMain> sysConfigMains = this.list(configMainQueryWrapper);
        Map<String,Object> map = new HashMap<>();
        for(SysConfigMain configMain:sysConfigMains){
            for(String key:keys){
                if(key.equals(configMain.getFdName())){
                    map.put(key,configMain.getFdValue());
                    break;
                }
            }
        }
        return map;
    }

    @Override
    public String getSystemValueByName(String key) {
        LambdaQueryWrapper<SysConfigMain> configMainQueryWrapper = new LambdaQueryWrapper<>();
        configMainQueryWrapper.eq(SysConfigMain::getFdName,key);
        configMainQueryWrapper.eq(SysConfigMain::getFdIsSystem,Boolean.TRUE);
        SysConfigMain configMain = this.getOne(configMainQueryWrapper);
        String value = "false";
        if(configMain != null){
            value = configMain.getFdValue();
        }
        return value;
    }

    @Override
    public String getValueByName(String key) {
        LambdaQueryWrapper<SysConfigMain> configMainQueryWrapper = new LambdaQueryWrapper<>();
        configMainQueryWrapper.eq(SysConfigMain::getFdName,key);
        SysConfigMain configMain = this.getOne(configMainQueryWrapper);
        String value = "";
        if(configMain != null){
            value = configMain.getFdValue();
        }
        return value;
    }
}
