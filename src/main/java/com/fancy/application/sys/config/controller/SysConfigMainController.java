package com.fancy.application.sys.config.controller;



import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.config.entity.SysConfigMain;
import com.fancy.application.sys.config.service.ISysConfigMainService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.Map;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@RestController
@RequestMapping("/sys/config/sys_config_main")
@Log4j2
public class SysConfigMainController extends BaseController {
    @Resource
    private ISysConfigMainService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysConfigMain> basePage){
        QueryWrapper<SysConfigMain> queryWrapper = basePage.getWrapper();

        IPage<SysConfigMain> sysConfigMainPage = baseService.page(basePage.getPage(),queryWrapper);
        return Result.ok(sysConfigMainPage);
    }

    @PutMapping
    public Result save(@RequestBody SysConfigMain sysConfigMain){
        baseService.saveOrUpdate(sysConfigMain);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysConfigMain sysConfigMain = baseService.getById(id);
        return Result.ok().addData(sysConfigMain);
    }

    @PostMapping(value = "/saveSystemConfig")
    public Result saveSystemConfig(@RequestBody Map<String,Object> map){
        baseService.saveSystemConfig(map);
        return Result.ok();
    }
    @GetMapping(value = "/getSystemConfig")
    public Result getSystemConfig(){
        Map<String,Object> map = baseService.getSystemConfig();
        return Result.ok(map);
    }
}
