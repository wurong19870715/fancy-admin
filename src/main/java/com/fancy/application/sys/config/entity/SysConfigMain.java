package com.fancy.application.sys.config.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_config_main")
@Dictionary(modelName = "com.fancy.application.sys.enums.entity.SysConfigMain",
        tableName = "sys_config_main",
        serviceBean = "sysConfigMainService",
        messageKey = "配置项",
        desc="配置项"
)
public class SysConfigMain extends BaseEntity {

    /**
    * 是否系统内置
    */
    @TableField(value="fd_is_system")
    @SimpleField(messageKey = "是否系统内置",name = "fdIsSystem",column = "fd_is_system",length=5)
    private Boolean fdIsSystem = Boolean.FALSE;
    /**
    * 参数名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "参数名称",name = "fdName",column = "fd_name",length=50,nullable = false)
    private String fdName;
    /**
    * 参数值
    */
    @TableField(value="fd_value")
    @SimpleField(messageKey = "参数值",name = "fdValue",column = "fd_value",length=50)
    private String fdValue;
    /**
    * 备注
    */
    @TableField(value="fd_remark")
    @SimpleField(messageKey = "备注",name = "fdRemark",column = "fd_remark",length=500)
    private String fdRemark;


}
