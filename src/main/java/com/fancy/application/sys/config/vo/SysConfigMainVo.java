package com.fancy.application.sys.config.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysConfigMainVo extends BaseVo {
    /**
    * 是否系统内置
    */
    private Boolean fdIsSystem = Boolean.FALSE;
    /**
    * 参数名称
    */
    private String fdName;
    /**
    * 参数值
    */
    private String fdValue;
    /**
    * 备注
    */
    private String fdRemark;

}
