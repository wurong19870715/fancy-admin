package com.fancy.application.sys.config.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.config.entity.SysConfigMain;
import org.apache.ibatis.annotations.Mapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@Mapper
public interface SysConfigMainMapper extends BaseMapper<SysConfigMain> {
}
