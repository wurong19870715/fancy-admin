package com.fancy.application.sys.config.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.config.entity.SysConfigMain;

import java.util.Map;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

public interface ISysConfigMainService extends IBaseService<SysConfigMain> {

    String getInitStatus();

    void updateInitStatus();

    void saveSystemConfig(Map<String, Object> map);

    Map<String, Object> getSystemConfig();

    String getSystemValueByName(String key);

    String getValueByName(String key);
}
