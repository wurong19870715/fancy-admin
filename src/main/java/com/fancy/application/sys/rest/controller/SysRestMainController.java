package com.fancy.application.sys.rest.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.rest.entity.SysRestMain;
import com.fancy.application.sys.rest.service.ISysRestMainService;
import com.fancy.application.sys.rest.vo.SysRestMainVo;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import lombok.extern.log4j.Log4j2;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@RestController
@RequestMapping("/sys/rest/sys_rest_main")
@Log4j2
public class SysRestMainController extends BaseController {

    @Resource
    private ISysRestMainService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysRestMain> basePage){
        IPage<SysRestMain> sysRestMainPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(sysRestMainPage);
    }

    @PutMapping
    public Result save(@RequestBody SysRestMain sysRestMain){
        baseService.saveOrUpdate(sysRestMain);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam(required = false) String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam(required = false) String id ){
        SysRestMain sysRestMain = baseService.getById(id);
        return Result.ok().addData(sysRestMain);
    }
}
