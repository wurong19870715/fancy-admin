package com.fancy.application.sys.rest.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysRestLogVo extends BaseVo {
    /**
    * 参数管理
    */
    private String fdParam;
    /**
    * 接口服务key
    */
    private String fdServiceKey;
    /**
    * 接口IP
    */
    private String fdServiceIp;

}
