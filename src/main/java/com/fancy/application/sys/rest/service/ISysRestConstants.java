package com.fancy.application.sys.rest.service;

public interface ISysRestConstants {

    public final String fdBasePath = "/rest-api";
    public final String fdBasePackage = "com.fancy.application";

}
