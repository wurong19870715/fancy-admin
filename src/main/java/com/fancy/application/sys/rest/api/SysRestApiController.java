package com.fancy.application.sys.rest.api;

import cn.hutool.core.util.StrUtil;
import com.fancy.application.common.annotation.RestApi;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.rest.service.ISysRestPolicyService;
import lombok.extern.log4j.Log4j2;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/rest-api")
@Log4j2
public class SysRestApiController {

    @RestApi(name = "接口名称测试", key = "testdemo")
    @GetMapping(value = "/demo")
    public Result demo() {
        String u = null;
        System.out.println(u.toString());
        return Result.ok();
    }

    @RestApi(name = "接口名称测试-1",desc = "测试描述", key = "testdemo666")
    @GetMapping(value = "/demo444")
    public Result demo1() {
        return Result.ok();
    }

    @RestApi(name = "Token生成", key = "generateToken")
    @GetMapping(value = "/generateToken")
    public Result generateToken(@RequestBody TokenParam tokenParam) {
        TokenResult tokenResult = sysRestPolicyService.generateToken(tokenParam);
        if(StrUtil.isNotEmpty(tokenResult.getToken())){
            return Result.ok().addData(tokenResult);
        }else{
            return Result.error().addData(tokenResult);
        }

    }

    @Resource
    private ISysRestPolicyService sysRestPolicyService;
}
