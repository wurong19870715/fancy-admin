package com.fancy.application.sys.rest.service.impl;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.fancy.application.common.enums.SuccessEnumeration;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.rest.entity.SysRestLog;
import com.fancy.application.sys.rest.mapper.SysRestLogMapper;
import com.fancy.application.sys.rest.service.ISysRestLogService;

import org.springframework.stereotype.Service;
import org.springframework.util.StopWatch;

import javax.servlet.http.HttpServletRequest;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@Service("sysRestLogService")
public class SysRestLogServiceImpl extends BaseService<SysRestLogMapper,SysRestLog> implements ISysRestLogService {

    @Override
    public void saveErrorLog(HttpServletRequest request, Exception e) {
        SysRestLog log = (SysRestLog) request.getAttribute(ISysRestLogService.interceptorKey);
        StopWatch stopWatch = (StopWatch) request.getAttribute(ISysRestLogService.interceptorStopWatch);
        if(stopWatch!=null){
            stopWatch.stop();
            log.setFdExecuteTime(stopWatch.getTotalTimeMillis());
        }
        if(log!=null){
            log.setFdExecuteResult(SuccessEnumeration.FAILED.name());
            log.setFdErrorInfo(ExceptionUtil.stacktraceToString(e));
            save(log);
        }

    }
}
