package com.fancy.application.sys.rest.entity;

import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_rest_policy")
@Dictionary(modelName = "com.fancy.application.sys.rest.entity.SysRestPolicy",
tableName = "sys_rest_policy",
serviceBean = "sysRestPolicyService",
messageKey = "访问策略"
)
public class SysRestPolicy extends BaseEntity {

    /**
    * 策略名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "策略名称",name = "fdName",column = "fd_name",length = 36)
    private String fdName;
    /**
    * 密钥
    */
    @TableField(value="fd_key")
    @SimpleField(messageKey = "密钥",name = "fdKey",column = "fd_key",length = 50)
    private String fdKey;

    /**
     * 应用key
     */
    @TableField(value="fd_rest_id")
    @SimpleField(messageKey = "rest应用Id",name = "fdRestId",column = "fd_rest_id",length = 50)
    private String fdRestId;

    /**
    * 访问策略
    */
    @TableField(value="fd_auth_type")
    @SimpleField(messageKey = "访问策略",name = "fdAuthType",column = "fd_auth_type",length = 36)
    private String fdAuthType;
    /**
    * 访问IP
    */
    @TableField(value="fd_auth_ip")
    @SimpleField(messageKey = "访问IP",name = "fdAuthIp",column = "fd_auth_ip",length = 500)
    private String fdAuthIp;
    /**
    * 备注
    */
    @TableField(value="fd_remark")
    @SimpleField(messageKey = "备注",name = "fdRemark",column = "fd_remark",length = 1000)
    private String fdRemark;
    /**
    * 校验时长（秒）
    */
    @TableField(value="fd_auth_time")
    @SimpleField(messageKey = "校验时长（秒）",name = "fdAuthTime",column = "fd_auth_time",length = 100)
    private Integer fdAuthTime;

}
