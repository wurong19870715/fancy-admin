package com.fancy.application.sys.rest.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysRestPolicyVo extends BaseVo {
    /**
    * 策略名称
    */
    private String fdName;
    /**
    * 密钥
    */
    private String fdKey;
    /**
    * 访问策略
    */
    private String fdAuthType;
    /**
    * 访问IP
    */
    private String fdAuthIp;
    /**
    * 备注
    */
    private String fdRemark;
    /**
    * 校验时长（秒）
    */
    private Integer fdAuthTime;

}
