package com.fancy.application.sys.rest.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.rest.entity.SysRestLog;
import com.fancy.application.sys.rest.service.ISysRestLogService;
import com.fancy.application.sys.rest.vo.SysRestLogVo;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import lombok.extern.log4j.Log4j2;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@RestController
@RequestMapping("/sys/rest/sys_rest_log")
@Log4j2
public class SysRestLogController extends BaseController {

    @Resource
    private ISysRestLogService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysRestLog> basePage){
        IPage<SysRestLog> sysRestLogPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(sysRestLogPage);
    }

    @PutMapping
    public Result save(@RequestBody SysRestLog sysRestLog){
        baseService.saveOrUpdate(sysRestLog);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam(required = false) String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam(required = false) String id ){
        SysRestLog sysRestLog = baseService.getById(id);
        return Result.ok().addData(sysRestLog);
    }
}
