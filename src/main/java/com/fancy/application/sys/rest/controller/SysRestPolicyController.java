package com.fancy.application.sys.rest.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.common.vo.SelectVo;
import com.fancy.application.sys.rest.entity.SysRestPolicy;
import com.fancy.application.sys.rest.service.ISysRestPolicyService;
import com.fancy.application.sys.rest.vo.SysRestPolicyVo;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import lombok.extern.log4j.Log4j2;

import java.util.ArrayList;
import java.util.List;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@RestController
@RequestMapping("/sys/rest/sys_rest_policy")
@Log4j2
public class SysRestPolicyController extends BaseController {

    @Resource
    private ISysRestPolicyService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysRestPolicy> basePage){
        IPage<SysRestPolicy> sysRestPolicyPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(sysRestPolicyPage);
    }

    @PutMapping
    public Result save(@RequestBody SysRestPolicy sysRestPolicy){
        baseService.saveOrUpdate(sysRestPolicy);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam(required = false) String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam(required = false) String id ){
        SysRestPolicy sysRestPolicy = baseService.getById(id);
        return Result.ok().addData(sysRestPolicy);
    }
    @GetMapping(value = "/generateToken")
    public Result generateToken(){
        String fdRestId = RandomUtil.randomString(16);
        String fdKey = RandomUtil.randomString(32);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("fdRestId",fdRestId);
        jsonObject.put("fdKey",fdKey);
        return Result.ok().addData(jsonObject);
    }

    /**
     * 获取策略列表
     * @return
     */
    @GetMapping(value = "/all")
    public Result all(){
        List<SysRestPolicy> sysRestPolicyList = baseService.list();
        List<SelectVo> selectVos = new ArrayList<>();
        sysRestPolicyList.forEach(sysRestPolicy -> {
            selectVos.add(new SelectVo(sysRestPolicy.getFdName(),sysRestPolicy.getFdId()));
        });
        return Result.ok().addData(selectVos);
    }
}
