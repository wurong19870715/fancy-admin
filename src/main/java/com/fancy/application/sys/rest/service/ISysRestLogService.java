package com.fancy.application.sys.rest.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.rest.entity.SysRestLog;

import javax.servlet.http.HttpServletRequest;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

public interface ISysRestLogService extends IBaseService<SysRestLog> {

    public final String interceptorKey = "InterceptorKey";

    public final String interceptorStopWatch = "interceptorStopWatch";

    void saveErrorLog(HttpServletRequest httpServletRequest,Exception e);
}
