package com.fancy.application.sys.rest.api;

import lombok.Data;

@Data
public class TokenParam {
    private String restId;
    private String key;

    @Override
    public String toString(){
        return this.restId+this.key+System.currentTimeMillis();
    }
}
