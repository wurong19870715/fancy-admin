package com.fancy.application.sys.rest.service.impl;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CheckURIResult {
    private Boolean pass;
    private String msg;
    private String restId;
    public CheckURIResult(String msg){
        this.pass = Boolean.FALSE;
        this.msg = msg;
    }
    public CheckURIResult(Boolean pass,String msg){
        this.pass = pass;
        this.msg = msg;
    }
    public CheckURIResult(Boolean pass,String msg,String restId){
        this.pass = pass;
        this.msg = msg;
        this.restId = restId;
    }
}
