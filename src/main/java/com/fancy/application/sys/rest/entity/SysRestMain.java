package com.fancy.application.sys.rest.entity;

import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_rest_main")
@Dictionary(modelName = "com.fancy.application.sys.rest.entity.SysRestMain",
tableName = "sys_rest_main",
serviceBean = "sysRestMainService",
messageKey = "接口列表"
)
public class SysRestMain extends BaseEntity {

    /**
    * 接口名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "接口名称",name = "fdName",column = "fd_name",length = 50)
    private String fdName;
    /**
    * 接口路径
    */
    @TableField(value="fd_service_path")
    @SimpleField(messageKey = "接口路径",name = "fdServicePath",column = "fd_service_path",length = 100)
    private String fdServicePath;
    /**
    * 接口key(唯一)
    */
    @TableField(value="fd_service_key")
    @SimpleField(messageKey = "接口key",name = "fdServiceKey",column = "fd_service_key",length = 36)
    private String fdServiceKey;
    /**
    * 策略id
    */
    @TableField(value="fd_policy_id")
    @SimpleField(messageKey = "策略id",name = "fdPolicyId",column = "fd_policy_id",length = 36)
    private String fdPolicyId;
    /**
    * 策略名称
    */
    @TableField(value="fd_policy_name")
    @SimpleField(messageKey = "策略名称",name = "fdPolicyName",column = "fd_policy_name",length = 100)
    private String fdPolicyName;
    /**
    * 备注
    */
    @TableField(value="fd_remark")
    @SimpleField(messageKey = "备注",name = "fdRemark",column = "fd_remark",length = 500)
    private String fdRemark;


    /**
     * 是否开启
     */
    @TableField(value="fd_enable")
    @SimpleField(messageKey = "是否开启",name = "fdEnable",column = "fd_enable",length = 5)
    private String fdEnable;


}
