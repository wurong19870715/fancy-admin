package com.fancy.application.sys.rest.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysRestMainVo extends BaseVo {
    /**
    * 接口名称
    */
    private String fdName;
    /**
    * 接口路径
    */
    private String fdServicePath;
    /**
    * 接口key
    */
    private String fdServiceKey;
    /**
    * 策略id
    */
    private String fdPolicyId;
    /**
    * 策略名称
    */
    private String fdPolicyName;
    /**
    * 备注
    */
    private String fdRemark;

    /**
     * 备注
     */
    private String fdEnable;

}
