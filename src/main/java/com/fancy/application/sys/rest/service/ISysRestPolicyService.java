package com.fancy.application.sys.rest.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.rest.api.TokenParam;
import com.fancy.application.sys.rest.api.TokenResult;
import com.fancy.application.sys.rest.entity.SysRestPolicy;
import com.fancy.application.sys.rest.service.impl.CheckURIResult;

import javax.servlet.http.HttpServletRequest;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

public interface ISysRestPolicyService extends IBaseService<SysRestPolicy> {

    public String redisKey = "sysRestPolicyList";

    TokenResult generateToken(TokenParam tokenParam);

    boolean checkWhiteList(String fdPolicyId, HttpServletRequest request);

    enum authType{
        anno,token;
    }

    CheckURIResult checkURI(String fdPolicyId, HttpServletRequest requestURI);
}
