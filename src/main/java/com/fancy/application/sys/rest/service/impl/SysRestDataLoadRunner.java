package com.fancy.application.sys.rest.service.impl;

import com.fancy.application.common.utils.RedisUtil;
import com.fancy.application.sys.rest.entity.SysRestMain;
import com.fancy.application.sys.rest.entity.SysRestPolicy;
import com.fancy.application.sys.rest.service.ISysRestMainService;
import com.fancy.application.sys.rest.service.ISysRestPolicyService;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

@Log4j2
@Component
@Order(3)
public class SysRestDataLoadRunner implements ApplicationRunner {
    /**
     * 启动加载接口的相关配置进入缓存
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {
        log.info("开始加载restService进入缓存");
        sysRestMainService.updateCache();
        log.info("加载restService进入缓存结束");
    }

    @Resource
    private ISysRestMainService sysRestMainService;

}
