package com.fancy.application.sys.rest.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.rest.entity.SysRestPolicy;
import org.apache.ibatis.annotations.Mapper;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@Mapper
public interface SysRestPolicyMapper extends BaseMapper<SysRestPolicy> {
}
