package com.fancy.application.sys.rest.service;


import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.rest.entity.SysRestMain;
import com.fancy.application.sys.rest.service.impl.CheckURIResult;

import javax.servlet.http.HttpServletRequest;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

public interface ISysRestMainService extends IBaseService<SysRestMain> {

    String redisKey = "sysRestMainList";

    CheckURIResult isAllowed(HttpServletRequest requestURI);

    void updateCache();

    SysRestMain getRestMainByURI(String URI);

    enum ServiceStatus{
        OK("1"),ERROR("0");


        ServiceStatus(String status) {
            this.status = status;
        }
        private String status;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public static String getServiceStatusnyName(String key){

            if (StringUtils.isEmpty(key)){
                return null;
            }
            for (ServiceStatus serviceStatus : ServiceStatus.values()){
                if(serviceStatus.status.equals(key)){
                    return serviceStatus.status;
                }
            }
            return null;
        }
    }

}
