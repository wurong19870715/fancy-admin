package com.fancy.application.sys.rest.api;

import lombok.Getter;
import lombok.Setter;

/**
 * 返回token结果
 */
@Getter
@Setter
public class TokenResult {
    private String token;
    private String msg;
    public TokenResult(String msg){
        this.token = "";
        this.msg = msg;
    }
    public TokenResult(String token,String msg){
        this.token = token;
        this.msg = msg;
    }
}
