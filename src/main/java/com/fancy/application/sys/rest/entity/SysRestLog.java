package com.fancy.application.sys.rest.entity;

import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-08-09
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_rest_log")
@Dictionary(modelName = "com.fancy.application.sys.rest.entity.SysRestLog",
tableName = "sys_rest_log",
serviceBean = "sysRestLogService",
messageKey = "接口日志"
)
public class SysRestLog extends BaseEntity {

    /**
    * 参数管理
    */
    @TableField(value="fd_param")
    @SimpleField(messageKey = "参数管理",name = "fdParam",column = "fd_param",length = 1000)
    private String fdParam;
    /**
    * 接口服务key
    */
    @TableField(value="fd_service_key")
    @SimpleField(messageKey = "接口服务key",name = "fdServiceKey",column = "fd_service_key",length = 50)
    private String fdServiceKey;
    /**
    * 接口IP
    */
    @TableField(value="fd_service_ip")
    @SimpleField(messageKey = "接口IP",name = "fdServiceIp",column = "fd_service_ip")
    private String fdServiceIp;

    /**
     * 接口执行结果
     */
    @TableField(value="fd_execute_result")
    @SimpleField(messageKey = "执行结果",name = "fdExecuteResult",column = "fd_execute_result",length = 10)
    private String fdExecuteResult;

    /**
     * 错误信息
     */
    @TableField(value="fd_error_info")
    @SimpleField(messageKey = "错误信息",name = "fdErrorInfo",column = "fd_error_info",type=TypeEnums.LONGTEXT,length = 1000)
    private String fdErrorInfo;
    /**
     * 接口执行结果
     */
    @TableField(value="fd_rest_id")
    @SimpleField(messageKey = "关联主表id",name = "fdRestId",column = "fd_rest_id")
    private String fdRestId;

    /**
     * 花费时间
     */
    @TableField(value="fd_execute_time")
    @SimpleField(messageKey = "执行时间",name = "fdExecuteTime",column = "fd_execute_time",type = TypeEnums.LONG)
    private Long fdExecuteTime;


}
