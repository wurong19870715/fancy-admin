package com.fancy.application.sys.datasource.entity;

import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-07-21
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_data_source_main")
@Dictionary(modelName = "com.fancy.application.sys.datasource.entity.SysDataSourceMain",
tableName = "sys_data_source_main",
serviceBean = "sysDataSourceMainService",
messageKey = "数据源管理"
)
public class SysDataSourceMain extends BaseEntity {

    /**
    * 链接名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "链接名称",name = "fdName",column = "fd_name",length = 36)
    private String fdName;
    /**
    * 备注
    */
    @TableField(value="fd_remark")
    @SimpleField(messageKey = "备注",name = "fdRemark",column = "fd_remark",length = 500)
    private String fdRemark;
    /**
    * 唯一值
    */
    @TableField(value="fd_key")
    @SimpleField(messageKey = "唯一值",name = "fdKey",column = "fd_key",length = 36)
    private String fdKey;
    /**
    * 数据库类型
    */
    @TableField(value="fd_db_type")
    @SimpleField(messageKey = "数据库类型",name = "fdDbType",column = "fd_db_type",length = 36)
    private String fdDbType;
    /**
    * 数据库链接url
    */
    @TableField(value="fd_db_url")
    @SimpleField(messageKey = "数据库链接url",name = "fdDbUrl",column = "fd_db_url",length = 200)
    private String fdDbUrl;
    /**
    * 数据库驱动
    */
    @TableField(value="fd_db_driver")
    @SimpleField(messageKey = "数据库驱动",name = "fdDbDriver",column = "fd_db_driver",length = 200)
    private String fdDbDriver;
    /**
    * 用户名
    */
    @TableField(value="fd_username")
    @SimpleField(messageKey = "用户名",name = "fdUsername",column = "fd_username",length = 50)
    private String fdUsername;
    /**
    * 密码
    */
    @TableField(value="fd_password")
    @SimpleField(messageKey = "密码",name = "fdPassword",column = "fd_password",length = 50)
    private String fdPassword;

}
