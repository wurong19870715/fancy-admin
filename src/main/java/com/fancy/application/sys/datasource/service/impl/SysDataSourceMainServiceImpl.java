package com.fancy.application.sys.datasource.service.impl;

import cn.hutool.db.ds.simple.SimpleDataSource;
import cn.hutool.json.JSONObject;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.datasource.entity.SysDataSourceMain;
import com.fancy.application.sys.datasource.mapper.SysDataSourceMainMapper;
import com.fancy.application.sys.datasource.service.ISysDataSourceMainService;

import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.sql.SQLException;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-07-21
* email：wurong715@163.com
*/
@Log4j2
@Service("sysDataSourceMainService")
public class SysDataSourceMainServiceImpl extends BaseService<SysDataSourceMainMapper,SysDataSourceMain> implements ISysDataSourceMainService {
    /**
     * 测试数据库连接新
     *
     * @param sysDataSourceMain
     * @return
     */
    @Override
    public JSONObject testConnection(SysDataSourceMain sysDataSourceMain) {
        DataSource ds = new SimpleDataSource(sysDataSourceMain.getFdDbUrl(), sysDataSourceMain.getFdUsername(), sysDataSourceMain.getFdPassword());
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("testFlag",true);
        jsonObject.put("msg","数据库连接成功!");
        try {
            ds.getConnection();
        } catch (SQLException e) {
            log.warn("测试数据源失败!!");
            log.warn(e.toString());
            jsonObject.put("testFlag",false);
            jsonObject.put("msg",e.toString());
        }
        return jsonObject;
    }
}
