package com.fancy.application.sys.datasource.service;


import cn.hutool.json.JSONObject;
import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.datasource.entity.SysDataSourceMain;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-07-21
* email：wurong715@163.com
*/

public interface ISysDataSourceMainService extends IBaseService<SysDataSourceMain> {

    JSONObject testConnection(SysDataSourceMain sysDataSourceMain);
}
