package com.fancy.application.sys.datasource.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.datasource.entity.SysDataSourceMain;
import org.apache.ibatis.annotations.Mapper;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-07-21
* email：wurong715@163.com
*/

@Mapper
public interface SysDataSourceMainMapper extends BaseMapper<SysDataSourceMain> {
}
