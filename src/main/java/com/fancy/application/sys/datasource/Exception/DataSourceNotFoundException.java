package com.fancy.application.sys.datasource.Exception;

import com.fancy.application.common.exception.FancyRuntimeException;

public class DataSourceNotFoundException extends FancyRuntimeException {
    public DataSourceNotFoundException(String s) {
        super(s);
    }
}
