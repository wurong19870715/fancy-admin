package com.fancy.application.sys.datasource.event;

import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ApplicationContextEvent;

public class DynamicDataSourceRegisterEvent extends ApplicationContextEvent {

    public DynamicDataSourceRegisterEvent(ApplicationContext source) {
        super(source);
    }
}
