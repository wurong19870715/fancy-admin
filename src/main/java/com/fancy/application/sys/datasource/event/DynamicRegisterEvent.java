package com.fancy.application.sys.datasource.event;

import com.fancy.application.sys.datasource.config.RefreshDependency;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * 暂不启用
 */
@Component
public class DynamicRegisterEvent implements ApplicationListener<DynamicDataSourceRegisterEvent> {

    @Override
    public void onApplicationEvent(DynamicDataSourceRegisterEvent event) {
        ApplicationContext ctx = event.getApplicationContext();
        AutowireCapableBeanFactory beanFactory = ctx.getAutowireCapableBeanFactory();
        Map<String, Object> beansMap = ctx.getBeansWithAnnotation(RefreshDependency.class);
        beansMap.values().forEach(beanFactory::autowireBean);
    }
}
