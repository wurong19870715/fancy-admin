package com.fancy.application.sys.datasource.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancyCode 自动生成v1.5
* @author wison
* 2022-07-21
* email：wurong715@163.com
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysDataSourceMainVo extends BaseVo {
    /**
    * 链接名称
    */
    private String fdName;
    /**
    * 备注
    */
    private String fdRemark;
    /**
    * 唯一值
    */
    private String fdKey;
    /**
    * 数据库类型
    */
    private String fdDbType;
    /**
    * 数据库链接url
    */
    private String fdDbUrl;
    /**
    * 数据库驱动
    */
    private String fdDbDriver;
    /**
    * 用户名
    */
    private String fdUsername;
    /**
    * 密码
    */
    private String fdPassword;

}
