package com.fancy.application.sys.datasource.config;

import java.lang.annotation.*;

/**
 * 暂不启用
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface RefreshDependency {

}
