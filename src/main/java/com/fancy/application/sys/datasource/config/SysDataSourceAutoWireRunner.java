package com.fancy.application.sys.datasource.config;

import com.fancy.application.sys.datasource.entity.SysDataSourceMain;
import com.fancy.application.sys.datasource.event.DynamicDataSourceRegisterEvent;
import com.fancy.application.sys.datasource.service.ISysDataSourceMainService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 第三方数据源动态注册
 */
@Log4j2
@Component
@Order(1)
public class SysDataSourceAutoWireRunner implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) {
        log.info("开始执行获取数据源代码");
        List<SysDataSourceMain> mainList = sysDataSourceMainService.list();
        //批量注册数据源
        dataSourceConfig.BatchRegisterDataSource(mainList);
    }


    @Resource
    private ISysDataSourceMainService sysDataSourceMainService;

    @Resource
    private DataSourceConfig dataSourceConfig;
}
