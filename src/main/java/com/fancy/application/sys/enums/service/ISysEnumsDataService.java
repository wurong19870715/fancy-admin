package com.fancy.application.sys.enums.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.common.vo.SelectVo;
import com.fancy.application.sys.enums.entity.SysEnumsData;

import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

public interface ISysEnumsDataService extends IBaseService<SysEnumsData> {

    List<SysEnumsData> getEnumsDataByMainId(String fdId);

    List<SelectVo> getEnumDataListsByMainType(String mainType);
}
