package com.fancy.application.sys.enums.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_enums_main")
@Dictionary(modelName = "com.fancy.application.sys.enums.entity.SysEnumsMain",
        tableName = "sys_enums_main",
        serviceBean = "sysEnumsMainService",
        messageKey = "枚举",
        desc="枚举"
)
public class SysEnumsMain extends BaseEntity {

    /**
    * 枚举名称
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "枚举名称",name = "fdName",column = "fd_name",length=50)
    private String fdName;
    /**
    * 枚举类型
    */
    @TableField(value="fd_type")
    @SimpleField(messageKey = "枚举类型",name = "fdType",column = "fd_type",length=50)
    private String fdType;
    /**
    * 系统枚举
    */
    @TableField(value="fd_is_system")
    @SimpleField(messageKey = "系统枚举",name = "fdIsSystem",column = "fd_is_system",length=50)
    private Boolean fdIsSystem = Boolean.FALSE;
    /**
    * 备注
    */
    @TableField(value="fd_remark")
    @SimpleField(messageKey = "备注",name = "fdRemark",column = "fd_remark",length=500)
    private String fdRemark;
    /**
    * 默认值
    */
    @TableField(value="fd_default_value")
    @SimpleField(messageKey = "默认值",name = "fdDefaultValue",column = "fd_default_value",length=10)
    private String fdDefaultValue;

}
