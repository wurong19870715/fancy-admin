package com.fancy.application.sys.enums.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.common.vo.SelectVo;
import com.fancy.application.sys.enums.entity.SysEnumsMain;
import com.fancy.application.sys.enums.service.ISysEnumsMainService;
import com.fancy.application.sys.enums.vo.SysEnumsMainVo;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@RestController
@RequestMapping("/sys/enums/sys_enums_main")
public class SysEnumsMainController extends BaseController {
@Resource
private ISysEnumsMainService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysEnumsMain> basePage){
        IPage<SysEnumsMain> sysEnumsMainPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(sysEnumsMainPage);
    }

    @GetMapping(value = "/all")
    public Result listAll(){
        List<SelectVo> selectVos = baseService.getAllEnums();
        return Result.ok(selectVos);
    }

    @PutMapping
    public Result save(@RequestBody SysEnumsMain sysEnumsMain){
        baseService.saveOrUpdate(sysEnumsMain);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysEnumsMain sysEnumsMain = baseService.getById(id);
        return Result.ok().addData(sysEnumsMain);
    }

    @GetMapping("/getEnumsByType")
    public Result getEnumsByType(@RequestParam String type ){
        List<SelectVo> enums = baseService.getEnumsByType(type);
        return Result.ok().addData(enums);
    }
}
