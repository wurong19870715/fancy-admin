package com.fancy.application.sys.enums.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_enums_data")
@Dictionary(modelName = "com.fancy.application.sys.enums.entity.SysEnumsData",
        tableName = "sys_enums_data",
        serviceBean = "sysEnumsDataService",
        messageKey = "枚举选项",
        desc="枚举选项"
)
public class SysEnumsData extends BaseEntity {

    /**
    * 显示值
    */
    @TableField(value="fd_name")
    @SimpleField(messageKey = "显示值",name = "fdName",column = "fd_name",length=50)
    private String fdName;
    /**
    * 实际值
    */
    @TableField(value="fd_value")
    @SimpleField(messageKey = "实际值",name = "fdValue",column = "fd_value",length=50)
    private String fdValue;
    /**
    * 排序号
    */
    @TableField(value="fd_order")
    @SimpleField(messageKey = "排序号",name = "fdOrder",column = "fd_order")
    private Integer fdOrder;
    /**
    * 关联枚举id
    */
    @TableField(value="fd_main_id")
    @SimpleField(messageKey = "关联枚举id",name = "fdMainId",column = "fd_main_id",relationModel = SysEnumsMain.class)
    private String fdMainId;

}
