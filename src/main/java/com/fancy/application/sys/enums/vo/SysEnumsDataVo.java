package com.fancy.application.sys.enums.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysEnumsDataVo extends BaseVo {
    /**
    * 显示值
    */
    private String fdName;
    /**
    * 实际值
    */
    private String fdValue;
    /**
    * 排序号
    */
    private Integer fdOrder;
    /**
    * 关联枚举id
    */
    private String fdMainId;

}
