package com.fancy.application.sys.enums.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysEnumsMainVo extends BaseVo {
    /**
    * 枚举名称
    */
    private String fdName;
    /**
    * 枚举类型
    */
    private String fdType;
    /**
    * 系统枚举
    */
    private Boolean fdIsSystem = Boolean.FALSE;
    /**
    * 备注
    */
    private String fdRemark;
    /**
    * 默认值
    */
    private String fdDefaultValue;

}
