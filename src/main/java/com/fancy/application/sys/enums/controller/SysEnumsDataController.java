package com.fancy.application.sys.enums.controller;



import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.Result;
import com.fancy.application.common.vo.SelectVo;
import com.fancy.application.sys.enums.entity.SysEnumsData;
import com.fancy.application.sys.enums.service.ISysEnumsDataService;
import com.fancy.application.sys.enums.vo.SysEnumsDataVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@RestController
@RequestMapping("/sys/enums/sys_enums_data")
public class SysEnumsDataController extends BaseController {
    @Resource
    private ISysEnumsDataService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody Map<String, String> map){
        QueryWrapper<SysEnumsData> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("fd_main_id",map.get("id"));
        List<SysEnumsData> dataList = baseService.list(queryWrapper);
        return Result.ok(dataList);
    }

    @PutMapping
    public Result save(@RequestBody SysEnumsData sysEnumsData){
        baseService.saveOrUpdate(sysEnumsData);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysEnumsData sysEnumsData = baseService.getById(id);
        return Result.ok().addData(sysEnumsData);
    }

    @GetMapping("/getEnumDataListsByMainType")
    public Result getEnumDataListsByMainType(@RequestParam String mainType ){
        List<SelectVo> selectVos = new ArrayList<>();
        if (StrUtil.isNotBlank(mainType)) {
            selectVos = baseService.getEnumDataListsByMainType(mainType);
        }
        return Result.ok(selectVos);
    }
}
