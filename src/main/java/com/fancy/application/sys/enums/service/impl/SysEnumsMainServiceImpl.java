package com.fancy.application.sys.enums.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.common.vo.SelectVo;
import com.fancy.application.sys.enums.entity.SysEnumsData;
import com.fancy.application.sys.enums.entity.SysEnumsMain;
import com.fancy.application.sys.enums.mapper.SysEnumsMainMapper;
import com.fancy.application.sys.enums.service.ISysEnumsDataService;
import com.fancy.application.sys.enums.service.ISysEnumsMainService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@Service("sysEnumsMainService")
public class SysEnumsMainServiceImpl extends BaseService<SysEnumsMainMapper,SysEnumsMain> implements ISysEnumsMainService {

    @Resource
    private ISysEnumsDataService sysEnumsDataService;

    @Override
    public List<SelectVo> getEnumsByType(String type) {
        QueryWrapper<SysEnumsMain> enumsMainQueryWrapper = new QueryWrapper<>();
        enumsMainQueryWrapper.eq("fd_type",type);
        SysEnumsMain main = this.getOne(enumsMainQueryWrapper);
        List<SysEnumsData> enumsDataList = sysEnumsDataService.getEnumsDataByMainId(main.getFdId());
        List<SelectVo> selectVos = new ArrayList<>();
        enumsDataList.forEach(sysEnumsData -> selectVos.add(new SelectVo(sysEnumsData.getFdName(),sysEnumsData.getFdValue())));
        return selectVos;
    }

    @Override
    public List<SelectVo> getAllEnums() {
        List<SysEnumsMain> enumsList = this.list();
        List<SelectVo> selectVos = new ArrayList<>();
        enumsList.forEach(SysEnumsMain -> selectVos.add(new SelectVo(SysEnumsMain.getFdName(),SysEnumsMain.getFdType())));
        return selectVos;
    }
}
