package com.fancy.application.sys.enums.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.enums.entity.SysEnumsData;
import org.apache.ibatis.annotations.Mapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@Mapper
public interface SysEnumsDataMapper extends BaseMapper<SysEnumsData> {
}
