package com.fancy.application.sys.enums.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.common.vo.SelectVo;
import com.fancy.application.sys.enums.entity.SysEnumsData;
import com.fancy.application.sys.enums.entity.SysEnumsMain;
import com.fancy.application.sys.enums.mapper.SysEnumsDataMapper;
import com.fancy.application.sys.enums.service.ISysEnumsDataService;
import com.fancy.application.sys.enums.service.ISysEnumsMainService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
* fancycode自动生成v1.0
* @author wison
* 2019-11-27
*/

@Service("sysEnumsDataService")
public class SysEnumsDataServiceImpl extends BaseService<SysEnumsDataMapper,SysEnumsData> implements ISysEnumsDataService {

    @Resource
    private ISysEnumsMainService sysEnumsMainService;

    @Override
    public List<SysEnumsData> getEnumsDataByMainId(String fdId) {
        QueryWrapper<SysEnumsData> enumsDataQueryWrapper = new QueryWrapper<>();
        enumsDataQueryWrapper.eq("fd_main_id",fdId);
        enumsDataQueryWrapper.orderByAsc("fd_order");
        return this.list(enumsDataQueryWrapper);
    }

    @Override
    public List<SelectVo> getEnumDataListsByMainType(String mainType) {
        QueryWrapper<SysEnumsMain> enumsMainQueryWrapper = new QueryWrapper<>();
        enumsMainQueryWrapper.eq("fd_type",mainType);
        SysEnumsMain main = sysEnumsMainService.getOne(enumsMainQueryWrapper);
        List<SysEnumsData> enumsDataList = this.getEnumsDataByMainId(main.getFdId());
        List<SelectVo> selectVos = new ArrayList<>();
        enumsDataList.forEach(sysEnumsData -> selectVos.add(new SelectVo(sysEnumsData.getFdName(),sysEnumsData.getFdValue())));
        return selectVos;
    }
}
