package com.fancy.application.sys.auth.vo;

import cn.hutool.core.date.DateUtil;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author wison
 * @date 2019/8/28
 */
@Getter
public class Token {
    private String token;

    private Long currentTimes;

    private String userName;

    private List<String> permissions = new ArrayList<>();

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public Token(String token, String userName){
        this.token = token;
        this.userName = userName;
        this.currentTimes = DateUtil.currentSeconds();
    }

}
