package com.fancy.application.sys.auth.vo;

import com.fancy.application.common.vo.BaseVo;
import lombok.Getter;
import lombok.Setter;

/**
 * @author wison
 */
@Getter
@Setter
public class LoginForm {
    private String userName;
    private String password;
    private String captchaCode;
    private String captchaKey;
}
