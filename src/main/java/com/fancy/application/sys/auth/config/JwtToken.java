package com.fancy.application.sys.auth.config;

import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * Created by wison on 2019/8/27.
 */
@Data
public class JwtToken implements AuthenticationToken {

    public JwtToken(String token) {
        this.token = token;
    }

    private String token;

    @Override
    public String getPrincipal() {
        return token;
    }

    @Override
    public String getCredentials() {
        return token;
    }
}
