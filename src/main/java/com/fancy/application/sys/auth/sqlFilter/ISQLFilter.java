package com.fancy.application.sys.auth.sqlFilter;

public interface ISQLFilter {
    String getSQL();
}
