package com.fancy.application.sys.auth.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.utils.JwtUtil;
import com.fancy.application.common.utils.UserUtil;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.auth.service.ILoginService;
import com.fancy.application.sys.auth.service.impl.CaptchaImg;
import com.fancy.application.sys.auth.vo.LoginForm;
import com.fancy.application.sys.auth.vo.Token;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import com.fancy.application.sys.org.service.ISysOrgPersonService;
import com.fancy.application.sys.permission.service.ISysPermissionMatcher;
import com.fancy.application.sys.permission.service.ISysPermissionRoleService;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 *
 * @author wison
 * @date 2019/8/26
 */
@Log4j2
@RestController
@RequestMapping("/auth/login")
public class LoginController extends BaseController {

    @Resource
    private ILoginService loginService;
    @Resource
    private ISysOrgPersonService sysOrgPersonService;
    @Resource
    private JwtUtil jwtUtil;
    @Resource
    private ISysPermissionMatcher sysPermissionMatcher;
    @Resource
    private ISysPermissionRoleService sysPermissionRoleService;

    @PostMapping
    public Result login(@RequestBody LoginForm loginForm){
        //先验证验证码是否正确
        Boolean flag = loginService.validateCaptcha(loginForm.getCaptchaCode(),loginForm.getCaptchaKey());
        if(!flag){
            return Result.error("验证码错误!");
        }
        SysOrgPerson person = sysOrgPersonService.getPersonByLoginInfo(loginForm);
        if(person!= null){
            String token = jwtUtil.sign(loginForm.getUserName(),person.getFdPassword());
            Token tokenModel = new Token(token,person.getFdLoginName());
            List<String> resList = sysPermissionRoleService.getResByUser(person);
            tokenModel.setPermissions(resList);
            return Result.ok().addData(tokenModel);
        }else{
            return Result.error("用户名或密码错误!");

        }

    }

    /**
     * 校验是否启动成功
     */
    @GetMapping("/serverStart")
    public Result serverStart(){
        return Result.ok().addData("fancyAdmin应用启动成功!");
    }


    @GetMapping(value = "/refreshToken")
    public Result refreshToken(HttpServletRequest request){
        String token = request.getParameter("token");
        if(StrUtil.isNotEmpty(token)){
            String username = jwtUtil.getUsername(token);
            QueryWrapper<SysOrgPerson> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("fd_login_name",username).eq("fd_is_avaliable",true);
            SysOrgPerson person = sysOrgPersonService.getOne(queryWrapper);
            token = jwtUtil.sign(username,person.getFdPassword());
            return Result.ok().addData(token);
        }else{
            return Result.error("token为空,请重新登录");
        }

    }
    @GetMapping(value = "/captchaImg")
    public Result generateCaptchaImg(){
        CaptchaImg img = loginService.getCaptchaImg();
        return Result.ok().addData(img);
    }
}
