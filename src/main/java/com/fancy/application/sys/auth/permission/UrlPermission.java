package com.fancy.application.sys.auth.permission;

import lombok.extern.log4j.Log4j2;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.util.AntPathMatcher;
import org.apache.shiro.util.PatternMatcher;

@Log4j2
public class UrlPermission implements Permission {
    //在 Realm 的授权方法中,由数据库查询出来的权限字符串
    private final String url;

    public UrlPermission(String url){
        this.url = url;
    }

    @Override
    public boolean implies(Permission permission) {
        if(!(permission instanceof UrlPermission)){
            return false;
        }
        //
        UrlPermission urlPermission = (UrlPermission)permission;
        PatternMatcher patternMatcher = new AntPathMatcher();

        log.debug("this.url(来自数据库中存放的通配符数据),在 Realm 的授权方法中注入的 => " + this.url);
        log.debug("urlPermission.url(来自浏览器正在访问的链接) => " +  urlPermission.url);
        boolean matches = patternMatcher.matches(this.url,urlPermission.url);
        log.debug("matches => " + matches);
        return matches;
    }
}
