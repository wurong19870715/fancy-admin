package com.fancy.application.sys.auth.service.impl;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
public class CaptchaImg {
    @Getter
    private String key;

    public CaptchaImg(){
        String prefixKey = "captcha_id_";
        Snowflake snowflake = IdUtil.getSnowflake(1, 1);
        long id = snowflake.nextId();
        this.key  = prefixKey+id;
    }


    private String imgData;
}
