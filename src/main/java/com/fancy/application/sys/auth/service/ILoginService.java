package com.fancy.application.sys.auth.service;

import com.fancy.application.sys.auth.service.impl.CaptchaImg;

public interface ILoginService {

    CaptchaImg getCaptchaImg();

    Boolean validateCaptcha(String captchaCode, String captchaKey);
}
