/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fancy.application.sys.auth.vo;

import lombok.Data;

/**
 *
 * @author wison
 */

@Data
public class TreeView {
  private String id;
  private String name;

  public TreeView(String id, String name) {
    this.id = id;
    this.name = name;
  }
}
