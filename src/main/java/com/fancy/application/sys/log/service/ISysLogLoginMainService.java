package com.fancy.application.sys.log.service;


import com.fancy.application.common.service.IBaseService;
import com.fancy.application.sys.log.entity.SysLogLoginMain;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

public interface ISysLogLoginMainService extends IBaseService<SysLogLoginMain> {

    void removeAllLog();
}
