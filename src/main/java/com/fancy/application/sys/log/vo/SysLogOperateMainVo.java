package com.fancy.application.sys.log.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysLogOperateMainVo extends SysLogCommonVo {
    /**
    * 系统模块
    */
    private String fdModule;
    /**
    * 操作类型
    */
    private String fdOperateTyoe;
    /**
    * 请求地址
    */
    private String fdUrl;
    /**
    * IP地址
    */
    private String fdIp;
    /**
    * 登录地址
    */
    private String fdLocation;

}
