package com.fancy.application.sys.log.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@EqualsAndHashCode(callSuper = true)
@Data
@Dictionary(modelName = "com.fancy.application.sys.log.entity.SysLogCommon")
public abstract class SysLogCommon extends BaseEntity {

    /**
    * 状态
    */
    @SimpleField(messageKey = "状态",name = "fdStatus",column = "fd_status")
    @TableField(value="fd_status")
    private String fdStatus;
    /**
    * 日志信息
    */
    @TableField(value="fd_msg")
    @SimpleField(messageKey = "日志信息",name = "fdMsg",column = "fd_msg",length=500)
    private String fdMsg;
    /**
    * 用户id
    */
    @SimpleField(messageKey = "用户id",name = "fdUserId",column = "fd_user_id",relationModel = SysOrgPerson.class)
    @TableField(value="fd_user_id")
    private String fdUserId;
    /**
    * 登录名
    */
    @TableField(value="fd_login_name")
    @SimpleField(messageKey = "登录名",name = "fdLoginName",column = "fd_login_name",relationModel = SysOrgPerson.class,relationField = "fdLoginName")
    private String fdLoginName;
}
