package com.fancy.application.sys.log.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_log_login_main")
@Dictionary(modelName = "com.fancy.application.sys.log.entity.SysLogLoginMain",
        extendClass = "com.fancy.application.sys.log.entity.SysLogLoginCommon",
        tableName = "sys_log_login_main",
        serviceBean = "sysLogLoginMainService",
        messageKey = "登录日志表",
        desc="登录日志表"
)
public class SysLogLoginMain extends SysLogCommon {

    /**
    * IP地址
    */
    @TableField(value="fd_ip")
    @SimpleField(messageKey = "IP地址",name = "fdIp",column = "fd_ip",length=50)
    private String fdIp;
    /**
    * 登录地址
    */
    @TableField(value="fd_location")
    @SimpleField(messageKey = "登录地址",name = "fdLocation",column = "fd_location",length=50)
    private String fdLocation;
    /**
    * 浏览器
    */
    @TableField(value="fd_browser")
    @SimpleField(messageKey = "浏览器",name = "fdBrowser",column = "fd_browser",length=50)
    private String fdBrowser;
    /**
    * 操作系统
    */
    @TableField(value="fd_computer")
    @SimpleField(messageKey = "操作系统",name = "fdComputer",column = "fd_computer",length=50)
    private String fdComputer;

}
