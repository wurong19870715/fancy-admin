package com.fancy.application.sys.log.service.impl;

import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.log.entity.SysLogOperateMain;
import com.fancy.application.sys.log.mapper.SysLogOperateMainMapper;
import com.fancy.application.sys.log.service.ISysLogOperateMainService;
import org.springframework.stereotype.Service;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@Service("sysLogOperateMainService")
public class SysLogOperateMainServiceImpl extends BaseService<SysLogOperateMainMapper,SysLogOperateMain> implements ISysLogOperateMainService {
}
