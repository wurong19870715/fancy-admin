package com.fancy.application.sys.log.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysLogCommonVo extends BaseVo {
    /**
    * 状态
    */
    private String fdStatus;
    /**
    * 日志信息
    */
    private String fdMsg;
    /**
    * 用户id
    */
    private String fdUserId;
    /**
    * 登录名
    */
    private String fdLoginName;

}
