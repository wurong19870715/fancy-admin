package com.fancy.application.sys.log.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fancy.application.common.entity.BaseEntity;
import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="sys_log_operate_main")
@Dictionary(modelName = "com.fancy.application.sys.log.entity.SysLogOperateMain",
        extendClass = "com.fancy.application.sys.log.entity.SysLogLoginCommon",
        tableName = "sys_log_operate_main",
        serviceBean = "sysLogOperateMainService",
        messageKey = "操作日志表",
        desc="操作日志表"
)
public class SysLogOperateMain extends SysLogCommon {

    /**
    * 系统模块
    */
    @TableField(value="fd_module")
    @SimpleField(messageKey = "系统模块",name = "fdModule",column = "fd_module",length=100)
    private String fdModule;
    /**
    * 操作类型
    */
    @TableField(value="fd_operate_tyoe")
    @SimpleField(messageKey = "操作类型",name = "fdOperateTyoe",column = "fd_operate_tyoe",length=50)
    private String fdOperateTyoe;
    /**
    * 请求地址
    */
    @TableField(value="fd_url")
    @SimpleField(messageKey = "请求地址",name = "fdUrl",column = "fd_url",length=500)
    private String fdUrl;
    /**
    * IP地址
    */
    @TableField(value="fd_ip")
    @SimpleField(messageKey = "IP地址",name = "fdIp",column = "fd_ip",length=50)
    private String fdIp;
    /**
    * 登录地址
    */
    @TableField(value="fd_location")
    @SimpleField(messageKey = "登录地址",name = "fdLocation",column = "fd_location",length=50)
    private String fdLocation;

}
