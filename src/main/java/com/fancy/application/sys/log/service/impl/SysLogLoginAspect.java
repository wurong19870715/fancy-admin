package com.fancy.application.sys.log.service.impl;


import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.servlet.ServletUtil;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.auth.vo.LoginForm;
import com.fancy.application.sys.log.entity.SysLogLoginMain;
import com.fancy.application.sys.log.service.ISysLogLoginMainService;
import lombok.SneakyThrows;
import lombok.extern.log4j.Log4j2;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;

/**
 * 登录日志记录,暂时不考虑退出
 */
@Component
@Aspect
@Log4j2
public class SysLogLoginAspect {
    @Resource
    private HttpServletRequest request;

    /**
     * 记录登出日志
     */
    @Pointcut("execution(* com.fancy.application.sys.auth.controller.LoginController.login(*)))")
    public void userLoginAdvice(){}
    /**
     * 记录登出日志
     */
    @Pointcut("execution(* com.baomidou.mybatisplus.extension.service.IService.remove*(..)))")
    public void userLogoutAdvice(){}


    @SneakyThrows
    @Around("userLoginAdvice()")
    @Transactional
    public Object saveUserLoginLog(ProceedingJoinPoint joinPoint){
        Object[] obj = joinPoint.getArgs();
        LoginForm loginForm = (LoginForm) obj[0];
        Object proceed = joinPoint.proceed();
        Result result = (Result) proceed;
        String userName = loginForm.getUserName();
        String userAgent = request.getHeader("user-agent");
        UserAgent ua = UserAgentUtil.parse(userAgent);
        String browser = ua.getBrowser().getName();
        String os = ua.getOs().getName();
        String ip = ServletUtil.getClientIP(request);
        String location = sysLogLocationService.getLocationByIp(ip);
        if(StrUtil.isNotBlank(location)){
            List<String> str = StrUtil.split(location, "|");
            if(str.size()>4){
                location = str.get(3);
            }
        }
        log.debug("ua==={}",userAgent);
        log.debug("browser==={}",browser);
        log.debug("os==={}",os);
        log.debug("ip==={}",ip);
        log.debug("location==={}",location);

        SysLogLoginMain logLoginMain = new SysLogLoginMain();
        if(result.getStatus()==1){  //登录成功
            logLoginMain.setFdStatus("成功");
        }else{  //失败
            logLoginMain.setFdStatus("失败");
        }
        logLoginMain.setFdBrowser(browser);
        logLoginMain.setFdComputer(os);
        logLoginMain.setFdIp(ip);
        logLoginMain.setFdLocation(location);
        logLoginMain.setFdLoginName(userName);
        logLoginMain.setFdMsg(result.getMessage());
        sysLogLoginMainService.save(logLoginMain);
        return result;
    }

    @Resource
    private ISysLogLoginMainService sysLogLoginMainService;

    @Resource
    private SysLogLocationService sysLogLocationService;


}
