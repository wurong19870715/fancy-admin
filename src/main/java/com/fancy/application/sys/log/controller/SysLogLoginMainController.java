package com.fancy.application.sys.log.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.log.entity.SysLogLoginMain;
import com.fancy.application.sys.log.service.ISysLogLoginMainService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@RestController
@RequestMapping("/sys/log/sys_log_login_main")
public class SysLogLoginMainController extends BaseController {
    @Resource
    private ISysLogLoginMainService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysLogLoginMain> basePage){
        IPage<SysLogLoginMain> sysLogLoginMainPage = baseService.page(
                basePage.getPage(),
                basePage.getWrapper("fd_login_name").orderByDesc("fd_create_time"));
        return Result.ok(sysLogLoginMainPage);
    }

    @PutMapping
    public Result save(@RequestBody SysLogLoginMain sysLogLoginMain){
        baseService.saveOrUpdate(sysLogLoginMain);
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysLogLoginMain sysLogLoginMain = baseService.getById(id);
        return Result.ok().addData(sysLogLoginMain);
    }
    @PostMapping(value = "/clearAllLog")
    public Result clearAllLog(){
        this.baseService.removeAllLog();
        return Result.ok();
    }
}
