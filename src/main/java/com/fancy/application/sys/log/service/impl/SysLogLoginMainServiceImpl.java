package com.fancy.application.sys.log.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fancy.application.common.service.impl.BaseService;
import com.fancy.application.sys.log.entity.SysLogLoginMain;
import com.fancy.application.sys.log.mapper.SysLogLoginMainMapper;
import com.fancy.application.sys.log.service.ISysLogLoginMainService;
import com.fancy.application.sys.org.entity.SysOrgPerson;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@Service("sysLogLoginMainService")
public class SysLogLoginMainServiceImpl extends BaseService<SysLogLoginMainMapper,SysLogLoginMain> implements ISysLogLoginMainService {
    @Override
    @Transactional
    public void removeAllLog() {
        List<SysLogLoginMain> mainList = this.list();
        List<String> ids = mainList.stream().map(SysLogLoginMain::getFdId).distinct().collect(Collectors.toList());
        this.removeByIds(ids);
    }
}
