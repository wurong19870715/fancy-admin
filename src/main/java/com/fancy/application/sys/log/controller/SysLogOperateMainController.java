package com.fancy.application.sys.log.controller;



import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fancy.application.common.controller.BaseController;
import com.fancy.application.common.vo.BasePage;
import com.fancy.application.common.vo.Result;
import com.fancy.application.sys.log.entity.SysLogOperateMain;
import com.fancy.application.sys.log.service.ISysLogOperateMainService;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@RestController
@RequestMapping("/sys/log/sys_log_operate_main")
public class SysLogOperateMainController extends BaseController {
@Resource
private ISysLogOperateMainService baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<SysLogOperateMain> basePage){
        IPage<SysLogOperateMain> sysLogOperateMainPage = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(sysLogOperateMainPage);
    }

    @PutMapping
    public Result save(@RequestBody SysLogOperateMain sysLogOperateMain){
        baseService.saveOrUpdate(sysLogOperateMain);
        return Result.ok();
    }
    @DeleteMapping
        public Result del(@RequestParam String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam String id ){
        SysLogOperateMain sysLogOperateMain = baseService.getById(id);
        return Result.ok().addData(sysLogOperateMain);
    }
}
