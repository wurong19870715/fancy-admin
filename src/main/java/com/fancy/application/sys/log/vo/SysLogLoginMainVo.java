package com.fancy.application.sys.log.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@EqualsAndHashCode(callSuper = true)
@Data
public class SysLogLoginMainVo extends SysLogCommonVo {
    /**
    * IP地址
    */
    private String fdIp;
    /**
    * 登录地址
    */
    private String fdLocation;
    /**
    * 浏览器
    */
    private String fdBrowser;
    /**
    * 操作系统
    */
    private String fdComputer;

}
