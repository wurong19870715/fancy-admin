package com.fancy.application.sys.log.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.fancy.application.sys.log.entity.SysLogLoginMain;
import org.apache.ibatis.annotations.Mapper;

/**
* fancycode自动生成v1.0
* @author wison
* 2020-01-02
*/

@Mapper
public interface SysLogLoginMainMapper extends BaseMapper<SysLogLoginMain> {
}
