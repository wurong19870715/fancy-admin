--mysql初始化脚本
--导入管理员账户(admin/1)
INSERT INTO sys_org_person
(fd_salt, fd_login_name, fd_ding_id, fd_password, fd_org_name, fd_wechat_no, fd_birth_day, fd_org_id, fd_sap_no, fd_no, fd_name, fd_order, fd_is_avaliable, fd_parent_id, fd_parent_name, fd_hierarchy_id, fd_id, fd_create_time)
VALUES('c06c9868005e4b2bac8f315ff5c285b0', 'admin', NULL, 'e7645f0687d4a84ec9b515af44b9f1b8', NULL, NULL, NULL, NULL, NULL, NULL, '管理员',NULL, 1, NULL, NULL, NULL, '2819f01a0c464bca8833ceae243e940e', '2020-10-12 00:00:00');
--新建视图sys_org_model
DROP VIEW IF EXISTS sys_org_model ;
create view sys_org_model as
select fd_id,fd_name,fd_no,fd_order,fd_is_avaliable,fd_parent_id,
fd_parent_name,fd_hierarchy_id ,'1' as fd_org_type ,fd_create_time from sys_org_element
UNION
select fd_id,fd_name,fd_no,fd_order,fd_is_avaliable,fd_parent_id,
fd_parent_name,fd_hierarchy_id ,'2' as fd_org_type ,fd_create_time from sys_org_dept
UNION
select fd_id,fd_name,fd_no,fd_order,fd_is_avaliable,fd_parent_id,
fd_parent_name,fd_hierarchy_id ,'4' as fd_org_type ,fd_create_time from sys_org_post
UNION
select fd_id,fd_name,fd_no,fd_order,fd_is_avaliable,fd_parent_id,
fd_parent_name,fd_hierarchy_id ,'8' as fd_org_type ,fd_create_time from sys_org_person;

