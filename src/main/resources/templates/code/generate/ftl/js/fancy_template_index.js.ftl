export default[
    {
        path: '/',
        component: resolve => require(['@/components/common/Template.vue'], resolve),
        meta: { title: '首页' },
        children:[
            <#list tableList as table>
            {
                name: '${table.modelName?uncap_first}List',
                <#if (table_index == 0) >
                path: '/${modulePath?replace(".","/")}',
                <#else>
                path: '/${modulePath?replace(".","/")}/${table.tableName}',
                </#if>
                component: resolve => require(['@/page/${modulePath?replace(".","/")}/${table.tableName}/${table.modelName}_list.vue'], resolve),
                meta: { title: '${table.tableNameStr}',keepAlive: true }
            },
            {
                name: '${table.modelName?uncap_first}Edit',
                path: '/${modulePath?replace(".","/")}/${table.tableName}/edit',
                component: resolve => require(['@/page/${modulePath?replace(".","/")}/${table.tableName}/${table.modelName}_edit.vue'], resolve),
                meta: { title: '新增',keepAlive: false }
            },
            {
                name: '${table.modelName?uncap_first}View',
                path: '/${modulePath?replace(".","/")}/${table.tableName}/view',
                component: resolve => require(['@/page/${modulePath?replace(".","/")}/${table.tableName}/${table.modelName}_view.vue'], resolve),
                meta: { title: '查看',keepAlive: false }
            }
            <#if (table_has_next)>,
                </#if>
            </#list>
        ]
    }
]
