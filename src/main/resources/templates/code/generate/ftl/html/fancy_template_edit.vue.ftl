<template>
    <div>
        <el-container class="form-container">
            <el-card class="box-card" shadow="never">
                <div slot="header" class="clearfix">
                    <span>${tableNameStr}</span>
                    <el-button type="primary" size="mini" style="float:right;padding: 6px 9px;" @click="handlerSubmitForm">保存</el-button>
                </div>
                <div>
                    <el-form  :rules="rules"  ref="form" :label-position="labelPosition"  :model="form" size="small" label-width="120px">
                    <#list list as l>
                        <#if l_index%2==0>
                        <el-row>
                        </#if>
                        <el-col :span="12">
                            <el-form-item  label="${l.fdName}" <#if l.fdIsRequired>required</#if> prop="${l.fdModelName}">
                                <el-input class="input" v-model="form.${l.fdModelName}" <#if l.fdIsRequired>required</#if> ></el-input>
                            </el-form-item>
                        </el-col>
                        <#if l_index%2==1>
                        </el-row>
                        </#if>
                    </#list>
                    <#if list?size%2==1>
                        </el-row>
                    </#if>
                    </el-form>
                </div>
            </el-card>
        </el-container>
    </div>
</template>
<script>
import {edit,parentEdit} from '@/components/mixins/form'
export default {
    name: "${modelName?uncap_first}Edit",
    components: {
    },
    data() {
        return {
            updateUrl:'/api/${modulePath?replace(".","/")}/${tableName}',
            fetchUrl:'/api/${modulePath?replace(".","/")}/${tableName}',
            fetchParentUrl:'/api/${modulePath?replace(".","/")}/${tableName}',
            form: {
                <#list list as l>
                ${l.fdModelName}:''<#if (l_has_next)>,</#if>
                </#list>
            },
            rules:{
            <#list list as l>
                <#if l.fdIsRequired>
                ${l.fdModelName} :[{required: true, message: '请输入${l.fdName}', trigger: 'blur'}]<#if (l_has_next)>,</#if>
                </#if>
            </#list>
	        }
        }
    },
    mixins:[edit,parentEdit]
}
</script>
<style scoped>
@import "~@/assets/css/form.css";
</style>