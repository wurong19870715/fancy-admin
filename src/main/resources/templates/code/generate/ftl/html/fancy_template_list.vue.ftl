<template>
    <div class="table">
        <div class="container">
            <div class="handle-box">
                <el-button size="mini"  type="primary" class="handle-del mr10" @click="add">新增</el-button>
                <el-input size="mini"  v-model="keyword" placeholder="筛选名称" class="handle-input mr10"></el-input>
                <el-button size="mini"  type="primary" icon="search" @click="query">筛选</el-button>
            </div>
            <el-table style="width:100%" :data="tableData.records" border  :header-cell-style="{background:'#F5F7FA'}" ref="multipleTable" >
                <el-table-column type="index" label="序号" align="center" width="50"/>
                <#list list as l>
                    <el-table-column prop="${l.fdModelName}" <#if l.fdColumnType =='Date'>:formatter="dateFormat"</#if> label="${l.fdName}" width="150"/>
                </#list>
                <el-table-column label="操作" align="center">
                    <template slot-scope="scope">
                        <el-button type="primary" size="mini" @click="handleView(scope.row)">查看</el-button>
                        <el-button @click="handleEdit(scope.row)">编辑</el-button>
                        <el-button type="danger" @click="handleDelete(scope.$index, scope.row)">删除</el-button>
                    </template>
                </el-table-column>
            </el-table>
            <div class="pagination">
                <el-pagination background @current-change="handleCurrentChange" :page-size="tableData.size" layout="prev, pager, next" :total="tableData.total"/>
            </div>
        </div>
    </div>
</template>

<script>
import {list} from '@/components/mixins/form';
export default {
    name: '${modelName?uncap_first}List',
    data() {
        return {
            editUrl:'/${modulePath?replace(".","/")}/${tableName}/edit',
            viewUrl:'/${modulePath?replace(".","/")}/${tableName}/view',
            listUrl:'/api/${modulePath?replace(".","/")}/${tableName}/list',
            deleteUrl:'/api/${modulePath?replace(".","/")}/${tableName}'
        }
    },
    mixins:[list]
}
</script>

<style scoped>
 @import "~@/assets/css/list.css";
</style>
