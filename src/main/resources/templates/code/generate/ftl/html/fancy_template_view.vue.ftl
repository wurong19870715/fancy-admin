<template>
    <div class="">
        <el-container class="form-container" style="">
            <el-card class="box-card" shadow="never">
                <div slot="header" class="clearfix">
                    <span>${tableNameStr}</span>
                    <el-button type="primary" size="mini" style="float:right;padding: 6px 9px;margin-left:10px" @click="handlerEdit">编辑</el-button>
                </div>
                <div >
                <el-form  ref="form"   :model="form" size="small" label-width="120px">
                <#list list as l>
                    <#if l_index%2==0>
                    <el-row>
                    </#if>
                    <el-col :span="12">
                        <el-form-item  label="${l.fdName}">
                            <el-input class="input" v-model="form.${l.fdModelName}"></el-input>
                        </el-form-item>
                    </el-col>
                    <#if l_index%2==1>
                    </el-row>
                    </#if>
                </#list>
                <#if list?size%2==1>
                    </el-row>
                </#if>
                </el-form>
                </div>
            </el-card>
        </el-container>
    </div>
</template>
<script>
import {view} from '@/components/mixins/form';
export default {
    name: "${modelName?uncap_first}Add",
    components: {
    },
    data() {
        return {
            editUrl:'/${modulePath?replace(".","/")}/${tableName}/edit',
            fetchUrl:'/api/${modulePath?replace(".","/")}/${tableName}',
            form: {
                <#list list as l>
                ${l.fdModelName}:''<#if (l_has_next)>,</#if>
                </#list>
            }
        }
    },
    mixins:[view]
}
</script>
<style scoped >
@import "~@/assets/css/form.css";
</style>