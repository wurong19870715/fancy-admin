package ${packageName}.${modulePath}.service;


import com.fancy.application.common.service.IBaseService;
import ${packageName}.${modulePath}.entity.${modelName};

<#include "CopyRight.ftl"  encoding="UTF-8">

public interface I${modelName}Service extends IBaseService<${modelName}> {

}
