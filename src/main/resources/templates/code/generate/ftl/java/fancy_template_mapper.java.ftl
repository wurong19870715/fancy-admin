package ${packageName}.${modulePath}.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import ${packageName}.${modulePath}.entity.${modelName};
import org.apache.ibatis.annotations.Mapper;

<#include "CopyRight.ftl"  encoding="UTF-8">

@Mapper
public interface ${modelName}Mapper extends BaseMapper<${modelName}> {
}
