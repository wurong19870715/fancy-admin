package ${packageName}.${modulePath}.entity;

import com.fancy.application.framework.dic.annotation.Dictionary;
import com.fancy.application.framework.dic.annotation.SimpleField;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import ${packageName}.common.entity.BaseEntity;
import com.fancy.application.framework.dic.enums.TypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

<#include "CopyRight.ftl"  encoding="UTF-8">

@EqualsAndHashCode(callSuper = true)
@Data
@TableName(value="${tableName}")
@Dictionary(modelName = "${packageName}.${modulePath}.entity.${modelName}",
tableName = "${tableName}",
serviceBean = "${modelName?uncap_first}Service",
messageKey = "${tableNameStr}"
)
public class ${modelName} extends BaseEntity {

    <#list list as p>
    <#if p.fdModelName != 'fdId' && p.fdModelName != 'fdCreateTime'>
    /**
    * ${p.fdName}
    */
    @TableField(value="${p.fdColumnName}")
    @SimpleField(messageKey = "${p.fdName}",name = "${p.fdModelName}",column = "${p.fdColumnName}",length = ${p.fdLength}<#include "simpleField.ftl"  encoding="UTF-8">)
    <#if p.fdColumnType =='Boolean'>
    private ${p.fdColumnType} ${p.fdModelName} = Boolean.FALSE;
    </#if>
    <#if p.fdColumnType !='Boolean'>
    private ${p.fdColumnType} ${p.fdModelName};
    </#if>
    </#if>
    </#list>

}
