package ${packageName}.${modulePath}.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.baomidou.mybatisplus.core.metadata.IPage;
import ${packageName}.common.controller.BaseController;
import ${packageName}.common.vo.BasePage;
import ${packageName}.common.vo.Result;
import ${packageName}.${modulePath}.entity.${modelName};
import ${packageName}.${modulePath}.service.I${modelName}Service;
import ${packageName}.${modulePath}.vo.${modelName}Vo;
import org.springframework.web.bind.annotation.*;
import javax.annotation.Resource;
import lombok.extern.log4j.Log4j2;

<#include "CopyRight.ftl"  encoding="UTF-8">

@RestController
@RequestMapping("/${modulePath?replace(".","/")}/${tableName}")
@Log4j2
public class ${modelName}Controller extends BaseController {

    @Resource
    private I${modelName}Service baseService;

    @PostMapping(value = "/list")
    public Result list(@RequestBody BasePage<${modelName}> basePage){
        IPage<${modelName}> ${modelName?uncap_first}Page = baseService.page(basePage.getPage(),basePage.getWrapper());
        return Result.ok(${modelName?uncap_first}Page);
    }

    @PutMapping
    public Result save(@RequestBody ${modelName} ${modelName?uncap_first}){
        baseService.saveOrUpdate(${modelName?uncap_first});
        return Result.ok();
    }
    @DeleteMapping
    public Result del(@RequestParam(required = false) String id ){
        baseService.removeById(id);
        return Result.ok();
    }
    @GetMapping
    public Result view(@RequestParam(required = false) String id ){
        ${modelName} ${modelName?uncap_first} = baseService.getById(id);
        return Result.ok().addData(${modelName?uncap_first});
    }
}
