package ${packageName}.${modulePath}.service.impl;

import com.fancy.application.common.service.impl.BaseService;
import ${packageName}.${modulePath}.entity.${modelName};
import ${packageName}.${modulePath}.mapper.${modelName}Mapper;
import ${packageName}.${modulePath}.service.I${modelName}Service;

import org.springframework.stereotype.Service;

<#include "CopyRight.ftl"  encoding="UTF-8">

@Service("${modelName?uncap_first}Service")
public class ${modelName}ServiceImpl extends BaseService<${modelName}Mapper,${modelName}> implements I${modelName}Service {
}
