package ${packageName}.${modulePath}.vo;


import com.fancy.application.common.vo.BaseVo;
import lombok.Data;
import lombok.EqualsAndHashCode;
import java.util.Date;

<#include "CopyRight.ftl"  encoding="UTF-8">

@EqualsAndHashCode(callSuper = true)
@Data
public class ${modelName}Vo extends BaseVo {
<#list list as p>
    <#if p.fdModelName != 'fdId' && p.fdModelName != 'fdCreateTime'>
    /**
    * ${p.fdName}
    */
    <#if p.fdColumnType =='Boolean'>
    private ${p.fdColumnType} ${p.fdModelName} = Boolean.FALSE;
    </#if>
    <#if p.fdColumnType !='Boolean'>
    <#if p.fdColumnType =='Date'>
    private String ${p.fdModelName};
    </#if>
    <#if p.fdColumnType !='Date'>
    private ${p.fdColumnType} ${p.fdModelName};
    </#if>
    </#if>
    </#if>
</#list>

}
